<?php

function quiz_id_meta_box_markup( $object )
{
    wp_nonce_field(basename(__FILE__), "quiz-id-meta-box-nonce");

    $values = get_post_custom( $object->ID );
    $text = isset( $values['balance_quiz_id'] ) ? esc_attr( $values['balance_quiz_id'][0] ) : '';

    ?>
        <input type="text" name="balance_quiz_id" id="balance_quiz_id" value="<?php echo $text; ?>" />
    <?php  
}

function add_quiz_id_meta_box()
{
    add_meta_box("quiz-id-meta-box", "Quiz ID", "quiz_id_meta_box_markup", "quiz", "side", "low", null);
}

add_action("add_meta_boxes", "add_quiz_id_meta_box");

function save_quiz_id_custom_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["quiz-id-meta-box-nonce"]) || !wp_verify_nonce($_POST["quiz-id-meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "quiz";
    if($slug != $post->post_type)
        return $post_id;

    $meta_quiz_id_value = "";

    if(isset($_POST["balance_quiz_id"]))
    {
        $meta_quiz_id_value = $_POST["balance_quiz_id"];
    }   
    update_post_meta($post_id, "balance_quiz_id", $meta_quiz_id_value);
}

add_action("save_post", "save_quiz_id_custom_meta_box", 10, 3);