<?php
    use PHPUnit\Framework\TestCase;

    use GuzzleHttp\Client;
    use GuzzleHttp\Handler\MockHandler;
    use GuzzleHttp\HandlerStack;
    use GuzzleHttp\Psr7\Response;
    use GuzzleHttp\Psr7\Request;
    use GuzzleHttp\Exception\RequestException;


    class ResourcesPage extends TestCase
    {
        public function testResourcePage(){
            // Create a mock and queue two responses.
            $mock = new MockHandler([
                new Response(200, ['X-Foo' => 'Bar'], 'Hello, World'),
                new Response(202, ['Content-Length' => 0]),
                new RequestException('Error Communicating with Server', new Request('GET', 'test'))
            ]);

            $handlerStack = HandlerStack::create($mock);
            $client = new Client();
            $one = microtime(1);

            $response = $client->request('GET', 'http://local-testxekera.com/wp-admin/admin-ajax.php?action=load_resources&query=');

            $two = microtime(1);

            echo "Response Without Search is: ".$response->getStatusCode()." and \r\n";

            echo "\r\n";

            $body = json_decode($response->getBody());
            echo "\r\n";

            echo 'Total records:'.count($body->resources);
            echo "\r\n";

            echo 'Total Request time: '. ( $two - $one );

            $stack = [];

            $this->assertEmpty($stack);

            //echo $client->request('GET', '/')->getStatusCode();
        }

        public function testResourcePageWithSearch(){
            global $argv;

            // Create a mock and queue two responses.
            $mock = new MockHandler([
                new Response(200, ['X-Foo' => 'Bar'], 'Hello, World'),
                new Response(202, ['Content-Length' => 0]),
                new RequestException('Error Communicating with Server', new Request('GET', 'test'))
            ]);

            $handlerStack = HandlerStack::create($mock);
            $client = new Client();
            $one = microtime(1);

//            var_dump($argv);

            $response = $client->request('GET', 'http://local-testxekera.com/wp-admin/admin-ajax.php?action=load_resources&query='.$argv[4]);

            $two = microtime(1);

            echo "ResponseTime With keyword ".$argv[4]." search is: ".$response->getStatusCode()." and \r\n";

            $body = json_decode($response->getBody());
            echo "\r\n";
            echo "\r\n";

            echo 'Total records:'.count($body->resources);

            //echo $response->getBody();

            echo "\r\n";

            echo 'Total Request time: '. ( $two - $one );

            $stack = [];

            $this->assertEmpty($stack);

            //echo $client->request('GET', '/')->getStatusCode();
        }

    }
//    class ResourcesPage extends resourcesPageBrowserStack
//    {
//        public function testResourcePage(){
//            // Create a mock and queue two responses.
//            $mock = new MockHandler([
//                new Response(200, ['X-Foo' => 'Bar'], 'Hello, World'),
//                new Response(202, ['Content-Length' => 0]),
//                new RequestException('Error Communicating with Server', new Request('GET', 'test'))
//            ]);
//
//            $handlerStack = HandlerStack::create($mock);
//            $client = new Client();
//            $one = microtime(1);
//
//            $response = $client->request('GET', 'http://local-testxekera.com/wp-admin/admin-ajax.php?action=load_resources&query=');
//
//            $two = microtime(1);
//
//            echo "Response Without Search is: ".$response->getStatusCode()." and \r\n";
//
//            echo "\r\n";
//
//            $body = json_decode($response->getBody());
//            echo "\r\n";
//
//            echo 'Total records:'.count($body->resources);
//            echo "\r\n";
//
//            echo 'Total Request time: '. ( $two - $one );
//
//            $stack = [];
//
//            $this->assertEmpty($stack);
//
//            //echo $client->request('GET', '/')->getStatusCode();
//        }
//
//        public function testResourcePageWithSearch(){
//            global $argv;
//
//            // Create a mock and queue two responses.
//            $mock = new MockHandler([
//                new Response(200, ['X-Foo' => 'Bar'], 'Hello, World'),
//                new Response(202, ['Content-Length' => 0]),
//                new RequestException('Error Communicating with Server', new Request('GET', 'test'))
//            ]);
//
//            $handlerStack = HandlerStack::create($mock);
//            $client = new Client();
//            $one = microtime(1);
//
////            var_dump($argv);
//
//            $response = $client->request('GET', 'http://local-testxekera.com/wp-admin/admin-ajax.php?action=load_resources&query='.$argv[4]);
//
//            $two = microtime(1);
//
//            echo "ResponseTime With keyword ".$argv[4]." search is: ".$response->getStatusCode()." and \r\n";
//
//            $body = json_decode($response->getBody());
//            echo "\r\n";
//            echo "\r\n";
//
//            echo 'Total records:'.count($body->resources);
//
//            //echo $response->getBody();
//
//            echo "\r\n";
//
//            echo 'Total Request time: '. ( $two - $one );
//
//            $stack = [];
//
//            $this->assertEmpty($stack);
//
//            //echo $client->request('GET', '/')->getStatusCode();
//        }
//
//    }