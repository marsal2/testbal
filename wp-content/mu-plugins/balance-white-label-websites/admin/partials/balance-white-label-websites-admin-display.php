<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_White_Label_Websites
 * @subpackage Balance_White_Label_Websites/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
