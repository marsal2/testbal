<?php


function render_m19_vertical_logos( $module_data, $background_color = '#eee' ) {
	$output = '';

	if ( !empty( $module_data['logos'] ) && count( $module_data['logos'] ) > 0 && !empty( $module_data['logos'][0]['uploadimage']['image']['fullpath'] ) ) {
		$output .= '<!-- M19: VERTICAL LOGOS -->';
		$output .= '<section aria-label="logos for ' .str_replace(array("'",'"'),'', $module_data["title"]). '" style="background-color: ' . $background_color . ';" class="block thumbnail-holder logotype-holder">';
		$output .= '    <div class="container">';
		$output .= '      <div class="row">';
		if ( !empty( $module_data['title'] ) ) {
			$output .= '        <article class="article" aria-label="article module">';
			$output .= '          <h1 class="text-info text-center">' . $module_data['title'] . '</h1>';
			$output .= '        </article>';
		}
		$output .= '        <div class="thumbnail-holder">';
		foreach ( $module_data['logos'] as $key => $logo ) {
			if ( !empty( $logo['uploadimage']['image']['fullpath'] ) ) {
				$title = !empty( $logo['title'] ) ? $logo['title'] : '';
				$img = '<img src="'.$logo['uploadimage']['image']['fullpath'].'" alt="'.$title.'" title="'.$title.'">';
				$output .= '          <div class="col">';
				$output .= '            <div class="thumbnail">';
				$output .= '              <div class="img-holder">';
				if ( !empty( $logo['link'] ) ) {
				  if ( !empty( $logo['link']['url']) || !empty( $logo['link']['url_page_id'] ) ) {
					  $output .= sprintf(build_link( $logo['link']), '', $img);
				  }
				}
			  else {
				  $output .= $img;
			  }
				$output .= '              </div>';
				$output .= '            </div>';
				$output .= '          </div>';
			}
		}
		$output .= '        </div>';
		$output .= '      </div>';
		$output .= '    </div>';
		$output .= '  </section>';
		$output .= '<!-- end M19: VERTICAL LOGOS -->';
	}
	return stripslashes( $output );
}
