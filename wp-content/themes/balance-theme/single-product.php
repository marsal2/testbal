<?php
/**
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $additional_body_class;
$additional_body_class = 'page-product';
// get header based on user type
$plain = getHeaderType();
get_header( $plain ); ?>
<article class="text-block article default-content-style" aria-label="article single product">
	<div class="container">
		<div class="row">
		<?php
			/**
			 * woocommerce_before_main_content hook.
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 */
			// do_action( 'woocommerce_before_main_content' );
		?>
		<?php
			$can_buy = false;

			if ( ! is_user_logged_in() && get_post_meta( get_the_ID(), '_product_type_public', true ) ) {
				$can_buy = true;
			} else if ( current_user_can( 'administrator' ) ) {
				$can_buy = true;
			}	else {
				$ptype = array(
					'cu_partner' => get_post_meta( get_the_ID(), '_product_type_cu_partner', true ),
					'normal_cu' => get_post_meta( get_the_ID(), '_product_type_normal_cu', true ),
					'balance' => get_post_meta( get_the_ID(), '_product_type_balance', true )
				);

				foreach ( $ptype as $p => $v ) {
					if ( $v && current_user_can( 'buy_'.$p.'_products' ) ) {
						$can_buy = true;
						break;
					}
				}
			}

			if ( $can_buy ) {
				while ( have_posts() ) {
					the_post();
					wc_get_template_part( 'content', 'single-product' );
				}
			} else {
				wc_add_notice( __( 'You cannot view this product.', 'balance_theme' ), 'error' );
				wc_print_notices();
			}
		?>
		<?php
			/**
			 * woocommerce_after_main_content hook.
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			// do_action( 'woocommerce_after_main_content' );
		?>
		<?php
			/**
			 * woocommerce_sidebar hook.
			 *
			 * @hooked woocommerce_get_sidebar - 10
			 */
			do_action( 'woocommerce_sidebar' );
		?>
		</div>
	</div>
</article>
<?php get_footer( $plain ); ?>
