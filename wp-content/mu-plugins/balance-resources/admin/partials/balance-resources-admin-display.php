<?php

/**
 * Provide a admin area view for the resources global overview
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_Resources
 * @subpackage Balance_Resources/admin/partials
 */

  echo '<div class="wrap">';
  global $resources_cpts;
  echo '<h2>';
    echo 'Resources&nbsp;';
    echo '<a href="' . admin_url( 'edit-tags.php?taxonomy=resource_tag' )  . '" class="page-title-action">Manage Tags</a>';
    echo '&nbsp;';
    echo '<a href="' . admin_url( 'admin.php?page=access_groups_list' )  . '" class="page-title-action">Manage Access To Resources (Access Groups)</a>';
  echo '</h2>';

  echo '<br>';

  //Create an instance of our package class...
  $resource_list_table = new WP_Resources_List_Table( $resources_cpts );
  //Fetch, prepare, sort, and filter our data...
  $resource_list_table->prepare_items();
  echo '<!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->';
  echo '<form id="resources-list" method="get">';
      echo '<!-- For plugins, we also need to ensure that the form posts back to our current page -->';
      echo '<input type="hidden" name="page" value="' . $_REQUEST['page'] . '" />';
      echo '<!-- Now we can render the completed list table -->';
      $resource_list_table->display();
  echo '</form>';
  echo '<br />';

echo '</div><!-- end .wrap -->';
echo '<div class="clear"></div>';

?>

