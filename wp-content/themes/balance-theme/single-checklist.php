<?php
/*
Modules: {"m34[0]":{"name":"M34: Section","params":{"mode":"tmce","media_buttons":true,"quicktags":true}},"m12a[0]":{"name":"M12a: PDF link"}}
*/

global $additional_body_class, $data, $post;
Balance_Resources_Public::increment_resource_view_count( $post->ID, 1, 0 );
$additional_body_class = 'single-checklist';
get_custom_data();

get_header();

get_template_part('partials/downloadable-resource');
echo render_m23a_may_also_like_resources();
get_footer();
