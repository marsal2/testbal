# GetShippingAddressesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_addresses** | [**\qpPlatform\Model\ShippingAddress[]**](ShippingAddress.md) | &lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;An array of shipping addresses. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


