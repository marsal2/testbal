<?php
global $post, $data, $article_page, $total_article_pages;

if ( !empty( $data['multipage_module_module'] ) && !empty( $data['multipage_module_module'][0] ) ) {

  $article_data = $data['multipage_module_module'][0];

  if ( !empty( $article_data['pages'] ) && $total_article_pages > 1){
    $chapters = array();

    foreach($article_data['pages'] as $idx=>$page) {
      $chapters[] = array(
        'title' => $page['title'],
        'slug' => get_relative_permalink($post->ID).'page/'.($idx+1),
        'selected' => ($idx+1) == $article_page
      );
    }
    if( !empty( $article_data['last_page_quiz'] ) ){
      $chapters[] = array(
        'title' => __('Quiz', 'balance'),
        'slug' => get_relative_permalink($post->ID).'page/'.($idx+2),
        'selected' => ($idx+2) == $article_page
      );
    }
    echo render_m65_chapter_select($chapters);
  }
}
