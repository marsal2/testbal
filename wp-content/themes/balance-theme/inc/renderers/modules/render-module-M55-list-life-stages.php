<?php

function render_m55_listed_life_stages( $module_data, $islifestage = true ) {

	global $post;

	$output = '';

	if ( !empty( $module_data['title'] ) || !empty( $module_data['copy'] ) || empty( $module_data['showlifestages'] ) ) {
		$output .= '<!-- M55: LIFE STAGES -->';
		$output .=  '<section aria-label="stages" class="block">';
		$output .=    '<div class="container">';
		$output .=      '<div class="row">';

		if ( !empty( $module_data['title'] ) || !empty( $module_data['copy'] ) ) {
			$output .=        '<article class="article" aria-label="article module ' .str_replace(array("'",'"'),'', $module_data["title"]). '">';
			$output .=          !empty( $module_data['title'] ) ? '<h1 class="h2 text-warning text-center">'.$module_data['title'].'</h1>' : '';
			$output .=          !empty( $module_data['copy'] ) ? apply_filters( 'the_content', html_entity_decode( $module_data['copy'] ) ) : '';
			$output .=        '</article>';
		}

		$life_stages_args = array(
			'numberposts' => 5,
			'post_type' => 'life_stage',
			'meta_query' => array(
				array(
					'key' => '_wp_page_template',
					'value' => ( $islifestage ? 'life_stage' : 'life_experience' ),
				),
			),
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'post_status' => 'publish',
			'post_parent' => ( $islifestage ? 0 : $post->ID ),
		);

		//get all life stages
		$life_stages = get_posts( $life_stages_args );

		if ( empty( $module_data['showlifestages'] ) ) {
			$hasStagesToShow = false;
			$output .=        '<div class="thumbnail-holder">';

			foreach ( $life_stages as $life_stage ) {

				//get edit data from each life stage
				$life_stage_data = get_post_custom( $life_stage->ID );

				$unserialized_data = '';

				if ( isset( $life_stage_data['_page_edit_data'] ) ) {
					$unserialized_data = @unserialize( $life_stage_data['_page_edit_data'][0] );
				}
				if ( $unserialized_data === false ) {
					$unserialized_data = array();
				}

				$life_stage_data = $unserialized_data;

				$title = !empty( $life_stage_data['life_stage_homepage_info_module'][0]['title'] ) ? $life_stage_data['life_stage_homepage_info_module'][0]['title'] : '';
				$buttontext = !empty( $life_stage_data['life_stage_homepage_info_module'][0]['buttontext'] ) ? $life_stage_data['life_stage_homepage_info_module'][0]['buttontext'] : '';
				$imageurl = !empty( $life_stage_data['life_stage_homepage_info_module'][0]['image']['image']['fullpath'] ) ? $life_stage_data['life_stage_homepage_info_module'][0]['image']['image']['fullpath'] : '';
				$bullets = !empty( $life_stage_data['life_stage_homepage_info_module'][0]['bullets'] ) ? $life_stage_data['life_stage_homepage_info_module'][0]['bullets'] : array( 'copy' => '' );

				if ($title == '' && $buttontext == '' && $imageurl == '') break;
				$hasStagesToShow = true;

				$life_stage_url = get_relative_permalink( $life_stage->ID );
                $alt =  !empty( $title ) ? $title : 'Getting Started';
				$output .=                '<div class="col">';
				$output .=                  '<div class="thumbnail">';
				$output .=                    '<div class="img-holder">'.( !empty( $imageurl ) ? '<a href="'.$life_stage_url.'"><img alt="'.$alt.'" src="'.$imageurl.'" width="207" height="200"></a>' : '' ).'</div>';
				$output .=                    '<div class="caption">';
				$output .=                      '<h2 class="h3"><a href="'.$life_stage_url.'">'.( !empty( $title ) ? $title : '' ).'</a></h2>';
				$output .=                      '<ul class="list">';
				foreach ( $bullets as $bullet ) {
					if ( !empty( $bullet['copy'] ) ) {
						$output .=                        '<li>'.do_shortcode( wpautop( $bullet['copy'] ) ).'</li>';
					}
				}
				$output .=                      '</ul>';
				$output .=                      '<div class="btn-holder text-center">';
				$output .=          '<a role="button" aria-label="'.( !empty( $title ) ? $title : '' ).'" href="' . $life_stage_url . '" class="btn btn-primary">' . ( empty( $buttontext ) ? __( 'I&#8217;m Here', 'balance' ) : $buttontext ) . '</a>';
				$output .=     '</div>';
				$output .=                    '</div>';
				$output .=                  '</div>';
				$output .=                '</div>';
			}

			if (!$hasStagesToShow) {
				// This is a special case. Return empty string to escape producing HTML artifacts when no showcase data was present.
				return '';
			}

			$output .=        '</div>';
		}

		$output .=      '</div>';
		$output .=    '</div>';
		$output .=  '</section>';
		$output .= '<!-- end M55: LIFE STAGES -->';
	}

	return stripslashes( $output );
}
