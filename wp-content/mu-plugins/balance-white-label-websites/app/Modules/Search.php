<?php

namespace App\Modules;

/**
 * Class Search
 * @package App\Modules
 */
class Search
{
    /**
     * @var \mysqli
     */
    private $db;

    /**
     * @var
     */
    private $dbTablePrefix;

    /**
     * @var array
     */
     public $resourceTypesArray = ['article' => 'articles', 'calculator' => 'calculators', 'video' => 'videos', 'newsletter' => 'newsletters', 'checklist' => 'checklist', 'podcast' => 'podcasts', 'toolkit' =>'toolkits', 'worksheet' => 'worksheets', 'booklet' => 'booklets'];
//    public $resourceTypesArray = ['article' => 'articles', 'calculator' => 'calculators', 'video' => 'videos', 'newsletter' => 'newsletters', 'podcast' => 'podcasts', 'toolkit' =>'toolkits', 'booklet' => 'booklets'];
    //@todo this is a global which might be available to the WLW via WP or it might not, perhaps it's something to set as .env var as it's a setting of sorts? Unlikely to change, but down the road if another type is added or one removed, this could cause issue
    // should be the same as /balance-website/wp-content/mu-plugins/balance-resources/balance-resources.php line 43

    /**
     * Search constructor.
     * @param \mysqli $db
     * @param string $dbTablePrefix
     */
    public function __construct(\mysqli $db, $dbTablePrefix)
    {
        $this->db = $db;
        $this->dbTablePrefix = $dbTablePrefix;
    }

    /**
     * @return mixed|string|void
     */
    public function getTaxonomies()
    {
        $categoryObj = new \stdClass();
        $this->getCategories($categoryObj);
        $this->utf8_encode_deep($categoryObj);
        header('Cache-Control: no-cache, must-revalidate'); //HTTP 1.1
        header('Pragma: no-cache'); //HTTP 1.0
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        header('Content-Type: application/json;charset=utf-8');

        echo json_encode($categoryObj);
        exit;
    }

    /**
     * @param $siteId
     * @param $searchQuery
     * @param bool $userLoggedIn
     * @param mixed $userId
     * @param $offset
     * @return mixed|string|void
     */
    public function getResources($siteId, $searchQuery, $userLoggedIn = false, $userId = 0, $offset,$resourceType,$sortBy,$tag,$stage)
    {
        $siteId = (is_numeric($siteId) ? $siteId : $this->db->escape_string($siteId));
        $searchQuery = $this->db->escape_string($searchQuery);
        $userId = (is_numeric($userId) ? $userId : $this->db->escape_string($userId));
        $offset = $offset ?: 1;

        if ($searchQuery !== '') {
	        // save search keyword to custom table
            $this->saveSearchKeyword($siteId, $searchQuery, $userId);
        }
        $resourcesObj = new \stdClass();
        $resourcesValue = $this->getResourceItems($siteId, $searchQuery, $userLoggedIn,$offset,$resourceType,$sortBy,$tag,$stage);
        $resourcesObj->resources = $resourcesValue['resourceData'];
        $resourcesObj->resourcesDataCount = $resourcesValue['resourceDataCount'];
        $this->utf8_encode_deep($resourcesObj);
        header('Cache-Control: no-cache, must-revalidate'); //HTTP 1.1
        header('Pragma: no-cache'); //HTTP 1.0
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        header('Content-Type: application/json;charset=utf-8');

        echo json_encode($resourcesObj);
        exit;
    }

    /**
     * @param $siteId
     * @param $searchQuery
     * @param bool $userLoggedIn
     * @param mixed $userId
     * @param $offset
     * @return mixed|string|void
     */
    public function getResourcesPage($siteId, $searchQuery, $userLoggedIn = false, $userId = 0, $offset,$resourceType,$sortBy)
    {
        $siteId = (is_numeric($siteId) ? $siteId : $this->db->escape_string($siteId));
        $searchQuery = $this->db->escape_string($searchQuery);
        $userId = (is_numeric($userId) ? $userId : $this->db->escape_string($userId));

        return $this->getResourceItems($siteId, $searchQuery, $userLoggedIn,$offset,$resourceType,$sortBy,'count');
    }


    private function utf8_encode_deep(&$input)
    {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                $this->utf8_encode_deep($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                $this->utf8_encode_deep($input->$var);
            }
        }
    }

    /**
     * @return array
     */
    private function getTags()
    {
        $sql = "select term_id as id, `name` as tag from {$this->dbTablePrefix}terms join {$this->dbTablePrefix}term_taxonomy using (term_id) where taxonomy = 'resource_tag'";

        $result = $this->returnResult($sql);
        $result = $this->convertRowValsStringToInt($result);

        return $result;
    }

    /**
     * @return array
     */
    private function getLifeStages( $life_stage_type = 'life_stage' )
    {
        $sql = "select P.ID as id, P.post_title as tag from {$this->dbTablePrefix}posts as P join {$this->dbTablePrefix}postmeta as PM on P.ID = post_id  where P.post_type = 'life_stage' and PM.`meta_key` = '_wp_page_template' and PM.meta_value = '$life_stage_type'";
        $result = $this->returnResult($sql);
        $result = $this->convertRowValsStringToInt($result);

        return $result;
    }

    private function convertRowValsStringToInt($array)
    {
        foreach ($array as $k => $v) {
            $array[$k]['id'] = (int)$v['id'];
        }

        return $array;
    }

    /**
     * @return array
     */
    private function getResourceTypesArray()
    {
        return array_keys($this->resourceTypesArray);
    }

    /**
     * @return array
     */
    private function getResourceTypes()
    {
        $resourceTypeArray = [];
        $resourceTypes = $this->getResourceTypesArray();
        $i = 0;
        foreach ($resourceTypes as $resourceType) {
            if ($resourceType === 'worksheet' or $resourceType === 'checklist') {continue;}
            $tmpClass = new \stdClass();
            $tmpClass->id = $i;
            $tmpClass->tag = $resourceType;
            $resourceTypeArray[] = $tmpClass;
            $i++;
        }

        return $resourceTypeArray;
    }

    /**
     * @param $siteId
     * @param $searchQuery
     * @param $userLoggedIn
     * @return array
     */
//    private function getResourceItems($siteId, $searchQuery, $userLoggedIn)
//    {
//        $searchQuery = str_replace('\\', "", $searchQuery);
//        $unquotedQuery = str_replace('"', "", $searchQuery);
//        $userLoggedInVal = $this->getUserAccessLevel($userLoggedIn);
//        $select = "select distinct P.ID, P.post_title as title, P.post_date_gmt as `date`, P.post_type, P.post_name, match (RS.html, RS.title) against ('{$searchQuery}') AS relevancy, (P.post_title = '{$unquotedQuery}') AS title_match, (P.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match";
//        $join = " join {$this->dbTablePrefix}resources RS on RS.wp_post_id = P.ID";
//        $join .= " left join {$this->dbTablePrefix}postmeta PM on PM.post_id = P.ID";
//        $join .= " left join {$this->dbTablePrefix}postmeta MM on MM.post_id = P.ID";
//        $join .= " left join {$this->dbTablePrefix}postmeta MD on MD.post_id = P.ID";
//        $from = " from {$this->dbTablePrefix}posts as P";
//        $where = " where P.post_status = 'publish' and MM.meta_key = 'list_in_search' and MM.meta_value = 'true' and PM.meta_key = 'access_level' and PM.meta_value <= '{$userLoggedInVal}' and MD.meta_key = '_page_edit_data'";
//        if ($searchQuery !== '') {
//            $where .= " and (match (RS.html, RS.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
//        }
//        list($join, $where) = $this->setWhitelabelConditions($siteId, $join, $where);
//        $order = " order by title_match desc, title_rough_match desc, relevancy desc";
//        $sql = $select . $from . $join . $where . $order;
//        return $this->returnResourcesResults($sql, $siteId);
//    }

    private function getResourceItemsOld($siteId, $searchQuery, $userLoggedIn,$offset)
    {
        $searchQuery = str_replace('\\', "", $searchQuery);
        $unquotedQuery = str_replace('"', "", $searchQuery);

        $userLoggedInVal = $this->getUserAccessLevel($userLoggedIn);

        $select = "select distinct P.ID, P.post_title as title, P.post_date_gmt as `date`, P.post_type, P.post_name, match (RS.html, RS.title) against ('{$searchQuery}') AS relevancy, (P.post_title = '{$unquotedQuery}') AS title_match, (P.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match";

        $join = " join {$this->dbTablePrefix}resources RS on RS.wp_post_id = P.ID";
        $join .= "  left join {$this->dbTablePrefix}postmeta PM on PM.post_id = P.ID AND PM.meta_value <='{$searchQuery}' AND PM.meta_key = 'access_level' AND PM.meta_key = 'list_in_search' ";
        $join .= "  left join {$this->dbTablePrefix}postmeta MM on MM.post_id = P.ID AND MM.meta_value = 'true'";
        $join .= "  left join {$this->dbTablePrefix}postmeta MD on MD.post_id = P.ID AND MD.meta_value = '_page_edit_data'";
        $from = " from {$this->dbTablePrefix}posts as P";
        $where = " where P.post_status = 'publish'" ;
//        and MM.meta_key = 'list_in_search' and MM.meta_value = 'true' and PM.meta_key = 'access_level' and PM.meta_value <= '{$userLoggedInVal}' and MD.meta_key = '_page_edit_data'";
        if ($searchQuery !== '') {
            $where .= " and (match (RS.html, RS.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
        }
        list($join, $where) = $this->setWhitelabelConditions($siteId, $join, $where);

        //To count the result set of above query to apply pagination
//        $query = $this->db->query($select . $from . $join . $where);
//        $result = [];
//        while ($row = $query->fetch_assoc()) {
//            $result[] = $row;
//        }
        //$limit = ceil(count($result)/9);
        //print_r(ceil($limit));die;
        $limit = 9;
        $offset = $offset-1;
        if($offset != 0){
            $limit = (9-1) * $offset;
        }
        //$order = " order by title_match desc, title_rough_match desc, relevancy desc limit {$limit} offset {$offset}";

        $order = " order by title_match desc, title_rough_match desc, relevancy desc limit {$offset},{$limit}";

        $sql = $select . $from . $join . $where . $order;
        return $this->returnResourcesResults($sql, $siteId);
    }

    private function getResourceItems($siteId, $searchQuery, $userLoggedIn,$offset,$resourceType,$sortBy,$tag,$stage)
    {
        global $wpdb;
        $searchQuery = str_replace('\\', "", $searchQuery);
        $unquotedQuery = str_replace('"', "", $searchQuery);
        //$userLoggedInVal = $this->getUserAccessLevel($userLoggedIn);

        $select = "select distinct P.ID, P.post_title as title, P.post_date_gmt as `date`, P.post_type, P.post_name, match (RS.html, RS.title) against ('{$searchQuery}') AS relevancy, (P.post_title = '{$unquotedQuery}') AS title_match, (P.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match";
        $selectCount = "select Count(DISTINCT P.id) AS pages";
        $join = " join {$this->dbTablePrefix}resources RS on RS.wp_post_id = P.ID";

         if($stage != ''){
             $join .=" join life_stage_type AS l ON (l.lifestagetype in ({$stage}) AND l.postid = RS.ID) ";
        }

        $join .= "  left join {$this->dbTablePrefix}postmeta PM on PM.post_id = P.ID AND PM.meta_value <='{$searchQuery}' AND PM.meta_key = 'access_level' AND PM.meta_key = 'list_in_search' ";
        $join .= "  left join {$this->dbTablePrefix}postmeta MM on MM.post_id = P.ID AND MM.meta_value = 'true'";
        $join .= "  left join {$this->dbTablePrefix}postmeta MD on MD.post_id = P.ID AND MD.meta_value = '_page_edit_data'";
        $from = " from {$this->dbTablePrefix}posts as P";
        $where = " where P.post_status = 'publish'" ;
        if ($searchQuery !== '') {
            $where .= " and (match (RS.html, RS.title) against ('{$searchQuery}' in boolean mode) or match (P.post_excerpt) against ('{$searchQuery}' in boolean mode))";
        }
        if($resourceType !== ''){
            $where .= " and P.post_type='{$resourceType}' ";
        }

        if($tag != ''){
             $where .= " and  P.ID in (select TR.object_id from {$this->dbTablePrefix}term_taxonomy as TT  join {$this->dbTablePrefix}term_relationships as TR on TR.term_taxonomy_id = TT.term_taxonomy_id where TT.taxonomy = 'resource_tag' and TT.term_id in ({$tag})) ";
        }

       

        // $select = "SELECT Distinct  (w.title = '{$unquotedQuery}') AS title_match, match (w.html, w.title) against ('{$searchQuery}') AS relevancy, (w.post_title LIKE '%{$unquotedQuery}%') AS title_rough_match, w.post_title, w.level_of_access, w.list_in_search, w.page_order, w.title, w.type, w.slug, w.wp_post_id";
        //     $from = " FROM wp_resources as w";
        //     $where = " WHERE  w.status = 'publish'  AND w.list_in_search = 'true' AND w.level_of_access = '100'";
        //     if ($searchQuery != '') {
        //         $where .= " and (match (w.html, w.title) against ('{$searchQuery}' in boolean mode))";
        //     }
        //     if($rtypes != '0'){
        //         $where .= " and w.type = '$rtypes'";
        //         $where .= " and P.post_type = '$rtypes'";
        //     }
        //     if($rtypes != '0'){
        //         $from .=", wp_posts AS P";
        //         //$where .= " and P.post_type = '$rtypes'";
        //     }
        //     if($dtype !== '0'){
        //         $from .=", life_stage_type AS l";
        //         $where .= " AND l.lifestagetype = '$dtype' AND l.postid = w.ID";
        //     }
        //     if($tags !='0'){
        //         $from .= " , wp_term_relationships as wtr ";
        //         $where .= " AND  w.wp_post_id = wtr.object_id AND wtr.term_taxonomy_id IN ('$advancedkeywords') ";
        //      }
        //     $order = " order by title_match desc, title_rough_match desc, relevancy desc";
        //     //$limit = 'limt 0, 9';
        //     $query = $select . $from . $where . $order .' limit '.$start.','. $limit;
        //     $check = $db->prepare($query);


            list($join, $where) = $this->setWhitelabelConditions($siteId, $join, $where);
            //Custom Pagination
            $limit = 9;
            $order = "";
            if ($offset == 1) {
                $offset = $offset - 1;
            } else {
                $offset = ($offset - 1) * $limit;
            }
            //$order = " order by title_match desc, title_rough_match desc, relevancy desc limit {$limit} offset {$offset}";
            //$order = " order by title_match desc, title_rough_match desc, relevancy desc limit {$offset},{$limit}";

            if ($sortBy == '-relevance') {
                $order .= "order by relevancy desc";
            }
            if ($sortBy == '-date') {
                $order .= "order by date desc";
            }
//        else{
//            //$order .= " limit {$offset},{$limit}";
//        }
            $order .= " limit {$offset},{$limit}";

            //echo $order;
            $sql = $select . $from . $join . $where . $order;
            // echo $sql;
            // exit;

            $sqlCount = $selectCount . $from . $join . $where;

            $resourcesDataCount = $wpdb->get_results( $sqlCount );

            return ['resourceDataCount'=>@$resourcesDataCount[0]->pages,'resourceData'=>$this->returnResourcesResults($sql, $siteId)];

    }



    /**
     * @param $categoryObj
     */
    private function getCategories($categoryObj)
    {
        $categoryObj->life_stages = $this->getLifeStages();
        $categoryObj->life_experiences = $this->getLifeStages( 'life_experience' );
        $categoryObj->resource_types = $this->getResourceTypes();
        $categoryObj->tags = $this->getTags();
    }

    /**
     * @param $sql
     * @return array
     */
    private function returnResult($sql)
    {
        $query = $this->db->query($sql);
        $result = [];
        while ($row = $query->fetch_assoc()) {
            $result[] = $row;
        }

        return $result;
    }

    /**
     * @param $postId
     * @return array
     */
    private function getResourceLifeStages($postId, $postType, $tags, $life_stage_type = 'life_stage')
    {
        $accessGroups = $this->getResourceAccessGroups($postId, $postType, $tags);

        $result = [];
        for ($i = 0; $i < count($accessGroups); $i++) {
            $sql = "select PM.post_id from {$this->dbTablePrefix}postmeta as PM join {$this->dbTablePrefix}postmeta as M on PM.post_id = M.post_id where M.meta_key = '_wp_page_template' and M.meta_value = '$life_stage_type' and PM.meta_value like '%\"accessgroups\";s:%:\"{$accessGroups[$i]['wp_post_id']}\";}}s:17:\"whole_edit_screen\";%'";
            $stages = $this->returnResult($sql);
            if ($stages) {
                $result[] = $stages[0];
            }
        }
        $return = [];
        if ($result) {
            for ($i = 0; $i < count($result); $i++) {
                $return[] = (int)$result[$i]['post_id'];
            }
        }

        return $return;
    }

    /**
     * @param $postId
     * @return array
     */
//    private function getResourceTags($postId)
//    {
//        $sql = "select T.term_id from {$this->dbTablePrefix}terms as T left join {$this->dbTablePrefix}term_taxonomy as TT on T.term_id = TT.term_id left join {$this->dbTablePrefix}term_relationships as TR on TR.term_taxonomy_id = TT.term_taxonomy_id where TT.taxonomy = 'resource_tag' and TR.object_id = '{$postId}'";
//        $query = $this->db->query($sql);
//        $result = [];
//        while ($row = $query->fetch_assoc()) {
//            $result[] = (int)$row['term_id'];
//        }
//
//        return $result;
//    }
    /**
     * @param $postId
     * @return array
     */
    private function getResourceTags($postId)
    {
        $sql = "select T.term_id from {$this->dbTablePrefix}terms as T join {$this->dbTablePrefix}term_taxonomy as TT on T.term_id = TT.term_id join {$this->dbTablePrefix}term_relationships as TR on TR.term_taxonomy_id = TT.term_taxonomy_id where TT.taxonomy = 'resource_tag' and TR.object_id = '{$postId}'";
        $query = $this->db->query($sql);
        $result = [];
        while ($row = $query->fetch_assoc()) {
            $result[] = (int)$row['term_id'];
        }

        return $result;
    }
    /**
     * @param $postId
     * @return string
     */
    private function getResourceTagsName($postId)
    {
        $sql = "select T.name from {$this->dbTablePrefix}terms as T join {$this->dbTablePrefix}term_taxonomy as TT on T.term_id = TT.term_id join {$this->dbTablePrefix}term_relationships as TR on TR.term_taxonomy_id = TT.term_taxonomy_id where TT.taxonomy = 'resource_tag' and TR.object_id = '{$postId}'";
        $query = $this->db->query($sql);
        $result = '';
        while ($row = $query->fetch_assoc()) {
            $result.= '<div class="btn-tag same-height same-height-left same-height-right" style="height: 105px;">
                '. $row['name'].'
                        </div>';
        }

        return $result;
    }

    /**
     * @param $postId
     * @return int
     */
    private function getViews($postId, $siteId, $page = 1) //@todo need to know what to do about the sub-pages return Page 1 counts or sum of all pages counts or highest count page or ???
    {
        $sql = "select view_count from {$this->dbTablePrefix}resources_view_count where wp_post_id = {$postId} and white_label_website_id = {$siteId} and page = {$page}";
        $viewCount = $this->returnResult($sql);
        if ($viewCount) {
            return (int)$viewCount[0]['view_count'];
        }

        return 0;
    }

    private function getUserAccessLevel($userLoggedIn)
    {
        if ($userLoggedIn) {
            return 200;
        }

        return 100;
    }

    /**
     * @param $siteId
     */
    public function getWhiteLabelAccessGroups($siteId)
    {

        $siteId = $this->db->escape_string($siteId);
        $sql = "select GROUP_CONCAT(AG.wp_post_id) as access_groups from {$this->dbTablePrefix}white_label_website_access_group as WLWAG left join {$this->dbTablePrefix}access_groups as AG on AG.wp_post_id = WLWAG.access_group_id where WLWAG.white_label_website_id = {$siteId}";
         $accessGroupsList =$this->returnResult($sql);

         return $accessGroupsList[0]['access_groups'];
    }

    /**
     * @param $siteId
     * @param $join
     * @param $where
     * @return array
     */
    private function setWhitelabelConditions($siteId, $join, $where)
    {
        if ($siteId !== 0) {
            $whiteLabelAccessGroups = $this->getWhiteLabelAccessGroups($siteId);
            $join .= " left join {$this->dbTablePrefix}access_groups AG on AG.wp_post_id in ($whiteLabelAccessGroups)";
            $join .= " left join {$this->dbTablePrefix}term_relationships as R on R.object_id = P.ID";
            $join .= " left join {$this->dbTablePrefix}term_taxonomy as TT on TT.term_taxonomy_id = R.term_taxonomy_id";

            $where .= ' and ((P.ID in (AG.post__in) and P.ID not in (AG.post__not_in))';
            $where .= ' or (TT.term_id in (AG.term__in) and TT.term_id not in (AG.term__not_in))';
            $where .= ' or find_in_set(P.post_type, AG.post_type))';
            $where .= " and TT.taxonomy = 'resource_tag'";
        }

        return array($join, $where);
    }

    /**
     * @param $sql
     * @return array
     */
    private function returnResourcesResults($sql, $siteId)
    {
//        echo $sql;
        $query = $this->db->query($sql);
        $result = [];
        if ($query) {
            while ($row = $query->fetch_assoc()) {
                if (array_key_exists($row['post_type'], $this->resourceTypesArray)) {
                    $row['views'] = $this->getViews($row['ID'], $siteId);
                    $row['url'] = '/resources/' . $this->getPluralType($row['post_type']) . '/' . $row['post_name'];
                    $row['tags'] = $this->getResourceTags($row['ID']);
                   // $row['tagsName'] = $this->getResourceTagsName($row['ID']);
                    $row['life_stages'] = $this->getResourceLifeStages($row['ID'], $row['post_type'], $row['tags']);
                    $row['life_experiences'] = $this->getResourceLifeStages($row['ID'], $row['post_type'], $row['tags'], 'life_experience');
                    $row['resource_type'] = $this->getResourceTypeId($row['post_type']);
                    $row['resource_type_name'] = '' . $row['post_type'];
                    unset($row->ID, $row->post_name);
                    $result[] = $row;
                }
            }
        }
        $this->db->close();

        return $result;
    }

    private function getPluralType($postType)
    {
        return $this->resourceTypesArray[$postType];
    }

    private function getResourceTypeId($arrayType)
    {
        return array_search($arrayType, array_keys($this->resourceTypesArray), false);
    }

    /**
     * @param $postId
     * @param $postType
     * @param $tags
     * @return array
     */
    private function getResourceAccessGroups($postId, $postType, $tags)
    {
        $sql = "select distinct wp_post_id from {$this->dbTablePrefix}access_groups as AG where (find_in_set({$postId}, post__in) and not find_in_set({$postId}, post__not_in) or find_in_set('{$postType}', post_type)";
        if (!empty($tags)) {
            $sql .= " or (";
            $first = true;
            foreach ($tags as $tag) {
                if (!$first) {
                    $sql .= " or ";
                }
                $sql .= "(find_in_set({$tag}, term__in) and not find_in_set({$tag}, term__not_in))";
                $first = false;
            }
            $sql .= ")";
        }
        $sql .= ')';

        $result = $this->returnResult($sql);
        return $result;
    }

    /**
     * @param $siteId
     * @param $searchKeyword
     * @param $userId
     */
    private function saveSearchKeyword($siteId, $searchKeyword, $userId)
    {
    	$sql = "insert into {$this->dbTablePrefix}resources_keyword_search(white_label_website_id, keyword, user_id, inserted_at) VALUES ('$siteId', '$searchKeyword', '$userId', NOW())";
    	$this->db->query($sql);
    }
}
