<?php

function render_m38_section_copy( $module_data ) {
  $output = '';
  if ( !empty( $module_data['copy'] ) ) {
    $output .= '<article class="text-block article default-content-style" aria-label="m38 article module ' .str_replace(array("'",'"'),'', $module_data["title"]). '">
      <div class="container">
        <div class="row">
          '.apply_filters( 'the_content', html_entity_decode( $module_data['copy'] ) ).'
        </div>
      </div>
    </article>';
  }
  return stripslashes( $output );
}