<?php
/*
 *  WP Edit module: White Label Websites Resources
 */
function white_label_websites_resources_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
  global $data;

  if( empty( $data['wlw_resources_module'] ) ){
    $data = init_array_on_var_and_array_key($data, 'wlw_resources_module');
    $data['wlw_resources_module'][ $key ] = array(
      'title' => '',
      'accesstoresources' => ''
    );
  }

  $autocomplete_args_resources = array(
	'post_type' => 'access_group',
	'posts_per_page' => -1,
	'post_status' => 'publish'
  );
  $output = '';
  $output .= '<a name="wlw-resources-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper wlw-resources-module-wrapper-'. $key .' hidden" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom wlw-resources-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-expanded" data-toggle-title="'. __( 'Expand', 'balance' ) .'" href="javascript:;">'. __( 'Collapse', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside">';

   $output .= '        <p>';
   $output .= '          <label><b>'. __( 'Access to Resources (Resources Groups)', 'balance' ) .':</b></label>';
   $output .=              multi_autocomplete_field( $data['wlw_resources_module'][ $key ]['accesstoresources'], 'wlw_resources_module['.$key.'][accesstoresources]', $autocomplete_args_resources );
   $output .= '        </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;
}
?>
