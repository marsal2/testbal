<?php

/*
 *  WP Edit module: M19
 *  Description: Module with line of vertical logos. M19 – Headline with horizontal line of logos
 */

function m19_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
	global $data;

	$output = simple_edit_module( $key, 'm19', array(
			array(
				'type' => 'text',
				'name' => 'title',
				'label' => 'Title',
				'default_value' => ''
			),
			array(
				'type' => 'instruction',
				'name' => 'logos-instruction',
				'label' => 'Logos',
				'default_value' => '',
				'additional_params' => array(
					'class' => '',
					'content' => ' Change order by dragging items up & down'
				)
			),
			array(
				'type' => 'repeater',
				'name' => 'logos',
				'label' => 'Logo',
				'default_value' => array(),
				'max' => 7,
				'fields' => array(
					array(
						'type' => 'upload',
						'name' => 'uploadimage',
						'label' => 'Logo image',
						'default_value' => array(),
						'additional_params' => array(
							'',
							'Image',
							'image'
						)
					),
					array(
						'type' => 'text',
						'name' => 'title',
						'label' => 'Logo title',
						'default_value' => ''
					),
					array(
						'type' => 'links_to',
						'name' => 'link',
						'label' => 'Logo link',
						'default_value' => array()
					),
				)
			)
		), $data, $module_title, $visible_on );
	return $output;
}
