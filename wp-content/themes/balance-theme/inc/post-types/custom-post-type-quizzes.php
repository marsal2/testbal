<?php
/*
 * Balance Custom post type: Quizzes
 *
 */

 /*
 * custom post type registration
 */

function cpt_quizzes() {
  $labels = array(
    'name'               => __( 'Quizzes', 'balance' ),
    'singular_name'      => __( 'Quiz', 'balance' ),
    'add_new'            => __( 'Add New', 'balance' ),
    'add_new_item'       => __( 'Add New Quiz', 'balance' ),
    'edit_item'          => __( 'Edit Quiz', 'balance' ),
    'new_item'           => __( 'New Quiz', 'balance' ),
    'all_items'          => __( 'Quizzes', 'balance' ),
    'view_item'          => __( 'View Quiz', 'balance' ),
    'search_items'       => __( 'Search Quizzes', 'balance' ),
    'not_found'          => __( 'No Quizzes found', 'balance' ),
    'not_found_in_trash' => __( 'No Quizzes found in the Trash', 'balance' ),
    'parent_item_colon'  => '',
    'menu_name'          => __( 'Quizzes', 'balance' )
  );
  $args = array(
    'labels'        => $labels,
    'description'   => __( 'Data about Quizzes', 'balance' ),
    'public'        => true,
    'supports'      => array( 'title' ),
    'has_archive'   => false,
    'exclude_from_search' => false,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 28,
    'menu_icon' => 'dashicons-clipboard',
    'capability_type' => 'post',
    'hierarchical' => false,
  );
  register_post_type( 'quiz', $args );
}
add_action( 'init', 'cpt_quizzes' );

/*
 * custom post type update messages
 */
function quizzes_updated_messages( $messages ) {
    global $post, $post_ID;
    $messages['quizzes'] = array(
        0 => '',
        1 => __( 'Quizzes updated.', 'balance' ),
        2 => __( 'Custom field updated.', 'balance' ),
        3 => __( 'Custom field deleted.', 'balance' ),
        4 => __( 'Quizzes post updated.', 'balance' ),
        5 => isset($_GET['revision']) ? sprintf( __('Quizzes post restored to revision from %s', 'balance' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
        6 => __( 'Quizzes saved.', 'balance' ),
        7 => __( 'Quizzes saved.', 'balance' ),
        8 => '',
        9 => '',
        10 => '',
    );
    return $messages;
}
add_filter( 'post_updated_messages', 'quizzes_updated_messages' );

?>
