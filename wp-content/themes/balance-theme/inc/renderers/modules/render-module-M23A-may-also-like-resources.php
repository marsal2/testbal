<?php

function render_m23a_may_also_like_resources( $module_data = '' ) {

  global $post;
  $tags = wp_get_post_terms( $post->ID, 'resource_tag' , array( 'fields' => 'ids' ) );
  $related_posts = get_related_posts_by_resource_tags( $post->ID , 4);

  if( count ( $related_posts ) == 0 )
    return '';

  $output = [];
  foreach( $related_posts as $key => $rel_post ) {
    $cl_hidden_mobile = $key%2 == 0 ? '' : 'hidden-xs';

    $cl_same_height = '';
    if( $key == 0 )
      $cl_same_height = 'same-height-left';
    else if ( $key + 1 == count ( $related_posts ) )
      $cl_same_height = 'same-height-right';

    $rel_post_link = get_relative_permalink( $rel_post->ID );
    $output[] =
      '<div class="col-xs-6 col-sm-3 ' . $cl_hidden_mobile . '">' . "\n" .
      '  <article class="info-wrap" aria-label="module for article '.str_replace(array("'",'"'),'', $rel_post->post_title).'">' . "\n" .
      '    <div class="img-holder">' . "\n" .
      '      <span class="icon-' . $rel_post->post_type . '">' . "\n" .
      '      </span>' . "\n" .
      '    </div>' . "\n" .
      '    <p style="height: 25px;" class="same-height ' . $cl_same_height . '">' . "\n" .
              $rel_post->post_title . "\n" .
      '    </p>' . "\n" .
      '      <a  aria-label="Read more interested number '. $rel_post->ID .'" href="' . $rel_post_link . '" class="text-read-more text-warning">' . "\n" .
      '         READ MORE' . "\n" .
      '      </a>' . "\n" .
      '  </article>' . "\n" .
      '</div>';
  }

  return
    '<section aria-label="You May Also Be Interested In" class="inner-block first-inner-child">' . "\n" .
    '  <div class="container">' . "\n" .
    '    <div class="row">' . "\n" .
    '      <h1 class="text-info text-center">You May Also Be Interested In</h1>' . "\n" .
    '      <div class="col-holder same-height-holder">' . "\n" .
            implode ( "\n", $output ) . "\n" .
    '      </div>' . "\n" .
    '    </div>' . "\n" .
    '  </div>' . "\n" .
    '</section>';
}

//$output = '';
//
//$output .= '<section class="inner-block first-inner-child">';
//$output .= '  <div class="container">';
//$output .= '    <div class="row">';
//$output .= '      <h1 class="text-info text-center">You May Also Be Interested In</h1>';
//$output .= '      <div class="col-holder same-height-holder">';
//$output .= '        <div class="col-xs-6 col-sm-3">';
//$output .= '          <article class="info-wrap">';
//$output .= '            <div class="img-holder"><span class="icon-mini-play"></span></div>';
//$output .= '            <p style="height: 25px;" class="same-height same-height-left">Lorem ipsum dolor</p><a href="#" class="text-read-more text-warning">READ MORE</a>';
//$output .= '          </article>';
//$output .= '        </div>';
//$output .= '        <div class="col-xs-6 col-sm-3 hidden-xs">';
//$output .= '          <article class="info-wrap">';
//$output .= '            <div class="img-holder"><span class="icon-calculator"></span></div>';
//$output .= '            <p style="height: 25px;" class="same-height">Lorem ipsum dolor</p><a href="#" class="text-read-more text-warning">READ MORE</a>';
//$output .= '          </article>';
//$output .= '        </div>';
//$output .= '        <div class="col-xs-6 col-sm-3">';
//$output .= '          <article class="info-wrap">';
//$output .= '            <div class="img-holder"><span class="icon-wifi"></span></div>';
//$output .= '            <p style="height: 25px;" class="same-height">Lorem ipsum dolor</p><a href="#" class="text-read-more text-warning">READ MORE</a>';
//$output .= '          </article>';
//$output .= '        </div>';
//$output .= '        <div class="col-xs-6 col-sm-3 hidden-xs">';
//$output .= '          <article class="info-wrap">';
//$output .= '            <div class="img-holder"><span class="icon-list"></span></div>';
//$output .= '            <p style="height: 25px;" class="same-height same-height-right">Lorem ipsum dolor</p><a href="#" class="text-read-more text-warning">READ MORE</a>';
//$output .= '          </article>';
//$output .= '        </div>';
//$output .= '      </div>';
//$output .= '    </div>';
//$output .= '  </div>';
//$output .= '</section>';
//
//return $output;
?>
