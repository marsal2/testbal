<?php
/*
Template Name: T01: Homepage
Modules: {"m1[0]":{"name":"M1: Hero"},"m34[0]":{"name":"M34: Section"},"m56_m17[0]":{"name":"M17: Four messages"},"m55[0]":{"name":"M55: Life Stages"},"m19[0]":{"name":"M19: Vertical logos"}}
*/
?>
<?php
global $additional_body_class, $data, $post;
$additional_body_class = 'homepage';
get_custom_data();

get_header();

/* M01 renderer */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0], 'tall' );
}

/* M34 renderer */
if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][0] ) ) {
	echo render_m34_section_title_copy( $data['m34_module'][0] );
}

/* M56/M17 renderer */
if ( !empty( $data['m56_module'] ) && !empty( $data['m56_module'][0] ) ) {
	echo render_m56_m17_three_four_messages( $data['m56_module'][0] );
}

/* M55 renderer */
if ( !empty( $data['m55_module'] ) && !empty( $data['m55_module'][0] ) ) {
	echo render_m55_listed_life_stages( $data['m55_module'][0] );
}

/* M19 renderer */
if ( !empty( $data['m19_module'] ) && !empty( $data['m19_module'][0] ) ) {
	echo render_m19_vertical_logos( $data['m19_module'][0] );
}

get_footer();

?>
