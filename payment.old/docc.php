<?php
if(session_id() == '') {
  session_start();
}
require_once('security.php');
$config = require_once('config.php');
// Check if a form has been sent
$postedToken = filter_input(INPUT_POST, 'token');
if(!empty($postedToken)) {
  if(!isTokenValid($postedToken)){
    echo '<script>window.history.back();</script>';
    exit;
  }
} else {
  echo '<script>window.history.back();</script>';
  exit;
}

?>
<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Balance credit card</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?=$config['BASE_URL'];?>/styles/main.css">
  </head>
  <body>
    <!-- Header -->
    <header id="header">
      <div style="background-image: url(https://www.cccssf.org/bankruptcy/images/header_background.png);background-repeat: no-repeat; background-size: cover; background-position: 50% 0; max-height: 50px; letter-spacing: -4px; text-align: right; padding: 10px 5px;" class="header-t">
        <div class="container">
          <div class="row">
            <!-- search form-->
            <ul class="contact-block" style="text-transform: uppercase; letter-spacing: 0; line-height: 0; font-size: 0; display: inline-block; vertical-align: middle; letter-spacing: 1px; margin-right: 11%;">
              <li><a href="tel:8007777526"><span class="icon-phone"></span>800.777.PLAN (7526)</a></li>
            </ul>
          </div>
        </div>
      </div>
      <nav class="navbar navbar-default">
        <div class="container">
          <div class="row">
            <div class="navbar-header" style="margin:30px 0; margin-left: 11%;">
              <a href="http://www.balancepro.org/"><img src="https://www.cccssf.org/bankruptcy/images/logo.svg" width="238" height="51" alt="balance"></a>
            </div>
          </div>
        </div>
      </nav>
    </header>

    <?php
    require_once('paypal.php');
    $paypalconf = include('paypal.conf.php');

    if (isset($_POST) || isset($_GET) ) {
      $_SESSION['initialformsubmit'] = array_merge($_GET, $_POST);
    }

    $amount = $_POST['cardholder-amount'];
    $amount = floatval($amount);
    $name = $_POST['cardholder-name'] . ' ' . $_POST['cardholder-surname'];
    $email = $_POST['cardholder-email'];

    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
      renderError("Invalid email.");
    }

    if(empty($amount) === true || is_numeric($amount) === false) {
      renderError("Missing params.");
    }

    if(empty($name) === true) {
      renderError("Missing params.");
    }

    //Run request and get the secure token response
    $paypalRequest = doPaypal('runPayflowCall', [
      'AMT' => $amount,
      'SECURETOKENID' => uniqid('MySecTokenID-'),
      "BILLTOFIRSTNAME" => $_POST['cardholder-name'],
      "BILLTOLASTNAME" => $_POST['cardholder-surname'],
      "BILLTOSTREET" => $_POST['cardholder-address1'] . (!empty( $_POST['cardholder-address2'] ) ? ', ' . $_POST['cardholder-address2'] : ''),
      "BILLTOCITY" => $_POST['cardholder-city'],
      "BILLTOSTATE" => $_POST['cardholder-state'],
      "BILLTOZIP" => $_POST['cardholder-zip'],
      "BILLTOCOUNTRY" => "US",
      "SHIPTOFIRSTNAME" => $_POST['cardholder-name'],
      "SHIPTOLASTNAME" => $_POST['cardholder-surname'],
      "SHIPTOSTREET" => $_POST['cardholder-address1'] . (!empty( $_POST['cardholder-address2'] ) ? ', ' . $_POST['cardholder-address2'] : ''),
      "SHIPTOCITY" => $_POST['cardholder-city'],
      "SHIPTOSTATE" => $_POST['cardholder-state'],
      "SHIPTOZIP" => $_POST['cardholder-zip'],
      "SHIPTOCOUNTRY" => "US",
    ]);

    if(wasError() !== false) renderError();

    if ($paypalRequest['RESULT'] != 0) {
      echo "Payflow call failed";
      echo "<pre><code>\n" . print_r($paypalRequest,true) . "\n</pre></code>";
      exit(0);
    } else {
      $securetoken = $paypalRequest['SECURETOKEN'];
      $securetokenid = $paypalRequest['SECURETOKENID'];
    }

    $iframe_source = 'https://payflowlink.paypal.com?SECURETOKEN=' . $securetoken . '&SECURETOKENID=' . $securetokenid . '&MODE=' . $paypalconf['mode'];

    ?>
    <div id="paymentviewwrap" style="width:490px; margin: 0 auto;background:url(/styles/ajax-loader.gif) center center no-repeat;">
      <iframe id="paymentview" src='<?php echo $iframe_source; ?>' width='490' height='565' border='0' frameborder='0' scrolling='no' allowtransparency='true'></iframe>
    </div>

    <!-- Footer -->
    <footer id="footer" style="background-image: url(https://www.cccssf.org/bankruptcy/images/footer_background.png);">
      <div class="container">
        <div class="row">
          <div class="logo-holder"><a href="http://www.balancepro.org/"><img src="https://www.cccssf.org/bankruptcy/images/logo-primary.svg" width="198" height="44" alt="Balance"></a></div>
        </div>
      </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script>
      $(document).ready(function() {

         $("iframe#paymentview").on('load', function() {
            $("#paymentviewwrap").css({'background':'none'});
         });
      });
    </script>
  </body>
</html>
