<?php

function render_m24_quiz_registration_form() {
  $user_data = null;

  // check if any data stored in the session for the already submissioned form and display that data so user does not have to input this data every time of trying to solve the quiz
  if ( !empty( $_SESSION['quiz_registration_form_data'] ) ) {
    $user_data = json_decode(stripslashes($_SESSION['quiz_registration_form_data']), true);
  }

  $output = '';
  $output .= '<div class="login-modal">';
  $output .= '<div class="login-form">';
  $output .= '  <fieldset>';
  $output .= '    <style>
                    #registerLegend {
                      text-align: left;
                      padding-top: 15px;
                      color: red;
                    }
                    @media (min-width: 768px) {
                      #registerLegend {
                        font-size: 16px;
                      }
                    }
                    @media (min-width: 1200px) {
                      #registerLegend {
                        font-size: 18px;
                      }
                    }
                    #registerLegend ul {
                      padding-left: 0;
                    }
                    .formRequired, .formOptional {
                      font-size: 30px;
                      vertical-align: text-top;
                    }
                    .formRequired,
                    .formOptional {
                      color:red;
                    }
                    .label {
                      white-space: normal;
                      text-align: left;
                    }
                    .login-modal .login-form .checkbox label {
                      padding-left: 0;
                    }
                    .login-modal .login-form input[type=text],
                    .login-modal .login-form input[type=email] {
                      background-color: #fff;
                    }
                    @media (min-width: 768px) {
                      .login-modal .login-form .label {
                        font-size: 16px;
                      }
                    }
                    @media (min-width: 1200px) {
                      .login-modal .login-form .label {
                        font-size: 18px;
                      }
                    }
                    .jcf-select {
                      text-align: left;
                      width: 100%;
                      height: 40px;
                      border: 1px solid #bbb;
                    }
                    .jcf-select .jcf-select-opener {
                      background: none;
                    }
                    .jcf-select-opener:before {
                      content: "\25be";
                      font-style: normal;
                      font-weight: normal;
                      text-decoration: inherit;
                      color: #000;
                      font-size: 18px;
                      padding-right: 0.5em;
                      position: absolute;
                      top: 6px;
                      right: 0;
                    }
                    .jcf-select .jcf-select-text {
                      line-height: 40px;
                    }
                  </style>';
  $output .= '    <div class="form-block">';
  $output .= '      <div class="input-group">';
  $output .= '        <span class="label"><label for="input1">First Name<span class="formRequired">*</span></label></span>';
  $output .= '        <input type="text" id="input1" required name="register[firstname]" placeholder="" value="' . ( !empty( $user_data['firstname'] ) ? $user_data['firstname'] : '' ) . '">';
  $output .= '      </div>';
  $output .= '      <div class="input-group">';
  $output .= '        <span class="label"><label for="input2">Last Name<span class="formRequired">*</span></label></span>';
  $output .= '        <input type="text" id="input2" required name="register[lastname]" placeholder="" value="' . ( !empty( $user_data['lastname'] ) ? $user_data['lastname'] : '' ) . '">';
  $output .= '      </div>';
  $output .= '      <div class="input-group">';
  $output .= '        <span class="label"><label for="input3">Email</label></span>';
  $output .= '        <input type="Email" id="input3" name="register[email]" value="' . ( !empty( $user_data['email'] ) ? $user_data['email'] : '' ) . '">';
  $output .= '      </div>';
  $output .= '      <div class="input-group">';
  $output .= '        <span class="label"><label for="input4">Street Address</label></span>';
  $output .= '        <input type="text" id="input4" name="register[streetAddress]" placeholder="" value="' . ( !empty( $user_data['streetAddress'] ) ? $user_data['streetAddress'] : '' ) . '">';
  $output .= '      </div>';
  $output .= '      <div class="input-group">';
  $output .= '        <span class="label"><label for="input5">City</label></span>';
  $output .= '        <input type="text" id="input5" name="register[city]" placeholder="" value="' . ( !empty( $user_data['city'] ) ? $user_data['city'] : '' ) . '">';
  $output .= '      </div>';
  $output .= '      <div class="input-group">';
  $output .= '        <span class="label"><label for="input6">State</label></span>';
  $output .= '        <input type="text" id="input6" name="register[state]" placeholder="" value="' . ( !empty( $user_data['state'] ) ? $user_data['state'] : '' ) . '">';
  $output .= '      </div>';
  $output .= '      <div class="input-group">';
  $output .= '        <span class="label"><label for="input7">Zip</label></span>';
  $output .= '        <input type="text" id="input7" name="register[zip]" placeholder="" value="' . ( !empty( $user_data['zip'] ) ? $user_data['zip'] : '' ) . '">';
  $output .= '      </div>';
  $output .= '      <div class="input-group">';
  $output .= '        <span class="label"><label for="input8">Last four digits of account/member number</label></span>';
  $output .= '        <input type="text" id="input8" name="register[memberNumber]" placeholder="" value="' . ( !empty( $user_data['memberNumber'] ) ? $user_data['memberNumber'] : '' ) . '">';
  $output .= '      </div>';
  $output .= '      <div id="registerLegend">';
  $output .= '        <ul style="list-style-type: none;">';
  $output .= '          <li><span class="formRequired">*</span>Required</li>';
  $output .= '        </ul>';
  $output .= '      </div>';
  $output .= '    </div>';
  $output .= '  </fieldset>';
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

function render_m24_quiz_questions( $module_data ) {
  global $data, $woocommerce;
	$output = '';
  $quizid = !empty( $data['multipage_module_module'][0]['last_page_quiz']) ? get_post_meta( $data['multipage_module_module'][0]['last_page_quiz'] )['balance_quiz_id'][0] : get_post_meta( $data['post_ID'] )['balance_quiz_id'][0];

	// check if quiz is payable and if user has a corresponding subscription
  $can_see_quiz = true;
  $user = wp_get_current_user();
	$subscription_id = $module_data['subscription'];
  $registration_on_the_end = !empty($module_data['show_register_form']) ? filter_var($module_data['show_register_form'], FILTER_VALIDATE_BOOLEAN) : false;

  // quiz is payable and subsciption is set for it
  if ( ! empty( $subscription_id ) && $module_data['payable'] ) {
  	if ( ! wcs_user_has_subscription( $user->ID, $subscription_id, 'active' ) ) {
			$can_see_quiz = false;

			$output = '<article name="article" class="text-block article background-white default-content-style" aria-label="article module quiz id-'. get_the_ID() .'">';
			$output .= '  <div class="container">';
			$output .= '    <div class="row">';
			$output .= '    	<p>' . __( $module_data['buy_first'], 'balance') . '</p>';

  		$output .= '      <a class="btn btn-warning" id="buy-quiz-now" href="' . get_permalink( woocommerce_get_page_id( "checkout" ) ) . '">' . __( 'Buy now', 'balance' ) . '</a>';

			$output .= "<script>
					jQuery('#buy-quiz-now').click(function(e) {
					  e.preventDefault();
						$.ajax({
							url: '" . admin_url('admin-ajax.php') ."',
							data: {
								'action': 'quiz_add_to_cart',
								'product_id': " . $subscription_id . ",
								'quiz_id': " . get_the_ID() . "
							},
							success:function(data) {
								window.location.href = jQuery('#buy-quiz-now').attr('href');
							},
							error: function(errorThrown) { }
						});
					});
		    </script>";

			$output .= '    </div>';
			$output .= '  </div>';
			$output .= '</article>';
  	}
  }

	if ( $can_see_quiz ) {

		$score              = 0;
		$total              = 0;
		$correct            = 0;
		$passed             = false;
		$quiz_results_style = 'display: none;';
		$results            = false;
		$quiz_submitted     = false;

    // quiz was submitted
		if( isset( $_POST['quiz-submission'] ) ) {
			$quiz_submitted     = true;
			$quiz_results_style = '';
			$results            = m24_parse_quiz_results( $_POST, $module_data['quiz'], $registration_on_the_end );
			$score              = $results['score'];
			$total              = $results['total'];
			$correct            = $results['correct'];
			$passed             = $results['passed'];
			$quiz_results       = $results['quiz_results'];
			$partner_domain     = $_GET['partner'];
			$output             .= "<script>";
			$output             .= "  jQuery(document).ready(function(){";
			$output             .= "    jQuery('.quiz-header').hide();";
			// if ($partner_domain) {
			// 	$output           .= "    jQuery.ajax('https://" . $partner_domain . "/log-quiz/" . $_POST['quizid'] . "/" . $correct . "', ";
			// 	$output           .= "      { xhrFields: { withCredentials: true }, crossDomain: true });";
			// }
			$output             .= "  });";
			$output             .= "</script>";
		}

		if ( ! empty( $module_data['quiz'] ) && count( $module_data['quiz'] ) > 0 ) {
			$valid = m24_check_quiz_validity( $module_data['quiz'] );

			// user is logged in, quiz has not been posted, cooldown is needed
			if ( is_user_logged_in() && !$quiz_submitted && $valid != false && $valid['cooldown'] !== false && is_object( $valid['try_again'] ) ) {
				$diff = $valid['try_again']->diff(new DateTime('now'));

				$output = '<section aria-label="quiz questions" class="quiz-questions modules=form-section">';
				$output .= '  <div class="container">';
				$output .= '    <div class="row">';
				$output .=        printReattempt($diff);
				$output .= '    </div>';
				$output .= '  </div>';
				$output .= '</section>';
			}
			else {
				// print score
				$output .= '<article name="balance_article" class="quiz-results text-block article background-white default-content-style" style="' . $quiz_results_style . '" aria-label="article module print score">';
				$output .= '  <div class="container">';
				$output .= '    <div class="row">';
				$output .= '      <h1 class="text-info text-center">' . __('Your Results', 'balance') . '</h1>';
				$output .= '      <p>' . __('Your score was', 'balance') . ' <b class="quiz-percent"> ' . $score . '%</b> (<span class="quiz-number-of-correct">' . $correct . '</span>/<span class="quiz-number-of-questions">' . $total . '</span>).</p>';

				// Print success/fail message

				// If failed
				if ( ! $passed ) {
					$output .= '	  <p>' . $module_data['message_fail'] . '</p>';
				}
				else {
					$output .= '	  <p>' . $module_data['message_success'] . '</p>';
					error_log("Quiz passed [".get_the_ID()."]. User logged in status [" . (is_user_logged_in() ? "Y":"N") . "]");

					if ( is_user_logged_in() && $module_data['has_certificate'] ) {
						// send PDF certificate to user if applicable
						$quiz_pdf = quiz_submit( get_the_ID(), $user->ID, $score, $correct, $total );
						$output .= '<p>Your certificate will be sent to ' . $user->user_email . '.</p>';
					}
				}

				// guest users can solve the quiz as many times as they like
				if ( !is_user_logged_in() ) {
					$output .= '<div class="btn-holder clearfix"><a href="javascript:window.location=window.location" class="btn btn-warning pull-left quiz-retry">' . __('Retry quiz', 'balance') . '</a></div>';
				} else {
					// if there are attempts left and no cooldown is needed, retry is possible
					if ( $valid || ( ! $valid['cooldown'] && $valid['attempts_left'] > 0 ) ) {
						$output .= '<div class="btn-holder clearfix"><a href="javascript:window.location=window.location" class="btn btn-warning pull-left quiz-retry">' . __('Retry quiz', 'balance') . '</a></div>';
					}
					// otherwise notify the user when they can solve the quiz again
					else {
						$diff = $valid['try_again']->diff(new DateTime('now'));
						$output .= printReattempt($diff);
					}
				}

				$output .= '    </div>';
				$output .= '  </div>';
				$output .= '</article>';
				// show results only if user has passed the quiz or if the setting is enabled
				if ( !$quiz_submitted || ( $quiz_submitted && $passed ) || $module_data['show_results'] ) {
					$output .= '<section aria-label="quiz questions" class="quiz-questions modules-form-section">';
					$output .= ' <div class="container">';
					$output .= '   <div class="row">';
					$output .= '      <form id="quiz-form" method="post" class="form-wrap">';
          $output .= '        <div class="quiz-questions">';
					$output .= '          <ol class="questions">';
					$count_of_questions = 0;
					foreach ( $module_data['quiz'] as $question_key => $quiz_question ) {
						if ( !empty( $quiz_question['question'] ) && !empty( $quiz_question['answers'] ) && count( $quiz_question['answers'] ) > 0 ) {
							$output .= '<li>';
							$output .= '<strong class="h3">' . $quiz_question['question'] . '</strong>';
							$output .= ' <ul class="choose-list">';
							$answer_index = 0;
							foreach ( $quiz_question['answers'] as $answer_key => $answer ) {
								$answer_class_input_data = m24_get_answer_class_input_data( $results, $question_key, $answer_index );
								$output .= '<li>';
								$output .= '  <label for="answer-' . $count_of_questions . '">';
								$output .= '    <input id="answer-' . $count_of_questions . '" type="radio" name="answer-' . $question_key . '" value="' . $answer_key .'" ' . esc_attr( $answer_class_input_data['input_extra'] ) . '>';
								$output .= '    <span class="fake-input"></span><span class="fake-label ' . esc_attr( $answer_class_input_data['answer_class'] ) . '">' . $answer['answer'] . '  <span class="' . esc_attr( $answer_class_input_data['icon_class'] ) . '"></span></span>';
								$output .= '  </label>';
								$output .= '</li>';
								$count_of_questions++;
								$answer_index++;
							}
							$output .= ' </ul>';
							$output .= '</li>';
						}
					}

          // form is set to display registration form at the end (display it only if user is not logged in already, if he/she is we already have his/her data for submission)
          if ( !is_user_logged_in() && $registration_on_the_end ) {
            $output .= '<li>';
            $output .= '<strong class="h3">' . __('Please provide your personal info:', 'balance') . '</strong>';
            $output .= render_m24_quiz_registration_form();
            $output .= '</li>';
          }

					$output .= '         </ol>';

					$output .= '<input type="hidden" name="quiz-submission" value="quiz-submission" /> ';

					$output .= ( !empty( $quizid ) ? '<input type="hidden" id="quizId" name="quizid" value="' . $quizid . '">' : '' );

          $wlw_id = !empty($_GET['wlw']) ? $_GET['wlw'] : '0'; // if not set the white label website id in the querystring then we are on main balance website, set it to 0

          $output .= '<input type="hidden" name="wlw" value="' . $wlw_id . '" /> ';

					if ( ! $quiz_submitted) {
						$output .= '         <input type="submit" value="' . __('Submit', 'balance') . '" class="btn btn-warning submit-quiz">';
					}
					$output .= '       </div>';
					$output .= '      </form>';
					$output .= '    </div>';
					$output .= '  </div>';
					$output .= '</section>';
				}
			}
		}
	}
	return stripslashes( $output );
}

function m24_get_answer_class_input_data( $results, $question_index, $answer_index ) {
	$data = array(
		'answer_class' => '',
		'input_extra' => '',
		'icon_class' => '',
	);
	if ( $results && is_array( $results ) && array_key_exists( 'quiz_results', $results ) ) {
		foreach ( $results['quiz_results'] as $result ) {
		  
      // Question_index should be numeric!
			if (  $result['question_index'] !== null && $result['question_index'] == $question_index ) {
				if ( false !== $result['correct'] ) {
					if ( $answer_index == $result['answered'] ) {
						$data['answer_class'] = 'correct-answer';
						$data['input_extra'] = 'checked';
						$data['icon_class'] = 'icon-check';
					} else {
						$data['input_extra'] = 'disabled';
					}
				} else if ( ! $result['correct'] ) {
					if ( $result['answered'] == -1 ) {
						$data['input_extra'] = 'disabled';
						if ( $result['correct_index'] == $answer_index ) {
							$data['answer_class'] = 'correct-answer';
						} else {
							$data['answer_class'] = 'incorrect-answer';
						}
					} else {
						if ( $result['answered'] == $answer_index ) {
							$data['answer_class'] = 'incorrect-answer';
							$data['input_extra'] = 'checked';
							$data['icon_class'] = 'icon-close';
						}
						else if ( $result['correct_index'] == $answer_index ) {
							$data['answer_class'] = 'correct-answer';
							$data['input_extra'] = 'disabled';
						} else {
							$data['input_extra'] = 'disabled';
						}
					}
				}
			}
		}
	}
	return $data;
}

function printReattempt( $diff ) {
	$output = '      <p>' . __( 'You can attempt to solve the quiz again in', 'balance' );
	if ( $diff->d > 0 ) {
		$output .= sprintf( _n( ' %s day', ' %s days', $diff->d, 'balance' ), $diff->d );
	}
	if ( $diff->h > 0 ) {
		$output .= sprintf( _n( ' %s hour', ' %s hours', $diff->h, 'balance' ), $diff->h );
	}
	if ( $diff->i > 0 ) {
		$output .=  sprintf( _n( ' %s minute', ' %s minutes',  $diff->i, 'balance' ), $diff->i );
	}
	$output .= '.</p>';
	return $output;
}
