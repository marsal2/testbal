<?php
/*
Modules: {"m1[0]":{"name":"M1: Hero"},"m34[0]":{"name":"M34: Section - title, copy"}}
*/
?>
<?php
global $additional_body_class, $data,$woocommerce;
$additional_body_class = 'woocommerce page-cart';
get_custom_data();

// get header based on user type
$plain = getHeaderType();
get_header( $plain );

/* Hero */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0] );
}

/* SECTION - TITLE, COPY */
if (!empty($data['m34_module']) && !empty($data['m34_module'][0])) {
    echo render_m34_section_title_copy($data['m34_module'][0]);
}
?>
<article name="page_cart_article" class="text-block article default-content-style" aria-label="article page cart">
	<div class="container">
		<div class="row">
			<?php echo do_shortcode('[woocommerce_cart]'); ?>
		</div>
	</div>
</article>
<?php
// strip footer markup from theme to display in iframe
get_footer( $plain );
