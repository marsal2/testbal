<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

if(session_id() == '') {
  session_start();
}
require_once('security.php');
$config = require_once('config.php');
// Check if a form has been sent


$postedToken = filter_input(INPUT_POST, 'token');
if(!empty($postedToken)) {
  if(!isTokenValid($postedToken)){
    echo '<script>window.history.back();</script>';
    exit;
  }
} else {
  echo '<script>window.history.back();</script>';
  exit;
}


function callAPI($method, $url, $data){
   $curl = curl_init();

   switch ($method){
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }

   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Authorization: Basic ZGQyOTk2Mjg0NzcwMTFlOTk4ZDUwYWFjYThmOGM4ZmE6',
      'Content-Type: application/json'
   ));
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

   // EXECUTE:
   $result = curl_exec($curl);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
}

$url = "https://api-test.qualpay.com/platform/checkout";

    if (isset($_POST) || isset($_GET) ) {
      $_SESSION['initialformsubmit'] = array_merge($_GET, $_POST);
    }

    $amount = $_POST['cardholder-amount'];
    $amount = floatval($amount);
    $name = $_POST['cardholder-name'] . ' ' . $_POST['cardholder-surname'];
    $email = $_POST['cardholder-email'];

    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
      renderError("Invalid email.");
    }

    if(empty($amount) === true || is_numeric($amount) === false) {
      renderError("Missing params.");
    }

    if(empty($name) === true) {
      renderError("Missing params.");
    }

$data='
{
    "amt_tran": '.$amount.',
    "customer_first_name": "'.$_POST['cardholder-name'].'",
    "customer_last_name": "'.$_POST['cardholder-surname'].'",
    "customer_email": "'.$email.'",
    "billing_addr1": "'.$_POST['cardholder-address1'] . (!empty( $_POST['cardholder-address2'] ) ? ', ' . $_POST['cardholder-address2'] : '').'",
    "billing_state": "'.$_POST['cardholder-state'].'",
    "billing_city": "'.$_POST['cardholder-city'].'",
    "billing_zip": "'.$_POST['cardholder-zip'].'"
}
';

$get_data = callAPI('POST', $url, $data);
$response = json_decode($get_data, true);
//$errors = $response['response']['errors'];
if ($response['message'] == "Success") {
    $data = $response['data'];
    header("Location: " . $data['checkout_link']);
    exit();
}
else {
    header("Location: " . $config['BASE_URL']);
    exit();
}


