<?php

function render_m90_form_captcha_field( $field, $form_id = '', $value = '' ) {
  $output_field = '';

  $output_field .=  '<div class="g-recaptcha"></div>';
  $output_field .=  '<img alt="autocomplete loader" class="loading-recaptcha" src="' .get_stylesheet_directory_uri() . '/images/autocomplete-loader.gif' .'">';

  $output = '';
  $output = sprintf( get_form_field_wrap( $field, $form_id ), $output_field );

  return stripslashes( $output );
}
