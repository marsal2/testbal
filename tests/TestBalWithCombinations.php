<?php
    use PHPUnit\Framework\TestCase;
    use GuzzleHttp\Client;

    class TestBalWithCombinations extends TestCase
    {
        public function testWithCombination(){
            global $argv;
            $client = new Client();
            $one = microtime(1);
            $url = 'http://www.testxekera.com/testbal/wp-admin/admin-ajax.php?action=load_resources&query='.@$argv[4].'&sortBy='.@$argv[5].'&type='.@$argv[6].'&stage='.@$argv[7].'&tag='.@$argv[8];

            $response = $client->request('GET', $url);
            $two = microtime(1);
            echo "The resource page server side URL is -> ".$url;
            echo "\r\n";

            echo "Response Without Search is: ".$response->getStatusCode()." and \r\n";
            echo "\r\n"; //var_dump($response);
            $data = json_decode($response->getBody(), true); // returns an array

            //$body = json_decode($response->getBody());
            //var_dump($data["resourcesDataCount"]);die;
            echo "\r\n";
            echo 'Total records:'.$data["resourcesDataCount"];
            echo "\r\n";
            echo 'Total Request time: '. ( $two - $one );
//            $stack = [];
//            $this->assertEmpty($stack);
            if($data["resourcesDataCount"] == 0){
                http_response_code(201);
                $this->assertEquals(201, http_response_code()); //Note you will get an int for the return value, not a string
                $this->assertFalse(true,'0 results found , search something else');
//                $this->assertThat($data["resourcesDataCount"],$this->addWarning('0 results found , change combination'));
            }
            else{
                $this->assertTrue(true,$data["resourcesDataCount"].' results found');

            }
    }
}