<?php
/**
 * SendInvoiceRequestTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  qpPlatform
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Qualpay Platform API
 *
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.2
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace qpPlatform;

/**
 * SendInvoiceRequestTest Class Doc Comment
 *
 * @category    Class
 * @description SendInvoiceRequest
 * @package     qpPlatform
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SendInvoiceRequestTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "SendInvoiceRequest"
     */
    public function testSendInvoiceRequest()
    {
    }

    /**
     * Test attribute "email_address"
     */
    public function testPropertyEmailAddress()
    {
    }
}
