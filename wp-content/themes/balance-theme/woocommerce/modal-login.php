<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see      http://docs.woothemes.com/document/template-structure/
 * @author   WooThemes
 * @package  WooCommerce/Templates
 * @version     2.2.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$my_account_page_permalink = get_the_permalink ( get_option( 'woocommerce_myaccount_page_id' ) );
if ( !is_user_logged_in() ) {
?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<div class="login-modal">
  <div id="Modal1"  aria-label="login modal" tabindex="-1" role="dialog" class="modal fade">
    <div role="document" class="modal-dialog">
      <div class="modal-content">
        <button type="button" data-dismiss="modal" aria-label="<?php _e( 'Close', 'balance' ); ?>" class="close"><span aria-hidden="true">&#215;</span></button>
        <div class="login-form">
          <fieldset>
            <div class="tab-holder">
              <div class="pop-up-tab visible-xs">
                <ul role="tablist" class="nav nav-tabs">
                  <li role="presentation" class="active"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab"><?php _e( 'Register', 'balance' ); ?></a></li>
                  <li><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab"><?php _e( 'Log In', 'balance' ); ?></a></li>
                </ul>
                <button aria-label="<?php _e( 'Close', 'balance' ); ?>" data-dismiss="modal" type="button" class="close visible-xs"><span aria-hidden="true">&#215;</span></button>
              </div>
              <div class="tab-content col-holder">

                <div id="tab4" role="tabpanel" class="tab-panel active">
                  <form method="post" class="register" action="<?php echo $my_account_page_permalink . '?action=register'; ?>" >
                    <?php do_action( 'woocommerce_register_form_start' ); ?>
                    <div class="form-content">
                      <h1 class="text-info"><?php _e( 'First Time Users Please Register', 'balance' ); ?></h1>
                      <p><?php _e( 'Please register as a New User to access our library of financial resources, newsletters, and more!', 'woocommerce' ); ?></p>
                      <div class="form-fields">
                        <div class="input-group"><span class="label">
                          <label for="billing_first_name"><?php _e( 'First Name', 'woocommerce' ); ?></label></span>
                          <input id="billing_first_name" type="text" name="billing_first_name" placeholder="<?php _e( 'First Name', 'balance' ); ?>" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" required>
                        </div>
                        <div class="input-group"><span class="label">
                            <label for="billing_last_name"><?php _e( 'Last Name', 'woocommerce' ); ?></label></span>
                          <input id="billing_last_name" type="text" name="billing_last_name" placeholder="<?php _e( 'Last Name', 'balance' ); ?>" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" required>
                        </div>
                        <div class="input-group"><span class="label">
                            <label for="email"><?php _e( 'Email', 'woocommerce' ); ?></label></span>
                          <input id="email" type="email" placeholder="<?php _e( 'Your email will be your username', 'balance' ); ?>" name="email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" required>
                        </div>
                        <div class="input-group"><span class="label">
                            <label for="password"><?php _e( 'Password', 'woocommerce' ); ?></label></span>
                          <input id="password" type="password" placeholder="<?php _e( '8 characters, no spaces', 'balance' ); ?>" name="password" value="<?php if ( ! empty( $_POST['password'] ) ) echo esc_attr( $_POST['password'] ); ?>" required>
                        </div>
                        <div class="input-group"><span class="label">
                            <label for="password2"><?php _e( 'Confirm Password', 'woocommerce' ); ?></label></span>
                          <input id="password2" type="password" name="password2" placeholder="<?php _e( '8 characters, no spaces', 'balance' ); ?>" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" required>
                        </div>
                        <div class="g-recaptcha" data-validate="#submit-modal-register"></div>
                        <img alt="autocomplete loader" class="loading-recaptcha" src="<?php echo get_stylesheet_directory_uri() . '/images/autocomplete-loader.gif'; ?>">
                        <br>
                      </div>

                      <!-- Spam Trap -->
                      <div style="<?php echo ( is_rtl() ) ? 'right' : 'left'; ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

                      <?php do_action( 'woocommerce_register_form' ); ?>
                      <?php do_action( 'register_form' ); ?>


                      <?php wp_nonce_field( 'woocommerce-register' ); ?>

                      <div class="btn-block">
                        <!-- <div class="btn-holder text-center"><a href="#" class="btn btn-warning">Register</a></div> -->
                        <div class="btn-holder text-center">
                          <input type="submit" class="btn btn-warning" name="register" id="submit-modal-register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>" disabled/>
                        </div>
                      </div>

                      <?php do_action( 'woocommerce_register_form_end' ); ?>

                    </div>
                    </form>
                </div>
                <div id="tab5" role="tabpanel" class="tab-panel">
                  <form method="post" class="login" action="<?php echo $my_account_page_permalink . '?action=login'; ?>">
                    <div class="form-content">
                      <h1 class="text-info"><?php _e( 'Returning Users Please Log In', 'balance' ); ?></h1>
                      <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p> -->

                      <?php do_action( 'woocommerce_login_form_start' ); ?>

                      <div class="form-fields">
                        <div class="input-group">
                        	<span class="label">
                          		<label for="input006"><?php _e( 'Email / Username', 'woocommerce' ); ?></label>
                          	</span>
                          	<input id="input006" type="text" placeholder="<?php _e( 'Your Email / Username', 'balance' ); ?>" name="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" >
                        </div>
                        <div class="input-group">
                        	<span class="label">
                          		<label for="input007"><?php _e( 'Password', 'woocommerce' ); ?></label>
                          	</span>
                          	<input id="input007" type="password" placeholder="<?php _e( 'Password', 'balance' ); ?>" name="password">
                        </div>
                        <div class="input-group">
                        	<span class="label">
                          		<label for="input000"><?php _e( 'Remember me?', 'woocommerce' ); ?></label>
                          	</span>
                          	<input id="input000" type="checkbox" name="rememberme">
                        </div>
                        <?php do_action( 'woocommerce_login_form' ); ?>
                        <?php wp_nonce_field( 'woocommerce-login' ); ?>
                        <div>
                        	<a href="<?php echo $my_account_page_permalink . '?action=lost-password'; ?>"><?php _e( 'Lost your password?', 'balance' ); ?></a>
                        </div>
                        <br>
                        <div class="g-recaptcha" data-validate="#submit-modal-login"></div>
                        <img alt="autocomplete loader" class="loading-recaptcha" src="<?php echo get_stylesheet_directory_uri() . '/images/autocomplete-loader.gif'; ?>">
                      </div>
                    </div>
                    <div class="btn-block">
                      <div class="btn-holder text-center"><a href="<?php echo $my_account_page_permalink . '?action=lost-password'; ?>" class="forgot-pass visible-xs"><?php _e( 'Forgot password?', 'balance' ); ?></a>
                        <input type="submit" class="btn btn-warning" name="login" id="submit-modal-login" value="<?php esc_attr_e( 'Log In', 'woocommerce' ); ?>" disabled/>
                      </div>
                    </div>
                    <?php do_action( 'woocommerce_login_form_end' ); ?>
                  </form>

                  <?php do_action( 'woocommerce_after_customer_login_form' ); ?>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
