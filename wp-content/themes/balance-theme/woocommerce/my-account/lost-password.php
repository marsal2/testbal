<?php
$my_account_page_permalink = get_the_permalink ( get_option('woocommerce_myaccount_page_id') );
?>
<ul role="tablist" class="nav nav-tabs">
	<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">
            <h1 style="font-size: 18px; font-family:Gotham,Arial,sans-serif;margin-bottom: 0px;"><span class="icon-user"></span><?php _e( 'Lost Password', 'balance' ); ?></h1></a></li>
</ul>
<div class="tab-content">
	<?php wc_print_notices(); ?>
	<div id="tab1" role="tabpanel" class="tab-panel active">
		<form method="post" class="login account-form" action="<?php echo $my_account_page_permalink . '?action=lost-password'; ?>">
			<fieldset>
				<p><?php echo apply_filters( 'woocommerce_lost_password_message', __( 'Lost your password? Please enter your email address. You will receive a link to create a new password via email.', 'woocommerce' ) ); ?></p>
				<div class="form-block">
					<div class="input-group">
						<span class="label">
							<label for="user_login"><?php _e( 'Email / Username', 'balance' ); ?></label>
						</span>
						<input aria-label="user login field" id="user_login" type="text" name="user_login" placeholder="<?php _e( 'Your email / username', 'balance' ); ?>" required>
					</div>
					<?php do_action( 'woocommerce_lostpassword_form' ); ?>
					<div class="btn-holder">
						<?php wp_nonce_field( 'lost_password' ); ?>
						<input type="hidden" name="wc_reset_password" value="true" />
						<input type="submit" class="btn btn-warning" name="login" value="<?php esc_attr_e( 'Reset Password', 'woocommerce' ); ?>" />
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
