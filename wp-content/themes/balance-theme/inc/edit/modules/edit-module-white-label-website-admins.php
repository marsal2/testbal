<?php
/*
 *  WP Edit module: White Label Websites Admins
 */
function white_label_websites_admins_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
	global $data, $wpdb, $post;

	// read the existing data from the whitelabel_admins table.
	$prefix = $wpdb->prefix;
	$post_table_id = $prefix . 'post_id';
	$white_label_websites_table_name = $prefix . 'white_label_websites';
	$white_label_website_id = $wpdb->get_var( "SELECT white_label_website_id FROM $white_label_websites_table_name WHERE $post_table_id = '$post->ID';" );
	$results = $wpdb->get_results( "SELECT * FROM whitelabel_admins WHERE  white_label_website_id = '$white_label_website_id'", OBJECT );
	foreach ($results as $result_key => $result_value) {
		$data['wlw_admins_module'][$key]['admin'][$result_key] = array(
			'email' => $result_value->email
		);
	}

	$output = simple_edit_module( $key, 'wlw_admins', array(
			array(
				'type' => 'repeater',
				'name' => 'admin',
				'label' => 'Admin',
				'default_value' => array(),
				'fields' => array(
					array(
						'type' => 'email',
						'name' => 'email',
						'label' => 'Email',
						'default_value' => ''
					),
				)
			)
		), $data, $module_title, $visible_on );

	return $output;

}
