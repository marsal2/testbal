# UpdateOutstandingRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from_contact** | [**\qpPlatform\Model\Contact**](Contact.md) | The merchant business contact information. | [optional] 
**billing_contact** | [**\qpPlatform\Model\Contact**](Contact.md) | The customer contact information. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


