<?php
/*
Default index template, serves for all that do not have tempalte defined or have default template explicitly defined
Modules: {"m1[0]":{"name":"M1: Hero"},"m34[0]":{"name":"M34: Section - title, copy"}}
*/

?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'page-general';
get_custom_data();

get_header();

/* Hero */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0] );
}

/* SECTION - TITLE, COPY */
if (!empty($data['m34_module']) && !empty($data['m34_module'][0])) {
    echo render_m34_section_title_copy($data['m34_module'][0]);
}

/* FOUR/THREE MESSAGES */
if (!empty($data['m56_module']) && !empty($data['m56_module'][0])) {
    echo render_m56_m17_three_four_messages($data['m56_module'][0]);
}

/* LISTED LIFE STAGES */
if (!empty($data['m55_module']) && !empty($data['m55_module'][0])) {
    echo render_m55_listed_life_stages($data['m55_module'][0]);
}

/* VERTICAL LOGOS */
if (!empty($data['m19_module']) && !empty($data['m19_module'][0])) {
    echo render_m19_vertical_logos($data['m19_module'][0]);
}

/* SECTION M33 – Headline, blurb, CTA  */
if (!empty($data['m33_module']) && !empty($data['m33_module'][0])) {
    echo render_m33_section_title_copy_button($data['m33_module'][0]);
}

/* SECTION M13 – Bulleted list  */
if (!empty($data['m13_module']) && !empty($data['m13_module'][0])) {
    echo render_m13_bulleted_list_with_links($data['m13_module'][0]);
}

get_footer();
