<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$get_user = get_user_by( 'login', $user_login );
$secret_code = md5($get_user->ID . time());
update_user_meta($get_user->ID, "wcemailverifiedcode", $secret_code);
$create_verify_link = $secret_code . "@" . $get_user->ID;
$verify_link = add_query_arg(array("action" => "verify-acount", "account-verification-code" => base64_encode($create_verify_link)), wc_get_page_permalink( 'myaccount' ));
?>
<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
<p><?php printf( __( "Thanks for creating an account on %s. Your username is <strong>%s</strong>.", 'woocommerce' ), esc_html( $blogname ), esc_html( $user_login ) ); ?></p>
<p><?php _e( 'Before you can start using it <strong>you need to verify it</strong>. Click below to verify it:', 'woocommerce' ); ?></p>
<p>
    <a class="btn btn-warning" href="<?php echo $verify_link; ?>"><?php _e( 'Verify your account', 'woocommerce' ); ?></a>
</p>
<p><?php printf( __( 'Once verified, you can access your account area here: %s.', 'woocommerce' ), wc_get_page_permalink( 'myaccount' ) ); ?></p>
<?php do_action( 'woocommerce_email_footer', $email ); ?>
