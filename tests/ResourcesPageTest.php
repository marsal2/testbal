<?php
    use PHPUnit\Framework\TestCase;

    use GuzzleHttp\Client;
    use GuzzleHttp\Handler\MockHandler;
    use GuzzleHttp\HandlerStack;
    use GuzzleHttp\Psr7\Response;
    use GuzzleHttp\Psr7\Request;
    use GuzzleHttp\Exception\RequestException;


    class ResourcesPageTest extends TestCase
    {
        public function testResourcePage(){
            global $argv;
            $client = new Client();
            $one = microtime(1);
            $url = 'http://testbal.com/wp-admin/admin-ajax.php?action=load_resources&query='.@$argv[4].'&sortBy='.@$argv[5].'&type='.@$argv[6].'&stage='.@$argv[7].'&tag='.@$argv[8];

            $response = $client->request('GET', $url);
            $two = microtime(1);
            echo "The resource page server side URL is -> ".$url;
            echo "Response Without Search is: ".$response->getStatusCode()." and \r\n";
            echo "\r\n"; //var_dump($response);
            $data = json_decode($response->getBody(), true); // returns an array

            //$body = json_decode($response->getBody());
            //var_dump($data["resourcesDataCount"]);die;
            echo "\r\n";
            echo 'Total records:'.$data["resourcesDataCount"];
            echo "\r\n";
            echo 'Total Request time: '. ( $two - $one );
//            $stack = [];
//            $this->assertEmpty($stack);
            if($data["resourcesDataCount"] == 0){
                $this->assertFalse(true);
            }
            else{
                $this->assertTrue(true);

            }

            //echo $client->request('GET', '/')->getStatusCode();
        }

    }
