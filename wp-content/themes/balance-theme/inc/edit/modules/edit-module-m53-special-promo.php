<?php
/*
 *  WP Edit module: M53
 *  Description: Module special promo right sidebar. M53 - Special Promo
 */

function m53_module_form( $key, $visible_on = 'all', $module_title = '') {
  global $data;

  if( empty( $data['m53_module'][ $key ] ) ){
    $data = init_array_on_var_and_array_key($data, 'm53_module');
    $data['m53_module'][ $key ] = array(
      'title' => '',
      'copy' => '',
      'image' => array(
        'attachment_id' => '',
        'filename' => '',
        'fullpath' => ''
      ),
      'linkfield' => array(
        'url_type' => '1',
        'url' => '',
        'url_page_id' => '',
        'url_onclick_js' => ''
      ),
    );
  }

  $output = '';
  $output .= '<a name="m53-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper m53-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom m53-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';

  $output .= '        <p>';
  $output .= '           <label><b>'. __( 'Image', 'balance' ) .':</b></label><br/>';
  $output .=             upload_field( $data['m53_module'][ $key ]['image'], 'm53_module['.$key.'][image]' );
  $output .= '        </p>';

  $output .= '        <p>';
  $output .= '          <label>'. __( 'Title', 'balance' ) .':</label>';
  $output .=              text_field( $data['m53_module'][ $key ]['title'], 'm53_module['.$key.'][title]');
  $output .= '        </p>';

  $output .= '            <p>';
  $output .= '              <label><b>'. __( 'Copy', 'balance' ) .':</b></label><br/>';
  $output .=                textarea_field( $data['m53_module'][ $key ]['copy'], 'm53_module['.$key.'][copy]', true, 0, '', '', array('media_buttons' => false, 'quicktags' => true ) );
  $output .= '            </p>';

  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Link', 'balance' ) .':</b></label>';
  $output .=          links_to_field( $data['m53_module'][ $key ]['linkfield'], 'm53_module['.$key.'][linkfield]');
  $output .= '      </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;

}

?>
