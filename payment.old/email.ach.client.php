<?php
  global $ach_client_email_title, $ach_client_email_body;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $ach_client_email_title; ?></title>
</head>

<body style="margin:0;">

<div style="background-color:#f5f5f5;margin:0;padding:70px 0 70px 0;width:100%">
  <table border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
      <tr>
        <td align="center" valign="top">
          <table style="background-color: #fdfdfd; border: 1px solid #dcdcdc; border-radius: 3px!important;" border="0" width="600" cellspacing="0" cellpadding="0">
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <table style="background-color: #02a69c; border-radius: 3px 3px 0 0!important; color: #ffffff; border-bottom: 0; font-weight: bold; line-height: 100%; vertical-align: middle; font-family: 'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;" border="0" width="600" cellspacing="0" cellpadding="0">
                    <tbody>
                      <tr>
                        <td style="background-image: url('https://ci4.googleusercontent.com/proxy/3yUoMscDrljk_TixekAhViqLQT9j7s0zxXG61Adx3_6dfiOdG1tAIUnaQdgENSRsOr6OiD61WmTztGPbmDcQ-h5-T2YCk53eIe0BRwK5gk6sJZh8Uj8at3J2uWLKggg1gg=s0-d-e1-ft#https://www.balancepro.org/wp-content/themes/balance-theme/images/img01.png'); padding: 36px 48px; display: block;">
                          <h1 style="color: #ffffff; font-family: 'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif; font-size: 30px; font-weight: 300; line-height: 150%; margin: 0; text-align: left;"><?php echo $ach_client_email_title; ?></h1>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table border="0" width="600" cellspacing="0" cellpadding="0">
                    <tbody>
                      <tr>
                        <td style="background-color: #fdfdfd;" valign="top">
                          <table border="0" width="100%" cellspacing="0" cellpadding="20">
                            <tbody>
                              <tr>
                                <td style="padding: 48px;" valign="top">
                                  <div style="color: #737373; font-family: 'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
                                    <?php echo $ach_client_email_body; ?>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td style="background-image: url('https://ci4.googleusercontent.com/proxy/3yUoMscDrljk_TixekAhViqLQT9j7s0zxXG61Adx3_6dfiOdG1tAIUnaQdgENSRsOr6OiD61WmTztGPbmDcQ-h5-T2YCk53eIe0BRwK5gk6sJZh8Uj8at3J2uWLKggg1gg=s0-d-e1-ft#https://www.balancepro.org/wp-content/themes/balance-theme/images/img01.png'); color: #fff; background-size: cover; background-repeat: no-repeat;" align="center" valign="middle">
                  <table border="0" width="600" cellspacing="0" cellpadding="10">
                    <tbody>
                      <tr>
                        <td style="padding: 0;" valign="middle">
                          <table border="0" width="100%" cellspacing="0" cellpadding="10">
                            <tbody>
                              <tr>
                                <td style="padding: 24px; border: 0; color: #fff; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;" colspan="2" valign="middle"><img style="border: none; display: inline; font-size: 14px; font-weight: bold; height: auto; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize;" src="https://ci4.googleusercontent.com/proxy/BUOVGlfTa4JmOfMexbRc9JEjNXlxZQf2tGuHfrKOx8gtJIv1XisGhvA6jCv_4NpCnNc0QgSQAsiKid9r4x6edcUAZNFdwqM-V0pb3febT7T6jHuXycRL7Uv1kldt7WkHQ0DgwINmlYg=s0-d-e1-ft#https://www.balancepro.org/wp-content/themes/balance-theme/images/logo-primary.png" alt="Balance" width="198" height="44" /></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td style="padding: 0;" valign="bottom">
                          <table border="0" width="100%" cellspacing="0" cellpadding="10">
                            <tbody>
                              <tr>
                                <td style="padding: 24px; border: 0; color: #fff; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;" colspan="2" valign="bottom">© 2017 BALANCE. All rights reserved.</td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>

</body>
</html>
