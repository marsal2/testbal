<?php
  if(session_id() == '') {
    session_start();
  }
?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Balance bank account</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?=$config['BASE_URL'];?>/styles/main.css">
        <script src="https://cdn.dwolla.com/1/dwolla.js"></script>
    </head>
    <body>
      <!-- Header -->
      <header id="header">
        <div style="background-image: url(https://www.cccssf.org/bankruptcy/images/header_background.png);background-repeat: no-repeat; background-size: cover; background-position: 50% 0; max-height: 50px; letter-spacing: -4px; text-align: right; padding: 10px 5px;" class="header-t">
          <div class="container">
            <div class="row">
              <!-- search form-->
              <ul class="contact-block" style="text-transform: uppercase; letter-spacing: 0; line-height: 0; font-size: 0; display: inline-block; vertical-align: middle; letter-spacing: 1px; margin-right: 11%;">
                <li><a href="tel:8007777526"><span class="icon-phone"></span>800.777.PLAN (7526)</a></li>
              </ul>
            </div>
          </div>
        </div>
        <nav class="navbar navbar-default">
          <div class="container">
            <div class="row">
              <div class="navbar-header" style="margin:30px 0; margin-left: 11%;">
                <a href="http://www.balancepro.org/"><img src="https://www.cccssf.org/bankruptcy/images/logo.svg" width="238" height="51" alt="balance"></a>
              </div>
            </div>
          </div>
        </nav>
      </header>

      <!-- Main -->
      <div class="main">
        <div id="iavContainer"></div>
        <div id="verification-success" style="display:none">
          <h1>Processing, please wait...</h1>
          <p>You will be redirected in 5 seconds.</p>
        </div>
        <div id="verification-error" style="display:none">
          <h1>There was an error with your payment.</h1>
          <p><a onclick='window.history.back()'>Try again</a> or contact us at <a href="tel:8007777526">800.777.PLAN (7526)</a></p>

          <p>Error description: <span id="error-description"></span></p>
        </div>
      </div>

      <!-- Footer -->
      <footer id="footer" style="background-image: url(https://www.cccssf.org/bankruptcy/images/footer_background.png);">
        <div class="container">
          <div class="row">
            <div class="logo-holder"><a href="http://www.balancepro.org/"><img src="https://www.cccssf.org/bankruptcy/images/logo-primary.svg" width="198" height="44" alt="Balance"></a></div>
          </div>
        </div>
      </footer>

      <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script>
        // this will display the user verification module (iframe supplied by dwolla)
        var iavToken = '<?php echo $customer['iav_token']; ?>';
        dwolla.configure('<?php echo $dwollaconf['mode']; ?>');
        dwolla.iav.start(iavToken, {
          container: 'iavContainer',
          stylesheets: [
            '//fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext',
            '<?=$config['BASE_URL']; ?>/styles/ach.css'
          ],
          microDeposits: 'false',
          fallbackToMicroDeposits: 'false'
        }, function(err, res) {
          if (err == null) { // the verification was successful continue with the transfer request
            console.log(res);
            console.log(res._links['funding-source'].href);
            $('#verification-success').show();
            window.setTimeout(function(){
              var funding_source = encodeURIComponent(res._links['funding-source'].href);
              window.location = "success-ach.php?source=" + funding_source;
            }, 5000);
          } else {
            // the transaction was not successfull, display the error thrown
            console.log(err);
            $('#error-description').html(JSON.stringify(err));
            $('#verification-error').show();
          }
        });
      </script>
    </body>
</html>
