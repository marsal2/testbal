<?php
/*
Template Name: T15: Annual reports and leadership
Modules: {"m1[0]":{"name":"M1: Hero"},"m34[0]":{"name":"M34: Section"},"m41b[0]":{"name":"M41b: Left / Right image sections"}}
*/

?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'annual-reports-leadership';
get_custom_data();

get_header();

/* M01/M57 renderer */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0] );
}

/* SECTION - TITLE, COPY */
if (!empty($data['m34_module']) && !empty($data['m34_module'][0])) {
    echo render_m34_section_title_copy($data['m34_module'][0]);
}

/* SECTION - LEFT / RIGHT IMAGE */
if (!empty($data['m41b_module']) && !empty($data['m41b_module'][0])) {
    echo render_m41b_left_right_image($data['m41b_module'][0]);
}

get_footer();
?>
