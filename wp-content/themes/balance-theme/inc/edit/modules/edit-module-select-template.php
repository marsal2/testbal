<?php
/*
 *  WP Edit module: Select Template
 *  Description: Module with dropdown of available wp templates. On change it reloads edit page with modules linked to the selected template.
 */
function select_template_module_form() {
  global $post;
  global $post_type;

  $available_templates = get_page_templates();
  ksort($available_templates);
  $current_template = get_page_template_slug( $post->ID );

  $output = '';
  $output .= '<a name="custom-templates-selector-wrapper"></a>';
  $output .= '<div class="postbox custom-templates-selector-wrapper">';
  $output .= '  <div class="inside">';
  $output .= '    <p>';
  $output .= '      <label>';
  $output .= '        <span class="text">'. __( 'Select template: ') .'</span>';
  $output .= '      </label>';
  $output .= '      <select class="custom-templates-selector custom-templates-selector">';
  $output .= '        <option '. selected($current_template, '', false) .' value="default">';
  $output .= '          <span class="dashicons dashicons-admin-appearance"></span> Default/Blank';
  $output .= '        </option>';
    foreach($available_templates as $key => $template) {
      $output .= '         <option '. selected($current_template, $template, false) .' value="'. $template .'">';
      $output .= '            <span class="dashicons dashicons-admin-appearance"></span>'. $key;
      $output .= '          </option>';
    }
  $output .= '      </select>';
  $output .= '      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="all-sections-expander" href="javascript:;">' . __( 'Expand All' ) . '</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="all-sections-collapser"href="javascript:;">' . __( 'Collapse All' ) . '</a>';
  $output .= '    </p>';
  $output .= '  </div>';
  $output .= '</div>';

  return $output;
}
?>
