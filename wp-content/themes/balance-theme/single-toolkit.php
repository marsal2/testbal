<?php
/*
Modules: {"m1[0]":{"name":"Hero"},"m38[0]":{"name":"Copy"},"resourcesections[0]":{"name":"Resource Sections"}}
*/

?>
<?php
global $additional_body_class, $post, $data;
Balance_Resources_Public::increment_resource_view_count( $post->ID, 1, 0 );
$additional_body_class = 'single-toolkit';
get_custom_data();

get_header();
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
  echo render_m1_hero( $data['m1_module'][0] );
}
get_template_part('partials/program');
echo render_m23a_may_also_like_resources();
get_footer();
