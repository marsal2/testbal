<?php
    use PHPUnit\Framework\TestCase;

    use GuzzleHttp\Client;
    use GuzzleHttp\Handler\MockHandler;
    use GuzzleHttp\HandlerStack;
    use GuzzleHttp\Psr7\Response;
    use GuzzleHttp\Psr7\Request;
    use GuzzleHttp\Exception\RequestException;


    class TestBalResourcesPageTest extends TestCase
    {
        public function testTestBalResourcePage(){
            global $argv;
            $client = new Client();
            $one = microtime(1);
            $url = 'http://www.testxekera.com/testbal/wp-admin/admin-ajax.php?action=load_resources&query=&sortBy=&type=&stage=&tag=';

            $response = $client->request('GET', $url);
            $two = microtime(1);
            echo "The resource page server side URL is -> ".$url;
            echo "Response Without Search is: ".$response->getStatusCode()." and \r\n";
            echo "\r\n"; //var_dump($response);
            $data = json_decode($response->getBody(), true); // returns an array
            echo "\r\n";
            echo 'Total records:'.$data["resourcesDataCount"];
            echo "\r\n";
            echo 'Total Request time: '. ( $two - $one );
//            $this->assertEmpty($stack);
            if($data["resourcesDataCount"] == 0){
                $this->assertFalse(true,'0 results found , change combination');
            }
            else{
                $this->assertTrue(true,$data["resourcesDataCount"].' results found');
            }

            //echo $client->request('GET', '/')->getStatusCode();
        }



    }
