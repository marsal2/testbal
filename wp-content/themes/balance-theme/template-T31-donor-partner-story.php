<?php
/*
Template Name: T31: Donor or Partner Story
Modules: {"m1[0]":{"name":"M1: Hero"},"m34[0]":{"name":"M34: Section","params":{"media_buttons":true,"quicktags":true}},"m20[0]":{"name":"M20: Split Carousel"},"m56_m17[0]":{"name":"M17: Why Partner or Donate"}}
*/
?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'donor-partner-story';
get_custom_data();

get_header();

/* M01/M57 renderer */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0] );
}

/* SECTION - TITLE, COPY */
if (!empty($data['m34_module']) && !empty($data['m34_module'][0])) {
    echo render_m34_section_title_copy($data['m34_module'][0]);
}

/* M20 renderer */
if ( !empty( $data['m20_module'] ) && !empty( $data['m20_module'][0] ) ) {
	echo render_m20_split_carousel( $data['m20_module'][0] );
}

/* FOUR/THREE MESSAGES */
if (!empty($data['m56_module']) && !empty($data['m56_module'][0])) {
    echo render_m56_m17_three_four_messages($data['m56_module'][0]);
}

get_footer();
?>
