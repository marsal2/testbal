<?php
/**
 * Customer Reset Password email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see      http://docs.woothemes.com/document/template-structure/
 * @author   WooThemes
 * @package  WooCommerce/Templates/Emails
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php _e( 'Someone requested that the password be reset for the following account:', 'woocommerce' ); ?></p>
<p><?php _e( 'Username', 'woocommerce' ); ?>: <strong><?php echo $user_login; ?></strong></p>
<p><?php _e( 'If this was a mistake, just ignore this email and nothing will happen, otherwise click below:', 'woocommerce' ); ?></p>
<p>
    <a class="btn btn-warning" href="<?php echo esc_url( add_query_arg( array( 'action' => 'reset-password', 'key' => $reset_key, 'login' => rawurlencode( $user_login ) ), wc_get_page_permalink( 'myaccount' ) ) ); ?>"><?php _e( 'Create new password', 'woocommerce' ); ?></a>
</p>
<p></p>

<?php do_action( 'woocommerce_email_footer', $email ); ?>
