<?php
/*
 *  WP Edit module: M50
 *  Description: Module for hero slider. M50 – Hero Slider
 */

function m50_module_form( $key, $visible_on = 'all', $module_title = '' ) {
	global $data;

	$output = simple_edit_module( $key, 'm19', array(
			array(
				'type' => 'repeater',
				'name' => 'slides',
				'label' => 'Slide',
				'default_value' => array(),
				'fields' => array(
					array(
						'type' => 'upload',
						'name' => 'bgimage',
						'label' => 'Background image',
						'default_value' => array(),
						'additional_params' => array(
							'',
							'Image',
							'image'
						)
					),
					array(
						'type' => 'text',
						'name' => 'title',
						'label' => 'Title',
						'default_value' => ''
					),
					array(
						'type' => 'text',
						'name' => 'copy',
						'label' => 'Copy',
						'default_value' => ''
					),
					array(
						'type' => 'text',
						'name' => 'buttontext',
						'label' => 'Button text',
						'default_value' => ''
					),
					array(
						'type' => 'links_to',
						'name' => 'linkfield',
						'label' => 'Button link',
						'default_value' => array()
					),
				)
			)
		), $data, $module_title, $visible_on );

	return $output;

}
