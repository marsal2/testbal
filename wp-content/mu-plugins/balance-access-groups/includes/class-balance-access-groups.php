<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_Access_Groups
 * @subpackage Balance_Access_Groups/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Balance_Access_Groups
 * @subpackage Balance_Access_Groups/includes
 * @author     ROIDNA <devs@roidna.com>
 */
class Balance_Access_Groups {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Balance_Access_Groups_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	protected $db;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'balance-access-groups';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Balance_Access_Groups_Loader. Orchestrates the hooks of the plugin.
	 * - Balance_Access_Groups_i18n. Defines internationalization functionality.
	 * - Balance_Access_Groups_Admin. Defines all hooks for the admin area.
	 * - Balance_Access_Groups_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-balance-access-groups-loader.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-balance-access-groups-db.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-balance-access-groups-i18n.php';


		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-balance-access-groups-db.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-balance-access-groups-wp-list-table.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-balance-access-groups-admin.php';


		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-balance-access-groups-public.php';





		$this->loader = new Balance_Access_Groups_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Balance_Access_Groups_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Balance_Access_Groups_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Balance_Access_Groups_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_ajax_access_groups', $plugin_admin, 'access_groups_ajax_api' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_menu', $plugin_admin,  'access_groups_menu', 999, 2 );

		$this->loader->add_action( 'save_post', $plugin_admin,  'access_group_metabox_save_cb', 10, 3);
		$this->loader->add_action( 'edit_post', $plugin_admin,  'access_group_metabox_save_cb', 10, 3);
		$this->loader->add_action( 'publish_post', $plugin_admin,  'access_group_metabox_save_cb', 10, 3);

		$this->loader->add_action( 'trash_access_group', $plugin_admin,  'access_group_trash_cb', 10, 3);
		$this->loader->add_action( 'untrashed_post', $plugin_admin,  'access_group_restore_cb', 10, 3);
		$this->loader->add_action( 'delete_post', $plugin_admin,  'access_group_delete_cb', 10, 3);


		$this->loader->add_action( 'init', $plugin_admin,  'access_groups_install' );
		$this->loader->add_action( 'init', $plugin_admin,  'register_custom_post_type' );

		//Disable auto save for posts
		$this->loader->add_action('admin_print_scripts', $plugin_admin, 'access_group_disable_autosave');

		$this->loader->add_filter("post_updated_messages", $plugin_admin, "access_groups_post_updated_messages");

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 *
	 */



	private function define_public_hooks() {

		$plugin_public = new Balance_Access_Groups_Public( $this->get_plugin_name(), $this->get_version() );

		// $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		// $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}





	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Balance_Access_Groups_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
