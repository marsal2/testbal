<?php
/*
 * Balance Custom post type: Programs
 *
 */

/*
 * custom post type registration
 */

function cpt_programs() {
	$labels = array(
		'name'               => __( 'Programs', 'balance' ),
		'singular_name'      => __( 'Program', 'balance' ),
		'add_new'            => __( 'Add New', 'balance' ),
		'add_new_item'       => __( 'Add New Program', 'balance' ),
		'edit_item'          => __( 'Edit Program', 'balance' ),
		'new_item'           => __( 'New Program', 'balance' ),
		'all_items'          => __( 'Programs', 'balance' ),
		'view_item'          => __( 'View Program', 'balance' ),
		'search_items'       => __( 'Search Programs', 'balance' ),
		'not_found'          => __( 'No Programs found', 'balance' ),
		'not_found_in_trash' => __( 'No Programs found in the Trash', 'balance' ),
		'parent_item_colon'  => '',
		'menu_name'          => __( 'Programs', 'balance' )
	);
	$args = array(
		'labels'        => $labels,
		'description'   => __( 'Data about Programs', 'balance' ),
		'public'        => true,
		'supports'      => array( 'title', 'thumbnail', 'excerpt', 'page-attributes' ),
		'rewrite'       => array( 'slug' => 'programs', 'with_front' => true ),
		'has_archive'   => false,
		'exclude_from_search' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 28,
		'menu_icon' => 'dashicons-feedback',
		'capability_type' => 'post',
		'hierarchical' => false,
	);
	register_post_type( 'program', $args );
}
add_action( 'init', 'cpt_programs' );

/*
 * custom post type update messages
 */
function programs_updated_messages( $messages ) {
	global $post, $post_ID;
	$messages['programs'] = array(
		0 => '',
		1 => __( 'Programs updated.', 'balance' ),
		2 => __( 'Custom field updated.', 'balance' ),
		3 => __( 'Custom field deleted.', 'balance' ),
		4 => __( 'Programs post updated.', 'balance' ),
		5 => isset( $_GET['revision'] ) ? sprintf( __( 'Programs post restored to revision from %s', 'balance' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => __( 'Programs saved.', 'balance' ),
		7 => __( 'Programs saved.', 'balance' ),
		8 => '',
		9 => '',
		10 => '',
	);
	return $messages;
}
add_filter( 'post_updated_messages', 'programs_updated_messages' );

/*
 * custom post type columns definition in administration (on edit.php page)
 */
add_filter( 'manage_edit-program_columns', 'edit_program_columns' ) ;

function edit_program_columns( $columns ) {
	global $post;

	$columns = array(
		'cb' => '',
		'title' => __( 'Program', 'balance' ),
		'taxonomy-program_category' => __( 'Category', 'balance' ),
		'program_type' => __( 'Type', 'balance' ),
		'hidden_on' => __( 'Hidden on balancepro.org', 'balance' ),
		'access_level' => __( 'Access Level', 'balance' ),
		'menu_order' => __( 'Order', 'balance' ),
		'date' => __( 'Date', 'balance' )
	);
	return $columns;
}

/*
 * custom post type columns display settings in administration (on edit.php page)
 */
add_action( 'manage_program_posts_custom_column', 'manage_program_columns', 10, 2 );

function manage_program_columns( $column, $post_id ) {
	global $post;

	$custom_data = get_custom_data( $post_id );

	$program_type = '';
	if ( !empty( $custom_data['m56b_module'][0]['program_type'] ) ) {
		$program_type = $custom_data['m56b_module'][0]['program_type'];
	}

	switch ( $column ) {
	case 'program_type' :
		switch ( $program_type ) {
		case 'link_to_old_site':
			echo __( 'Link to old site', 'balance' );
			break;
		case 'program_resources':
			echo __( 'List of resources', 'balance' );
			break;
		case 'resource_as_program':
			echo __( 'Single resource (multi page article version)', 'balance' );
			break;
		default:
			echo '—';
			break;
		}
		break;
	case 'menu_order' :
		echo $post->menu_order;
		break;
	case 'hidden_on' :
		$checkbox_value = get_post_meta( $post->ID, "meta-box-checkbox", true );
		echo '<span class="dashicons dashicons-' . ( $checkbox_value == 'true' ? 'yes' : 'no' ) . '"></span>';
		break;
	case 'access_level' :
		$access_level = get_post_meta( $post->ID, 'access_level', true );
		switch ( $access_level ) {
		case '200':
			echo 'Registered users';
			break;
		default:
			echo  'Non registered users';
			break;
		}
		break;
	default :
		break;
	}
}

// add a filter to admin list programs view
function add_program_category_taxonomy_program_filters() {
	global $typenow;

	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array( 'program_category' );
	$post_status = get_query_var( 'post_status', array( 'publish', 'draft' ) );
	// must set this to the post type you want the filter(s) displayed on
	if ( $typenow == 'program' ) {

		foreach ( $taxonomies as $tax_slug ) {
			$tax_obj = get_taxonomy( $tax_slug );
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms( $tax_slug, array( 'hide_empty' => false ) );
			if ( count( $terms ) > 0 ) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>$tax_name</option>";
				foreach ( $terms as $term ) {
					$posts_count  = 0;
					$count_args = array(
						'posts_per_page' => -1,
						'post_status' => $post_status,
						'post_type' => 'program',
						'tax_query' => array(
							array(
								'taxonomy' => 'program_category',
								'field' => 'slug',
								'terms' => $term->slug
							)
						)
					);
					$posts_count = count( get_posts( $count_args ) );
					echo '<option value='. $term->slug . ( !empty( $_GET[$tax_slug] ) ? ( $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '' ) : '' ) . '>' . $term->name .' (' . $posts_count .')</option>';
				}
				echo "</select>";
			}
		}
		$orderby = get_query_var( 'orderby', 'date' );
		echo "<span style='float:left; padding: 5px 10px 5px 5px;'>Order by:</span>";
		echo "<select name='orderby' id='orderby' class='postform'>";
		echo "<option value='date' " . selected( $orderby, 'date', false ) . ">Date</option>";
		echo "<option value='title' " . selected( $orderby, 'title', false ) . ">Title</option>";
		echo "<option value='menu_order' " . selected( $orderby, 'menu_order', false ) . ">Order</option>";
		echo "</select>";
		$order = get_query_var( 'order', 'DESC' );
		echo "<select name='order' id='order' class='postform'>";
		echo "<option value='DESC' " . selected( $order, 'DESC', false ) . ">Descending</option>";
		echo "<option value='ASC' " . selected( $order, 'ASC', false ) . ">Ascending</option>";
		echo "</select>";
	}
}
add_action( 'restrict_manage_posts', 'add_program_category_taxonomy_program_filters' );

?>
