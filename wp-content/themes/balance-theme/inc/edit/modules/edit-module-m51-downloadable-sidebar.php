<?php
/*
 *  WP Edit module: M51
 *  Description: Module sidebar downloadable content. M51 - Downloadable Content in Sidebar
 */

function m51_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
	global $data;

	$output = simple_edit_module( $key, 'm51', array(
			array(
				'type' => 'text',
				'name' => 'title',
				'label' => 'Title',
				'default_value' => ''
			),
			array(
				'type' => 'repeater',
				'name' => 'resources',
				'label' => 'Resource',
				'default_value' => array(),
				'fields' => array(
					array(
						'type' => 'upload',
						'name' => 'uploadimage',
						'label' => 'Image',
						'default_value' => array(),
						'additional_params' => array(
							'',
							'Image',
							'image'
						)
					),
					array(
						'type' => 'text',
						'name' => 'title',
						'label' => 'Title',
						'default_value' => ''
					),
					array(
						'type' => 'upload',
						'name' => 'file',
						'label' => 'File',
						'default_value' => '',
						'additional_params' => array(
							'',
							'File',
							'application/pdf' // "all" returns all media, "image" all images, specific mime type returns that mime type (check wp-includes/functions.php line 2252 for the mime types available)
						)
					),
					array(
						'type' => 'text',
						'name' => 'buttontext',
						'label' => 'Button text',
						'default_value' => ''
					),
					array(
						'type' => 'links_to',
						'name' => 'linkfield',
						'label' => 'Button link',
						'default_value' => array()
					),
				)
			),
			array(
				'type' => 'text',
				'name' => 'buttontext',
				'label' => 'Button text',
				'default_value' => ''
			),
			array(
				'type' => 'links_to',
				'name' => 'linkfield',
				'label' => 'Button link',
				'default_value' => ''
			)
		), $data, $module_title, $visible_on );

	return $output;

}

?>
