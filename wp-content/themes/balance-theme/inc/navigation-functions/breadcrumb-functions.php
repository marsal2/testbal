<?php

function get_breadcrumbs(){
  global $resources_cpts;
  global $post;

  if( is_front_page() || empty( $post ) ){
    return;
  }


  $post_type = strtolower( get_post_type( $post->ID ) );

  $title = get_the_title( $post->ID );
  $is_resource = in_array( $post_type, $resources_cpts);
  $is_lifestage = $post_type == 'life_stage';
  $is_life_experience = false;
  $is_news = $post_type == 'news';
  $is_quiz = $post_type == 'quiz';
  $is_program = $post_type == 'program';
  $post_type_obj = get_post_type_object( $post_type );

  if( $is_lifestage ) { //if life stage or experience

    $is_life_experience = get_metadata('post', $post->ID, '_wp_page_template')[0] == 'life_experience';

  }
  ?>
  <div class="breadcrumb-wrapper">
    <div class="container">
      <div class="row">
        <ol class="breadcrumb">
          <!-- Link to home -->
          <li><a href="<?php echo get_bloginfo('url'); ?>">Home</a></li>
          <!-- Link to resource page -->

          <?php

          //if is page, get all parents
          if( is_page() ){

            $parents = array_reverse( get_post_ancestors( $post->ID ) );

            foreach ($parents as $parent) {
              echo '<li><a aria-label="breadcrumb" href="'. get_relative_permalink( $parent ) .'">'. get_the_title( $parent ) .'</a></li>';
            }
          } elseif( $is_lifestage && !$is_life_experience ){
            echo '<li><a aria-label="breadcrumb" href="/life-stages">'. __( 'Life Stages', 'balance' ) .'</a></li>';
          } elseif( $is_life_experience ) { //life experience
            $life_exp_parent = wp_get_post_parent_id( $post->ID );
            echo '<li><a aria-label="breadcrumb" href="/life-stages">'. __( 'Life Stages', 'balance' ) .'</a></li><li><a href="'. get_relative_permalink( $life_exp_parent ) .'">'. get_the_title( $life_exp_parent ) .'</a></li>';
          } elseif( $is_resource ){ //single resource
            echo '<li><a aria-label="breadcrumb" href="/resources">'. __( 'Resources', 'balance' ) .'</a></li><li>'. $post_type_obj->labels->name .'</li>';
            /**
             * @todo if multipage article add breadcrumb for individual pages
             */
          } elseif( $is_news ){
            echo '<li><a href="/news">'. __( 'News', 'balance' ) .'</a></li>';
          } elseif( $is_quiz ){
            echo '<li>'. __( 'Quizzes', 'balance' ) .'</li>';
          } elseif( $is_program ){
            /**
             * @todo Home -> Programs -> Program title - if we are on a program / Home -> Programs -> Program title -> Resource title -> page number - if we are in program resource
             */
          }
          if ( !is_archive() ) {
          	//Output current post title
          	echo ( !empty( $title ) ? '<li>'. $title .'</li>' : '' );
          } else {
          	echo '<li>'. post_type_archive_title( '', false ) .'</li>';
          }
          ?>
        </ol>
      </div>
    </div>
  </div>
  <?php
}

?>
