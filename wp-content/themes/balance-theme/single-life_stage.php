<?php
/*
Modules: {"life_stages_type[0]":{"name":"Type Selector"},"life_stage_homepage_info[0]":{"name":"Featured info", "visible":"life_stage life_experience"},"m1[0]":{"name":"M1: Hero", "visible":"life_stage life_experience"},"m34[0]":{"name":"M34: Section", "visible":"life_stage life_experience"},"m20[0]":{"name":"M20: Split Carousel", "visible":"life_stage life_experience"},"m55[0]":{"name":"Life stages listing","visible":"life_stage"},"m31[1]":{"name":"M31: Section", "visible":"life_stage life_experience"},"life_stage_settings[0]":{"name":"Resources Settings", "visible":"life_stage life_experience"}}
*/

?>
<?php
global $data, $additional_body_class, $post;
$additional_body_class = 'single-life-stage';
get_custom_data();

get_header();


/* M01 renderer */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0] );
}

/* M34 renderer */
if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][0] ) ) {
	echo render_m34_section_title_copy( $data['m34_module'][0] );
}

/* M20 renderer */
if ( !empty( $data['m20_module'] ) && !empty( $data['m20_module'][0] ) ) {
	echo render_m20_split_carousel( $data['m20_module'][0] );
}

$is_life_stage = false;
$is_life_experience = false;

if ( $data['life_stage_type'][0]['type'] == 'life_stage' ) { //if life stage
	$is_life_stage = $post->ID;
	$is_life_experience = false;
		/* LISTED LIFE STAGES */
	if ( !empty( $data['m55_module'] ) && !empty( $data['m55_module'][0] ) ) {
		echo render_m55_listed_life_stages( $data['m55_module'][0], false );
	}

} else if ( $data['life_stage_type'][0]['type'] == 'life_experience' ) {
	$is_life_stage = false;
	$is_life_experience = $post->ID;
}

/* M34 renderer */
if ( !empty( $data['m31_module'] ) && !empty( $data['m31_module'][1] ) ) {
	echo render_m34_section_title_copy( $data['m31_module'][1] );
}

echo render_m14_15_resources_search( $is_life_stage, $is_life_experience );

get_footer();

?>
