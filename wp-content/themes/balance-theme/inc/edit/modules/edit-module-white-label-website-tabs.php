<?php
/*
 *  WP Edit module: White Label Websites Tabs
 *  Description: Module with tabs of the credit union site administration. On change only the modules of the section gets displayed
 */
function white_label_websites_tabs_module_form() {

  $output = '';
  $output .= '<h2 class="nav-tab-wrapper">';
    $output .= '<a href="#" class="nav-tab nav-tab-active" data-tab="tab-0">General</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-1">Homepage</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-2">Contact Us Page</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-3">Resources Page</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-4">Programs Page</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-5">Webinars Page</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-6">Administrators</a>';
  $output .= '</h2>';

  return $output;
}
?>
