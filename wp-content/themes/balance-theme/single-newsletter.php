<?php
/*
Modules: {"m34[0]":{"name":"single-newsletter"}}
*/

?>
<?php
global $additional_body_class, $post, $data;
Balance_Resources_Public::increment_resource_view_count( $post->ID, 1, 0 );
$additional_body_class = 'single-newsletter';
get_custom_data();

get_header();
get_template_part('partials/simple-content');
echo render_m23a_may_also_like_resources();
get_footer();
