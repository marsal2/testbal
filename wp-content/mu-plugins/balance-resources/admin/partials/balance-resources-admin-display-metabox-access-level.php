<?php

/**
 * Provide the view for a access level metabox
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_Resources
 * @subpackage Balance_Resources/admin/partials
 */

$meta = get_post_custom( $object->ID );
$value = '';

if ( ! empty( $meta['access_level'][0] ) ) {
  $value = $meta['access_level'][0];
} else {
  $value = '100'; // default value
}

wp_nonce_field( 'verify_this_0789', 'access_level_nonce' );

?>
<p>
  <input type="radio" name="access_level" id="access_level_100" value="100" <?php checked( esc_attr( $value ), '100' ); ?>  />
  <label for="access_level_100">Non registered users</label>
</p>
<p>
  <input type="radio" name="access_level" id="access_level_200" value="200" <?php checked( esc_attr( $value ), '200' ); ?> />
  <label for="access_level_200">Registered users</label>
</p>
