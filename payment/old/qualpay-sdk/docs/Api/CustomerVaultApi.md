# qpPlatform\CustomerVaultApi

All URIs are relative to *https://api-test.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addBillingCard**](CustomerVaultApi.md#addBillingCard) | **POST** /vault/customer/{customer_id}/billing | Add a Billing Card
[**addCustomer**](CustomerVaultApi.md#addCustomer) | **POST** /vault/customer | Add a Customer
[**addShippingAddress**](CustomerVaultApi.md#addShippingAddress) | **POST** /vault/customer/{customer_id}/shipping | Add a Shipping Address
[**browseCustomers**](CustomerVaultApi.md#browseCustomers) | **GET** /vault/customer | Get all Customers
[**deleteBillingCard**](CustomerVaultApi.md#deleteBillingCard) | **PUT** /vault/customer/{customer_id}/billing/delete | Delete a Billing Card
[**deleteCustomer**](CustomerVaultApi.md#deleteCustomer) | **DELETE** /vault/customer/{customer_id} | Delete a Customer
[**deleteShippingAddress**](CustomerVaultApi.md#deleteShippingAddress) | **DELETE** /vault/customer/{customer_id}/shipping/{id} | Delete a Shipping Address
[**getBillingCards**](CustomerVaultApi.md#getBillingCards) | **GET** /vault/customer/{customer_id}/billing | Get Billing Cards
[**getCustomer**](CustomerVaultApi.md#getCustomer) | **GET** /vault/customer/{customer_id} | Get by Customer ID
[**getShippingAddresses**](CustomerVaultApi.md#getShippingAddresses) | **GET** /vault/customer/{customer_id}/shipping | Get Shipping Addresses
[**setPrimaryBillingCard**](CustomerVaultApi.md#setPrimaryBillingCard) | **PUT** /vault/customer/{customer_id}/billing/primary | Set Primary Billing Card
[**setPrimaryShippingAddress**](CustomerVaultApi.md#setPrimaryShippingAddress) | **PUT** /vault/customer/{customer_id}/shipping/primary | Set Primary Shipping Address
[**updateBillingCard**](CustomerVaultApi.md#updateBillingCard) | **PUT** /vault/customer/{customer_id}/billing | Update a Billing Card
[**updateCustomer**](CustomerVaultApi.md#updateCustomer) | **PUT** /vault/customer/{customer_id} | Update a Customer
[**updateShippingAddress**](CustomerVaultApi.md#updateShippingAddress) | **PUT** /vault/customer/{customer_id}/shipping | Update a Shipping Address


# **addBillingCard**
> \qpPlatform\Model\CustomerResponse addBillingCard($customer_id, $body)

Add a Billing Card

Tokenizes and adds a billing card to a customer record. The response will include the tokenized card_number in the field, card_id, which can be used to manage the card and can be used in place of the card_number in Payment Gateway API requests. There is no limit on the number of billing cards you can add to a customer. The first billing card added will be designated as the primary billing card, used for subscription payments. You can use the Set Primary Billing Card API to change the default billing card.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Customer ID
$body = new \qpPlatform\Model\AddBillingCardRequest(); // \qpPlatform\Model\AddBillingCardRequest | Customer

try {
    $result = $apiInstance->addBillingCard($customer_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->addBillingCard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Customer ID |
 **body** | [**\qpPlatform\Model\AddBillingCardRequest**](../Model/AddBillingCardRequest.md)| Customer |

### Return type

[**\qpPlatform\Model\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addCustomer**
> \qpPlatform\Model\CustomerResponse addCustomer($body)

Add a Customer

Adds a customer to the Qualpay Customer Vault. A customer_id is required to create invoice and subscription payments. Billing cards, billing addresses and shipping addresses may be included in this request.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \qpPlatform\Model\AddCustomerRequest(); // \qpPlatform\Model\AddCustomerRequest | Customer

try {
    $result = $apiInstance->addCustomer($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->addCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\qpPlatform\Model\AddCustomerRequest**](../Model/AddCustomerRequest.md)| Customer |

### Return type

[**\qpPlatform\Model\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addShippingAddress**
> \qpPlatform\Model\CustomerResponse addShippingAddress($customer_id, $body)

Add a Shipping Address

Adds a shipping address to a customer. The response will include a unique shipping_id. The shipping_id can be used to manage the shipping_address.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Shipping Address
$body = new \qpPlatform\Model\AddShippingAddressRequest(); // \qpPlatform\Model\AddShippingAddressRequest | Shipping Address

try {
    $result = $apiInstance->addShippingAddress($customer_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->addShippingAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Shipping Address |
 **body** | [**\qpPlatform\Model\AddShippingAddressRequest**](../Model/AddShippingAddressRequest.md)| Shipping Address |

### Return type

[**\qpPlatform\Model\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **browseCustomers**
> \qpPlatform\Model\CustomerListResponse browseCustomers($count, $order_on, $order_by, $page, $filter, $merchant_id)

Get all Customers

Gets an array of customer vault objects. You can use filters to limit the results returned. Optional query parameters determine size and sort order of the returned array.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$count = 10; // int | The number of records in the result.
$order_on = "customer_id"; // string | The field on which the results will be sorted on. Refer to the response model for available fields.
$order_by = "desc"; // string | Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order)
$page = 0; // int | Zero-based page number, use this to choose a page when there are more results than the count parameter.
$filter = "filter_example"; // string | Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter.
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->browseCustomers($count, $order_on, $order_by, $page, $filter, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->browseCustomers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **int**| The number of records in the result. | [optional] [default to 10]
 **order_on** | **string**| The field on which the results will be sorted on. Refer to the response model for available fields. | [optional] [default to customer_id]
 **order_by** | **string**| Ascending or Descending Sort order of the result. Possible values are: asc (Ascending sort order), desc (Descending sort order) | [optional] [default to desc]
 **page** | **int**| Zero-based page number, use this to choose a page when there are more results than the count parameter. | [optional] [default to 0]
 **filter** | **string**| Results can be filtered by custom filter criteria. Refer to [Filter](/developer/api/reference#filters) to use the filter parameter. | [optional]
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\CustomerListResponse**](../Model/CustomerListResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteBillingCard**
> \qpPlatform\Model\CustomerResponse deleteBillingCard($customer_id, $body)

Delete a Billing Card

Deletes a customer's specific billing card based on the card_id provided in the request. The card_id is required in the request body to delete the billing card. A billing card with active subscriptions cannot be deleted.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Customer ID
$body = new \qpPlatform\Model\DeleteBillingCardRequest(); // \qpPlatform\Model\DeleteBillingCardRequest | Customer

try {
    $result = $apiInstance->deleteBillingCard($customer_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->deleteBillingCard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Customer ID |
 **body** | [**\qpPlatform\Model\DeleteBillingCardRequest**](../Model/DeleteBillingCardRequest.md)| Customer |

### Return type

[**\qpPlatform\Model\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteCustomer**
> \qpPlatform\Model\QPApiResponse deleteCustomer($customer_id, $merchant_id)

Delete a Customer

Deletes a customer from the customer vault. Deleting a customer will delete all the subscriptions associated with the customer. **This operation cannot be reversed.**

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Customer ID
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->deleteCustomer($customer_id, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->deleteCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Customer ID |
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\QPApiResponse**](../Model/QPApiResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteShippingAddress**
> \qpPlatform\Model\CustomerResponse deleteShippingAddress($customer_id, $id, $merchant_id)

Delete a Shipping Address

Deletes a customer's specific shipping address based on the shipping_id provided in the request.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Customer ID
$id = 789; // int | Shipping ID
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->deleteShippingAddress($customer_id, $id, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->deleteShippingAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Customer ID |
 **id** | **int**| Shipping ID |
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBillingCards**
> \qpPlatform\Model\GetBillingResponse getBillingCards($customer_id, $merchant_id)

Get Billing Cards

Gets a list of billing cards associated with a customer_id. The response will contain an array of billing_cards.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Customer ID
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->getBillingCards($customer_id, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->getBillingCards: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Customer ID |
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\GetBillingResponse**](../Model/GetBillingResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCustomer**
> \qpPlatform\Model\CustomerResponse getCustomer($customer_id, $merchant_id)

Get by Customer ID

Gets a customer vault record by customer_id.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Customer ID
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->getCustomer($customer_id, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->getCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Customer ID |
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getShippingAddresses**
> \qpPlatform\Model\GetShippingResponse getShippingAddresses($customer_id, $merchant_id)

Get Shipping Addresses

Gets all shipping addresses for a customer. The response will include an array of shipping addresses.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Customer ID
$merchant_id = 0; // int | Unique ID assigned by Qualpay to a merchant.

try {
    $result = $apiInstance->getShippingAddresses($customer_id, $merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->getShippingAddresses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Customer ID |
 **merchant_id** | **int**| Unique ID assigned by Qualpay to a merchant. | [optional] [default to 0]

### Return type

[**\qpPlatform\Model\GetShippingResponse**](../Model/GetShippingResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setPrimaryBillingCard**
> \qpPlatform\Model\CustomerResponse setPrimaryBillingCard($customer_id, $body)

Set Primary Billing Card

Sets a customer's specific billing card as the primary billing card. A primary billing card is used for subscription payments.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Customer ID
$body = new \qpPlatform\Model\SetPrimaryBillingCardRequest(); // \qpPlatform\Model\SetPrimaryBillingCardRequest | Customer

try {
    $result = $apiInstance->setPrimaryBillingCard($customer_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->setPrimaryBillingCard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Customer ID |
 **body** | [**\qpPlatform\Model\SetPrimaryBillingCardRequest**](../Model/SetPrimaryBillingCardRequest.md)| Customer |

### Return type

[**\qpPlatform\Model\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setPrimaryShippingAddress**
> \qpPlatform\Model\CustomerResponse setPrimaryShippingAddress($customer_id, $body)

Set Primary Shipping Address

Sets a customer's specific shipping address as primary.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Customer ID
$body = new \qpPlatform\Model\SetPrimaryShippingAddressRequest(); // \qpPlatform\Model\SetPrimaryShippingAddressRequest | Shipping Address

try {
    $result = $apiInstance->setPrimaryShippingAddress($customer_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->setPrimaryShippingAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Customer ID |
 **body** | [**\qpPlatform\Model\SetPrimaryShippingAddressRequest**](../Model/SetPrimaryShippingAddressRequest.md)| Shipping Address |

### Return type

[**\qpPlatform\Model\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateBillingCard**
> \qpPlatform\Model\CustomerResponse updateBillingCard($customer_id, $body)

Update a Billing Card

Updates a customer's billing card by card_id. The complete billing record should be included in the request.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Customer ID
$body = new \qpPlatform\Model\UpdateBillingCardRequest(); // \qpPlatform\Model\UpdateBillingCardRequest | Customer

try {
    $result = $apiInstance->updateBillingCard($customer_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->updateBillingCard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Customer ID |
 **body** | [**\qpPlatform\Model\UpdateBillingCardRequest**](../Model/UpdateBillingCardRequest.md)| Customer |

### Return type

[**\qpPlatform\Model\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateCustomer**
> \qpPlatform\Model\CustomerResponse updateCustomer($customer_id, $body)

Update a Customer

Updates a customer vault record. You can send the entire resource or only the fields that require an update. When updating array fields - billing_cards or shipping_addresses - the entire array should be included in the request body.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Customer ID
$body = new \qpPlatform\Model\UpdateCustomerRequest(); // \qpPlatform\Model\UpdateCustomerRequest | Customer

try {
    $result = $apiInstance->updateCustomer($customer_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->updateCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Customer ID |
 **body** | [**\qpPlatform\Model\UpdateCustomerRequest**](../Model/UpdateCustomerRequest.md)| Customer |

### Return type

[**\qpPlatform\Model\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateShippingAddress**
> \qpPlatform\Model\CustomerResponse updateShippingAddress($customer_id, $body)

Update a Shipping Address

Update a shipping address for a customer.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\CustomerVaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Shipping Address
$body = new \qpPlatform\Model\UpdateShippingAddressRequest(); // \qpPlatform\Model\UpdateShippingAddressRequest | Shipping Address

try {
    $result = $apiInstance->updateShippingAddress($customer_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerVaultApi->updateShippingAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Shipping Address |
 **body** | [**\qpPlatform\Model\UpdateShippingAddressRequest**](../Model/UpdateShippingAddressRequest.md)| Shipping Address |

### Return type

[**\qpPlatform\Model\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

