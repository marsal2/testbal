<?php

function render_m34_article_content( $module_data, $id = '', $wide = false, $wrap_class='', $h1_class='') {
  $output = '';
  $id = empty( $id ) ? '' : ' id="' . $id . '"';
  $class = empty($wide) ? 'col-sm-8' : '';
  if ( !empty( $module_data['title'] ) || !empty( $module_data['copy'] ) ) {
    $output .= '<!-- M34A: SECTION -->';
    $output .= '<article '.$id.' aria-label="article for ' .str_replace(array("'",'"'),'', $module_data["title"]). '" class="text-block default-content-style '.$wrap_class.'">';
    $output .=   '<div class="container">';
    $output .=     '<div class="row">';
    $output .=       '<div class="'.$class.'">';
    $output .=         !empty( $module_data['title'] ) ? '<h1 class="text-info '.$h1_class.'">'.$module_data['title'].'</h1>' : '';
    $output .=         !empty( $module_data['copy'] ) ? apply_filters( 'the_content', html_entity_decode( $module_data['copy'] ) ) : '';
    $output .=       '</div>';
    $output .=     '</div>';
    $output .=   '</div>';
    $output .=  '</article>';
    $output .= '<!-- end M34A: SECTION -->';
  }

  return stripslashes( $output );
}
