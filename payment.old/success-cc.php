<?php
if(session_id() == '') {
  session_start();
}
$config = require_once('config.php');
?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Balance credit card</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?=$config['BASE_URL'];?>/styles/main.css">
    </head>
    <body>

      <!-- Main -->
      <div class="main">
        <h1>Transaction approved! Thank you for your order.</h1>
        <p>You will be redirected in 10 seconds.</p>
      </div>

      <?php

      //Check whether we stored a server response after initial form submit.
      $initialformsubmit = array();
      if(!empty($_SESSION['initialformsubmit'])) {
        $initialformsubmit = $_SESSION['initialformsubmit'];
        unset($_SESSION['initialformsubmit']);
      }

      require_once('email.php');

      $cc_email = '';

      $email_key_map = array(
        'cardholder-name' => 'Name Surname',
        'cardholder-address1' => 'Address 1',
        'cardholder-address2' => 'Address 2',
        'cardholder-city' => 'City',
        'cardholder-state' => 'State',
        'cardholder-zip' => 'Zip',
        'cardholder-phone' => 'Phone',
        'email-certificate' => 'Email Certificate',
        'fax-certificate' => 'Fax Certificate',
        'cardholder-email' => 'Email',
        'recipient-fax' => 'Recipient\'s Fax',
        'recipient-name' => 'Recipient\'s Name',
        'recipient-phone' => 'Recipient\'s Phone',
        'payment' => 'Payment For',
        'cardholder-bankruptcy' => 'Bankruptcy Case #',
        'cardholder-amount' => 'Amount'
      );

      // check if any additional fields passed (set in config)
      if ( !empty( $initialformsubmit['additional_field'] ) ) {
        $additional_post_fields = $initialformsubmit['additional_field'];
        if ( is_array( $additional_post_fields ) ) {
          foreach ($additional_post_fields as $key => $value) {
            // create and email key map for additional field
            $email_key_map[$key] = ucwords(str_replace('_', ' ', $key));
            // modify the POST so that it contains the additional field value
            $initialformsubmit[$key] = $initialformsubmit['additional_field'][$key];
          }
        }
        // unset primary additional field from the POST so that it is not evaluated further
        unset($initialformsubmit['additional_field']);
      }

      function getCCEmail($formsubmitdata) {
        global $email_key_map, $cc_email;
        $tbody = array();
        foreach ($email_key_map as $key => $value) {
          // Set email content labels and values based on $_GET['preset']
          if( isset($formsubmitdata[$key]) ) {
            $label = $formsubmitdata[$key] ? $formsubmitdata[$key] : 'No';
            $tbody[] = join('', [
              '<th style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;" colspan="2" scope="row">',
              $value,
              '</th>',
              '<td style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">',
              $label,
              '</td>'
            ]);
          }
        }
        $cc_email = '<tr>' . join('</tr><tr>', $tbody) . '</tr>';
        ob_start();
        require_once('email.cc.admin.php');
        return ob_get_clean();
      }



      $description = $initialformsubmit['payment'];

      $billing_address = $initialformsubmit['cardholder-address1'];
      $billing_address .= !empty( $initialformsubmit['cardholder-address2'] ) ? ', \n' . $initialformsubmit['cardholder-address2'] : '';
      $billing_address .= ', \n' . $initialformsubmit['cardholder-city'];
      $billing_address .= ', \n' . $initialformsubmit['cardholder-state'];
      $billing_address .= ', \n' . $initialformsubmit['cardholder-zip'];

      $preset_key = @$initialformsubmit['use_preset'];
      $use_config = $config['DEFAULT'];
      if(!empty($preset_key)) {
        $use_config = @$config['presets'][$preset_key];
        if(empty($use_config)) {
          renderError("Invalid request");
        }
        if(!empty($use_config['OPTIONS']) && empty($description))
          $description = $use_config['DESCRIPTION'];
      }

      // TO DO: maybe send email to the client?

      sendEmail(
        $use_config['PMT_RECEPIENTS'],
        sprintf($use_config['PMT_SUBJECT_CARD'],$_POST['PNREF']),
        getCCEmail($initialformsubmit)
      );
      ?>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <script>
      // redirect user after successfull transfer
      window.setTimeout(function(){
        window.top.location.href = "<?=$config['HOME_URL'];?>";
      }, 10000);
    </script>

    </body>
</html>
