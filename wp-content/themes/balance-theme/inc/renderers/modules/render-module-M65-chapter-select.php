<?php

function render_m65_chapter_select( $chapters) {
  $output = '';
  if ( !empty( $chapters ) ) {
    foreach( $chapters as $chapter ) {
      $selected = empty($chapter['selected']) ? '' : 'selected="selected"';
      $output.='
        <option value="'.stripslashes($chapter['slug']).'" '.$selected.'>'.
          stripslashes($chapter['title']).
        '</option>';
    }
    $output = '
    <div style="background-image: url('.get_stylesheet_directory_uri().'/images/img03.jpg)" class="chapter-block">
      <div class="container">
        <div class="row">
          <form action="#" class="chapter-form col-sm-12">
            <div class="input-group"><span class="label">
                <label for="chapter-select">'.__('Jump to', 'balance').':</label></span>
              <div class="select-holder">
                <select id="chapter-select" class="jump anchor-select">
                  '.$output.'
                </select>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>';
  }
  return $output;
}