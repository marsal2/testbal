<?php
/*
 *  WP Edit module: M13
 *  Description: Module with bullets. M13 – Bulleted list with links
 */

function m13_module_form( $key, $visible_on = 'all', $module_title = '') {
  global $data;

  $output = simple_edit_module($key, 'm13', array(
    array(
      'type' => 'repeater',
      'name' => 'bullets',
      'label' => 'Item',
      'default_value' => array(),
      'fields' => array(
        array(
          'type' => 'text',
          'name' => 'copy',
          'label' => 'Bullet copy',
          'default_value' => ''
        ),
        array(
          'type' => 'text',
          'name' => 'linkfield',
          'label' => 'Bullet link',
          'default_value' => array()
        ),
      )
    )
  ), $data, $module_title, $visible_on);

  return $output;

}

?>
