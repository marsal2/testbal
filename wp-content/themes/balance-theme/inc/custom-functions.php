<?php
$theme_namespace = 'balance';
/**
 * Function to clean string of special characters (only aA-zZ, 0-9 and '_' allowed) and replace them with single '_' (if two special are together)
 *
 * @param unknown $string string to be cleaned of special characters
 * @return string cleaned string
 */
function clean( $string ) {
  $string = str_replace( ' ', '-', $string ); // Replaces all spaces with hyphens.
  $string = str_replace( '[', '_', $string ); // Replaces all spaces with hyphens.
  $string = str_replace( ']', '_', $string ); // Replaces all spaces with hyphens.
  $string = str_replace( '-', '_', $string ); // Replaces all spaces with hyphens.
  $string = str_replace( '___', '_', $string ); // Replaces all spaces with hyphens.
  $string = str_replace( '__', '_', $string ); // Replaces all spaces with hyphens.
  return preg_replace( '/[^A-Za-z0-9\-]/', '_', $string ); // Removes special chars.
}

/* Filter Tiny MCE Default Settings */
add_filter( 'tiny_mce_before_init', 'my_switch_tinymce_p_br' );

/**
 * Switch Default Behaviour in TinyMCE to use "<br>"
 * On Enter instead of "<p>"
 *
 * @link https://shellcreeper.com/?p=1041
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/tiny_mce_before_init
 * @link http://www.tinymce.com/wiki.php/Configuration:forced_root_block
 */
function my_switch_tinymce_p_br( $settings ) {
  $settings['forced_root_block'] = false;
  return $settings;
}
/**
 * Change text for the post excerpt
 *
 * @since 1.0.0
 * @param string  $translated_text
 * @param string  $text
 * @param string  $domain
 * @return string
 */
function change_excerpt_name( $translated_text, $text, $domain ) {
  switch ( $translated_text ) {
  case 'Excerpts are optional hand-crafted summaries of your content that can be used in your theme. <a href="http://codex.wordpress.org/Excerpt" target="_blank">Learn more about manual excerpts.</a>' :
    $translated_text = 'Enter short summary of the content';
    break;
  default :
    break;
  }
  return $translated_text;
}
add_filter( 'gettext', 'change_excerpt_name', 20, 3 );


/**
 *  Get the file size of any remote resource (using get_headers()),
 *  either in bytes or - default - as human-readable formatted string.
 *
 *  @author  Stephan Schmitz <eyecatchup@gmail.com>
 *  @license MIT <http://eyecatchup.mit-license.org/>
 *  @url     <https://gist.github.com/eyecatchup/f26300ffd7e50a92bc4d>
 *
 *  @param   string   $url          Takes the remote object's URL.
 *  @param   boolean  $formatSize   Whether to return size in bytes or formatted.
 *  @return  string                 Returns human-readable formatted size
 *                                  or size in bytes (default: formatted).
 */
function get_remote_file_size( $url, $formatSize = true ) {
  $head = array_change_key_case( get_headers( $url, 1 ) );
  // content-length of download (in bytes), read from Content-Length: field
  $clen = isset( $head['content-length'] ) ? $head['content-length'] : 0;

  // cannot retrieve file size, return "-1"
  if ( !$clen ) {
    return -1;
  }

  if ( !$formatSize ) {
    return $clen; // return size in bytes
  }

  $size = $clen;
  switch ( $clen ) {
  case $clen < 1024:
    $size = $clen .' B'; break;
  case $clen < 1048576:
    $size = round( $clen / 1024, 2 ) .' KB'; break;
  case $clen < 1073741824:
    $size = round( $clen / 1048576, 2 ) . ' MB'; break;
  case $clen < 1099511627776:
    $size = round( $clen / 1073741824, 2 ) . ' GB'; break;
  }

  return $size; // return formatted size

  // $url = 'http://download.tuxfamily.org/notepadplus/6.6.9/npp.6.6.9.Installer.exe';
  // echo get_remote_file_size($url); // echoes "7.51 MB"
}


/**
 * Returns the size of a file without downloading it, or -1 if the file
 * size could not be determined.
 *
 * @param unknown $url - The location of the remote file to download. Cannot
 * be null or empty.
 *
 * @return The size of the file referenced by $url, or -1 if the size
 * could not be determined.
 */
function curl_get_file_size( $url ) {
  // Assume failure.
  $result = -1;

  $curl = curl_init( $url );

  // Issue a HEAD request and follow any redirects.
  curl_setopt( $curl, CURLOPT_NOBODY, true );
  curl_setopt( $curl, CURLOPT_HEADER, true );
  curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
  curl_setopt( $curl, CURLOPT_USERAGENT, get_user_agent_string() );

  $data = curl_exec( $curl );
  curl_close( $curl );

  if ( $data ) {
    $content_length = "unknown";
    $status = "unknown";

    if ( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
      $status = (int)$matches[1];
    }

    if ( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
      $content_length = (int)$matches[1];
    }

    // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
    if ( $status == 200 || ( $status > 300 && $status <= 308 ) ) {
      $result = $content_length;
    }
  }

  return $result;
}

// http://stackoverflow.com/questions/14114411/remove-all-special-characters-from-a-string
function hyphenize( $string ) {
  $dict = array(
    "I'm"      => "I am",
    "thier"    => "their",
  );
  return strtolower(
    preg_replace(
      array( '#[\\s-]+#', '#[^A-Za-z0-9\. -]+#' ),
      array( '-', '' ),
      // the full cleanString() can be download from http://www.unexpectedit.com/php/php-clean-string-of-utf8-chars-convert-to-similar-ascii-char
      cleanString(
        str_replace( ':', '',
          str_replace( ';', '',
            str_replace( ',', '',
              str_replace( // preg_replace to support more complicated replacements
                array_keys( $dict ),
                array_values( $dict ),
                urldecode( $string )
              )
            )
          )
        )
      )
    )
  );
}

function cleanString( $text ) {
  $utf8 = array(
    '/[áàâãªä]/u'   =>   'a',
    '/[ÁÀÂÃÄ]/u'    =>   'A',
    '/[ÍÌÎÏ]/u'     =>   'I',
    '/[íìîï]/u'     =>   'i',
    '/[éèêë]/u'     =>   'e',
    '/[ÉÈÊË]/u'     =>   'E',
    '/[óòôõºö]/u'   =>   'o',
    '/[ÓÒÔÕÖ]/u'    =>   'O',
    '/[úùûü]/u'     =>   'u',
    '/[ÚÙÛÜ]/u'     =>   'U',
    '/ç/'           =>   'c',
    '/Ç/'           =>   'C',
    '/ñ/'           =>   'n',
    '/Ñ/'           =>   'N',
    '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
    '/[’‘‹›‚]/u'    =>   '-', // Literally a single quote
    '/[“”«»„]/u'    =>   '-', // Double quote
    '/ /'           =>   '-', // nonbreaking space (equiv. to 0x160)
  );
  return preg_replace( array_keys( $utf8 ), array_values( $utf8 ), $text );
}

/**
 * Function which gets the <img> tag from string end returns its src using preg_match_all()
 * */
function catch_that_image( $content_with_image ) {
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all( '/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content_with_image, $matches );
  if ( !empty( $matches[1] ) ) {
    $first_img = $matches[1][0];
  }
  if ( empty( $first_img ) ) {
    $first_img = '';
  }
  return $first_img;
}

// retrieves the formatted name for hour id ... 5 ==> 05:00 AM
function get_hours( $time_id , $minutes = '00' ) {
  $return_time = '';
  $time_id = intval( $time_id );
  switch ( $time_id ) {
  case $time_id > 12:
    $return_time = $time_id - 12 ;
    break;
  case $time_id < 10;
    $return_time = '0' . $time_id;
    break;
  default:
    $return_time = $time_id;
    break;
  }

  $return_time .= ':' . $minutes;

  if ( $time_id > 12 || $time_id == 24 ) {
    $return_time .= ' PM';
  } else {
    $return_time .= ' AM';
  }

  return $return_time;
}

function get_protocol_relative_home_url( $return = false ) {
  $url = home_url( '/', '' );
  $find = array( 'http://', 'https://' );
  $replace = '';
  $output = '//' . str_replace( $find, $replace, $url );
  if ( $return ) {
    return $output;
  }
  else {
    echo $output;
  }
}

function get_current_git_branch() {
  $stringfromfile = file( '../.git/HEAD', FILE_USE_INCLUDE_PATH );
  if ( !empty( $stringfromfile ) ) {
    $firstLine = $stringfromfile[0]; //get the string from the array
    $explodedstring = explode( "/", $firstLine, 3 );
    if ( count( $explodedstring ) > 1 ) {
      $explodedstring = array_slice( $explodedstring , 2 );
      $branchname = implode( "/", $explodedstring );
      echo "<div style='position:fixed;right:0;top:0;opacity:0.3;background:#000;z-index:9999;color:#fff;padding:5px;'>Branch: <span>" . $branchname . "</span></div>"; //show it on the page
    }
  }
}

function js_wp_editor( $settings = array() ) {
  if ( ! class_exists( '_WP_Editors' ) )
    require ABSPATH . WPINC . '/class-wp-editor.php';
  $set = _WP_Editors::parse_settings( 'apid', $settings );
  if ( !current_user_can( 'upload_files' ) )
    $set['media_buttons'] = false;
  if ( $set['media_buttons'] ) {
    wp_enqueue_script( 'thickbox' );
    wp_enqueue_style( 'thickbox' );
    wp_enqueue_script( 'media-upload' );
    $post = get_post();
    if ( ! $post && ! empty( $GLOBALS['post_ID'] ) )
      $post = $GLOBALS['post_ID'];
    wp_enqueue_media( array(
        'post' => $post
      ) );
  }
  _WP_Editors::editor_settings( 'apid', $set );
  $ap_vars = array(
    'url' => get_home_url(),
    'includes_url' => includes_url(),
    'stylesheet_directory_uri' => get_stylesheet_directory_uri()
  );
  wp_register_script( 'ap_wpeditor_init', get_template_directory_uri() . '/js/admin/js-wp-editor.js', array( 'jquery' ), '1.1', true );
  wp_enqueue_style( 'editor-styles', includes_url(). 'css/editor.css', false, THEME_VERSION, 'all' );
  wp_localize_script( 'ap_wpeditor_init', 'ap_vars', $ap_vars );
  wp_enqueue_script( 'ap_wpeditor_init' );
}

function get_link_href( $link_value_holder ) {
  $href = '';
  if ( !empty( $link_value_holder["url_page_id"] ) ) {
    $href = get_relative_permalink( $link_value_holder["url_page_id"] );
  } else if ( !empty( $link_value_holder["url"] ) ) {
      $href = $link_value_holder["url"];
    } else {
    $href = '#';
  }
  return $href;
}

function get_img_data( $img_id ) {
  $img_post_data = get_post( $img_id );
  if ( !empty( $img_post_data ) && isset( $img_post_data ) ) {
    $data = array(
      'src' => wp_get_attachment_url( $img_id )
      , 'thumb_src' =>  wp_get_attachment_thumb_url( $img_id )
      , 'alt' => get_post_meta( $img_id, '_wp_attachment_image_alt', true )
      , 'title' => $img_post_data->post_title
      , 'description' => $img_post_data->post_content
      , 'caption' => $img_post_data->post_excerpt
    );
    return $data;
  }
  else
    return false;
}
function get_relative_permalink( $page_id ) {
  $url = parse_url( get_permalink( $page_id ) );
  return $url['path'];
}

/**
 * Get the posts related to $post_id using resource_tag references
 * in case posts have same count of shared tags, they are sorted from
 * newer > later
 *
 * @param integer $post_id to get related posts for
 * @return integer $nr number of most related post ids to return
 */
function get_related_posts_by_resource_tags( $post_id, $nr = 4 ) {
  $tags = wp_get_post_terms( $post_id, 'resource_tag' , array( 'fields' => 'ids' ) );
  if ( count( $tags ) === 0 )
    return array();
  // query the posts
  global $wpdb;
  $query =
    "select ".
    " p.ID, p.post_type, p.post_title ".
    " from ".
    "   wp_term_taxonomy tt, wp_term_relationships tr, wp_posts p ".
    "   inner join wp_postmeta pm on pm.post_id = p.ID " .
    "where ".
    "  ( pm.meta_key = 'list_in_search' AND pm.meta_value = 'true' ) and " .
    "  tt.term_taxonomy_id=tr.term_taxonomy_id and ".
    "  tt.term_id in (" . ( implode( ',',  array_map( 'intval', $tags ) ) ) . ") and ".
    "  tr.object_id = p.ID and ".
    "  p.post_status = 'publish' and ".
    "  tr.object_id != ".intval( $post_id )." ".
    "group by tr.object_id ".
    "order by count(tr.term_taxonomy_id) desc, p.post_date desc limit $nr;"; // if same tag count, prefer newer posts
  //echo "query".$query;
  $posts = $wpdb->get_results( $query );
  // splice it to requested num
  $posts = array_splice( $posts, 0, $nr );
  return $posts;
}

add_action( 'wp_ajax_load_resources', 'load_resources' );
add_action( 'wp_ajax_nopriv_load_resources', 'load_resources' );
function load_resources( $searchQuery = '', $userLoggedIn = false ) {
  // if ( empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
  //  header("HTTP/1.1 500 Internal Server Error", true, 500);
  //      echo json_encode(array('resources' => array(), 'error' => '1', 'error_message' => 'Permission denied!'));
  // } else {
  global $wpdb;
  $mysqli = new \mysqli( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME );
  // require_once dirname( __FILE__ ).'/Search.php';
  require_once WPMU_PLUGIN_DIR.'/balance-white-label-websites/app/Modules/Search.php';
  $resources = new App\Modules\Search( $mysqli, $wpdb->prefix );
  $data = 'resources';
  if ( !empty( $_GET['data'] ) ) {
    $data = $_GET['data'];
  }
  switch ( $data ) {
  case 'categories':
    echo $resources->getTaxonomies();
    break;
  default:
      $offset = $_GET['pager'];
      $resourceType = isset($_GET['type'])?$_GET['type']:'';
      $sortBy = isset($_GET['sortBy'])?$_GET['sortBy']:'';
      $tag = isset($_GET['tag'])?$_GET['tag']:'';
      $stage = isset($_GET['stage'])?$_GET['stage']:'';
      echo $resources->getResources( 0, $_GET['query'], $userLoggedIn,0,$offset,$resourceType,$sortBy,$tag,$stage);
    break;
  }


  // }
  die();
}




function quiz_submit( $post_id = -1, $user_id = -1, $score = 0, $correct = 0, $total = 0 ) {
  global $wpdb; // used by queries below

  $response = ['error' => false, 'message' => ''];

  if ( $post_id < 1 ) {
    $response['error'] = true;
    $response['message'] = "invalid post id";
    return $response;
  }

  if ( $user_id < 1 ) {
    $response['error'] = true;
    $response['message'] = "user not logged in";
    return $response;
  }

  // get quiz module settings
  $meta = get_post_meta( $post_id, '_page_edit_data', true );
  if ( empty( $meta ) ) {
    $response['error'] = true;
    $response['message'] = "invalid post meta";
    return $response;
  }

  $full_title = $meta['post_title'];
  $title = explode(': ', $full_title);

  $meta = $meta['quiz_module'][0];

  if ( ! array_key_exists( 'has_certificate', $meta ) || ! $meta['has_certificate'] ) {
    $response['error'] = true;
    $response['message'] = "quiz does not have certificate";
    return $response;
  }
  if ( array_key_exists( 'success_rate', $meta ) && $score < $meta['success_rate'] ) {
    $response['error'] = true;
    $response['message'] = "score not high enough";
    return $response;
  }

    $response['cert'] = "will be sent";

    // get user data to fill in their personal information
    $user = get_userdata( $user_id );

    // see if we're on partner site
        //$partner_domain = $_GET['partner'];
        $partner_domain = 'tinkerfcu.testxekera.com';
    if ($partner_domain) {
      // if it's a partner site, override the user's name with the one from the partner user table.
      $row = $wpdb->get_row($wpdb->prepare("SELECT whitelabel_users.firstname, whitelabel_users.lastname FROM whitelabel_users INNER JOIN wp_white_label_websites ON whitelabel_users.white_label_website_id = wp_white_label_websites.white_label_website_id WHERE wp_white_label_websites.domain = %s and whitelabel_users.email = %s", $partner_domain, $user->user_email));
      if ($row) {
        $user->first_name = $row->firstname;
        $user->last_name = $row->lastname;
      }
    }

    error_log("User [${user_id}] passed the test with score: [${score}/${total}]. Quiz [${post_id}]. User e-mail present [" . (empty( $user->user_email ) ? "NO" : "YES") ."]");

    // fill in the response
    $response['user'] = $user->first_name .' ' . $user->last_name;
    $response['mail_subject'] = isset( $meta['email_subject'] ) ? $meta['email_subject'] : 'Your certificate of completion';
    $response['mail_body'] = isset( $meta['email_body'] ) ? $meta['email_body'] : '';

    // load certificate assets
    $fonts_src = get_template_directory() . '/inc/fpdf/fonts/';
    $bg_image = get_template_directory() . '/images/certificate_background.jpg';
    $sig_image = get_template_directory() . '/images/signature.png';

    // create PDF
    require_once( get_template_directory() . '/inc/fpdf/fpdf.php' );
    $pdf = new FPDF('L', 'mm', 'Letter');
    $pdf->SetAutoPageBreak(false);
    $pdf->SetMargins(109, 10);
    $pdf->AddFont('Gotham', 'B', 'Gotham-Bold.php');
    $pdf->AddFont('Gotham', '', 'Gotham-Light.php');
    $pdf->AddFont('DinoCond', '', 'DINOT-CondRegular.php');
    $pdf->AddPage();
    $pdf->Image($bg_image, 0, 0, 279.4, 215.9);
    $pdf->SetFont('DinoCond','', 40);
    $pdf->SetTextColor(115, 205, 215);
    $pdf->Cell(165, 15, $meta['certificate_title'], 0, 2);
    $pdf->SetFont('Gotham','', 25);
    $pdf->Cell(165, 30, 'Awarded to', 0, 2);
    $pdf->SetTextColor(68, 110, 106);
    $pdf->Cell(165, 20, $user->first_name . ' ' . $user->last_name, 0, 2);
    $pdf->SetTextColor(122, 213, 218);
    $pdf->Cell(165, 25, 'for your completion of', 0, 2);
    $pdf->SetFont('Gotham', 'B', 24);
    $pdf->Cell(100, 20, $title[0] . ':', 0, 2);
    $pdf->Cell(100, 15, $title[1], 0, 2);
    $pdf->Image($sig_image, $pdf->GetX() + 2, $pdf->GetY() + 8, 60, 15);
    $pdf->SetY($pdf->GetY() + 20);
    $pdf->SetTextColor(68, 110, 106);
    $pdf->SetFont('Gotham', '', 11.5);
    $pdf->Cell(72, 5, 'Rico C. Delgadillo', 0, 2);
    $pdf->Cell(72, 5, 'Interim President &Chief Executive Officer', 0, 2);
    $pdf->Cell(72, 5, 'BALANCE', 0, 0);
    $pdf->SetXY($pdf->GetX() + 18, $pdf->GetY() - 10);
    $pdf->Cell(72, 5, 'Date', 0, 2);
    $pdf->SetXY($pdf->GetX(), $pdf->GetY() - 12);
    $pdf->Cell(72, 5, date( get_option( 'date_format' ) ), 0, 0);
    $pdf->SetY($pdf->GetY() + 38, true);
    //$pdf->Cell(72, 20, 'union logo', 1, 0, 'C');

    //$content = $pdf->Output('S', '', true);
    //$content = chunk_split(base64_encode($content));

    $meta['email_body'] = str_replace(
      ['{{first}}', '{{last}}', '{{quiz_title}}'],
      [ $user->first_name, $user->last_name, $full_title ],
      $meta['email_body']
    );

    // Email settings
    if ( empty( $user->user_email ) ) {
      $response['error'] = true;
      $response['message'] = "no user email";

      return $response;
    }

    // send email with PDF as attachment
    $mailto = $user->user_email;
    $subject = empty( $meta['email_subject'] ) ? 'Your Balance certificate' : $meta['email_subject'];
    $response['email_body'] = $meta['email_body'];
    $message = empty ( $meta['email_body'] ) ? ' ' : $meta['email_body'];
    $message .= " Your score was $score% ($correct/$total).";

    global $phpmailer;
    add_action( 'phpmailer_init', function( & $phpmailer ) use( $pdf ) {
      $phpmailer->AddStringAttachment( $pdf->Output('', 'S'), 'BALANCE_certificate.pdf', 'base64', 'application/pdf' );
    });

    $sent = wp_mail( $mailto, $subject, $message );
    $response['error'] = false;
    $response['mail_sent'] = $sent;

    error_log("Email sent to [${user_id}] for quiz [${post_id}].");

    // send a separate email to admins with a copy of the certificate
    // retrieve any white label admin addresses associated with the white label website associated with this user
    $admin_addresses = $wpdb->get_col($wpdb->prepare("SELECT whitelabel_admins.email FROM whitelabel_admins INNER JOIN wp_white_label_websites ON whitelabel_admins.white_label_website_id = wp_white_label_websites.white_label_website_id WHERE wp_white_label_websites.domain = %s", $partner_domain));

    // send to certificates@balancepro.org if no other admin addresses
    if( !$admin_addresses )
      $admin_addresses[] = "certificates@balancepro.org";

    foreach( $admin_addresses as $address ) {
      $admin_message = "$user->first_name $user->last_name earned the $full_title Certificate of Completion with a score of $score% ($correct/$total).";
      wp_mail($address, "$full_title Certificate of Completion for $user->first_name $user->last_name", $admin_message);
    }

    return $response;
}

add_action( 'wp_mail_failed', 'balanceLogMailFailure', 10, 1);
function balanceLogMailFailure( $wp_error ) {
    error_log("Failed to send e-mail: " . ((string) $wp_error->get_error_messages()));
}

// Remove result count & 'order by' drop-down on shop product list.
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

// add custom interval for five minute run if cron task
add_filter( 'cron_schedules', 'balance_add_cron_interval' );
function balance_add_cron_interval( $schedules ) {
    $schedules['five_minutes'] = array(
        'interval' => 300,
        'display'  => esc_html__( 'Every Five Minutes' ),
    );
    return $schedules;
}

// add custom hook into the scheduled events
if ( ! wp_next_scheduled( 'balance_cron_hook' ) ) {
  wp_schedule_event( time(), 'five_minutes', 'balance_cron_hook' );
}

// hook into custom, run the quiz results transfer to mssql code
// add action only if TRANSFER_QUIZ_RESULTS_TO_MSSQL is set to TRUE (should only be set on whitelabel server)
if ( defined('TRANSFER_QUIZ_RESULTS_TO_MSSQL') && TRANSFER_QUIZ_RESULTS_TO_MSSQL === true ){
    add_action( 'balance_cron_hook', 'balance_transfer_quiz_results_to_mssql' );
}

function balance_transfer_quiz_results_to_mssql() {
  if ( defined( 'MS_PROTOCOL' ) && defined( 'MS_DB_HOST' ) && defined( 'MS_DB_USERNAME' ) && defined( 'MS_DB_PASSWORD' ) ) {

    global $wpdb;

    // get all quiz results which were not transferred yet (mssql_id is null, transferred flag is false)
    $query_not_transferred_quiz_results = $wpdb->get_results( "
      SELECT DISTINCT
        wp_quiz_results.ID as ID,
        (SELECT wp_postmeta.meta_value FROM wp_postmeta WHERE wp_postmeta.post_ID = wp_quiz_results.wp_post_id AND wp_postmeta.meta_key = 'balance_quiz_id') AS ExamCode,
        whitelabel_users.firstname AS FirstName,
        whitelabel_users.lastname AS LastName,
        whitelabel_users.email AS Email,
        whitelabel_users.streetAddress AS StreetAddress,
        whitelabel_users.city AS City,
        whitelabel_users.state AS State,
        whitelabel_users.zip AS Zip,
        whitelabel_users.memberNumber AS MemberNumber,
        wp_white_label_websites.sylinkid AS SyLink,
        wp_quiz_results.num_correct AS Score,
        wp_quiz_results.timestamp AS CreatedTimeStamp
      FROM
        wp_quiz_results
      INNER JOIN
        wp_users
        ON wp_users.ID = wp_quiz_results.wp_user_id
      INNER JOIN
        whitelabel_users
        ON whitelabel_users.email = wp_users.user_email
      INNER JOIN
        wp_white_label_websites
        ON wp_white_label_websites.white_label_website_id = whitelabel_users.white_label_website_id
      WHERE
       wp_quiz_results.transferred = 0
       AND
       wp_quiz_results.mssql_id IS NULL
    " );
        // 5070 ID is linked to test account (janez.svetin@gmail.com, test1234)
    try {

      $mssql_db = new PDO(MS_PROTOCOL.":host=".MS_DB_HOST, MS_DB_USERNAME, MS_DB_PASSWORD); // read from wp-config.php

      if ( !empty( $query_not_transferred_quiz_results ) ) {
        foreach ( $query_not_transferred_quiz_results as $key => $item ) {
          // query credit union id, it is different than sylink_id (see \balance-private-label-websites\app\Http\Controllers\QuizController.php)
          $query_credit_union_id = $mssql_db->prepare("SELECT CreditUnionId FROM [CallCenter].[dbo].[CreditUnion] WHERE PartnerId = :sylink");
          $query_credit_union_id->execute([':sylink' => $item->SyLink]);
          $credit_union_id = $query_credit_union_id->fetchAll();
          if ( !empty( $credit_union_id ) ) {
            $credit_union_id = $credit_union_id[0]['CreditUnionId'];
          } else {
            $credit_union_id = 0;
          }
          // logic from (\balance-private-label-websites\app\Repositories\QuizRepository.php), differentiation between quizzes with ID 99 (Partner Orientation) and higher than 19
          if ( $item->ExamCode == 99 || empty($item->ExamCode) ) {
            $insert_quiz_results = $mssql_db->prepare( "INSERT INTO [PartnerOrientation].[dbo].[UserExam]
                ( FirstName, LastName, Email, MemberNumber, ReferringCreditUnionId, Score, CreatedTimeStamp )
                OUTPUT INSERTED.UserExamID
                VALUES
                ( :FirstName, :LastName, :Email, :MemberNumber, :ReferringCreditUnionId, :Score, :CreatedTimeStamp )
            " );
        error_log('INSERTING NEW QUIZ');
          } else if ( $item->ExamCode == 30 || empty($item->ExamCode) ) {
            $insert_quiz_results = $mssql_db->prepare( "INSERT INTO [PersonalFinance].[dbo].[UserExam]
                ( ExamCode, FirstName, LastName, Email, StreetAddress, City, State, Zip, MemberNumber, ReferringCreditUnionId, Score, CreatedTimeStamp )
              OUTPUT INSERTED.UserExamID
              VALUES
                ( :ExamCode, :FirstName, :LastName, :Email, :StreetAddress, :City, :State, :Zip, :MemberNumber, :ReferringCreditUnionId, :Score, :CreatedTimeStamp )
            " );

            $insert_quiz_results->bindParam(':ExamCode', $item->ExamCode);
            $insert_quiz_results->bindParam(':StreetAddress', $item->StreetAddress);
            $insert_quiz_results->bindParam(':City', $item->City);
            $insert_quiz_results->bindParam(':State', $item->State);
            $insert_quiz_results->bindParam(':Zip', $item->Zip);

            error_log('INSERTING NEW QUIZ');
          } else if ( $item->ExamCode == 31 || empty($item->ExamCode) ) {
            $insert_quiz_results = $mssql_db->prepare( "INSERT INTO [PersonalFinance].[dbo].[UserExam]
                ( ExamCode, FirstName, LastName, Email, StreetAddress, City, State, Zip, MemberNumber, ReferringCreditUnionId, Score, CreatedTimeStamp )
              OUTPUT INSERTED.UserExamID
              VALUES
                ( :ExamCode, :FirstName, :LastName, :Email, :StreetAddress, :City, :State, :Zip, :MemberNumber, :ReferringCreditUnionId, :Score, :CreatedTimeStamp )
            " );

            $insert_quiz_results->bindParam(':ExamCode', $item->ExamCode);
            $insert_quiz_results->bindParam(':StreetAddress', $item->StreetAddress);
            $insert_quiz_results->bindParam(':City', $item->City);
            $insert_quiz_results->bindParam(':State', $item->State);
            $insert_quiz_results->bindParam(':Zip', $item->Zip);

            error_log('INSERTING NEW QUIZ');
          } else if ( $item->ExamCode == 20 || empty($item->ExamCode) ) {
            $insert_quiz_results = $mssql_db->prepare( "INSERT INTO [InBalance].[dbo].[UserExam]
                ( FirstName, LastName, Email, MemberNumber, ReferringCreditUnionId, Score, CreatedTimeStamp )
                OUTPUT INSERTED.UserExamID
                VALUES
                ( :FirstName, :LastName, :Email, :MemberNumber, :ReferringCreditUnionId, :Score, :CreatedTimeStamp )");
            /*$insert_quiz_results = $mssql_db->prepare( "INSERT INTO [InBalance].[dbo].[UserExam]
                ( FirstName, LastName, Email, StreetAddress, City, State, Zip, MemberNumber, ReferringCreditUnionId, Score, CreatedTimeStamp )
              VALUES
                ( :FirstName, :LastName, :Email, :StreetAddress, :City, :State, :Zip, :MemberNumber, :ReferringCreditUnionId, :Score, :CreatedTimeStamp )
            " );*/
            error_log('INSERTING NEW QUIZ');
          } else {
            $insert_quiz_results = $mssql_db->prepare( "INSERT INTO [PersonalFinance].[dbo].[UserExam]
                ( ExamCode, FirstName, LastName, Email, StreetAddress, City, State, Zip, MemberNumber, ReferringCreditUnionId, Score, CreatedTimeStamp )
              OUTPUT INSERTED.UserExamID
              VALUES
                ( :ExamCode, :FirstName, :LastName, :Email, :StreetAddress, :City, :State, :Zip, :MemberNumber, :ReferringCreditUnionId, :Score, :CreatedTimeStamp )
            " );

            $insert_quiz_results->bindParam(':ExamCode', $item->ExamCode);
            $insert_quiz_results->bindParam(':StreetAddress', $item->StreetAddress);
            $insert_quiz_results->bindParam(':City', $item->City);
            $insert_quiz_results->bindParam(':State', $item->State);
            $insert_quiz_results->bindParam(':Zip', $item->Zip);

          }

          $insert_quiz_results->bindParam(':FirstName', $item->FirstName);
          $insert_quiz_results->bindParam(':LastName', $item->LastName);
          $insert_quiz_results->bindParam(':Email', $item->Email);
//          $insert_quiz_results->bindParam(':StreetAddress', $item->StreetAddress);
//          $insert_quiz_results->bindParam(':City', $item->City);
//          $insert_quiz_results->bindParam(':State', $item->State);
//          $insert_quiz_results->bindParam(':Zip', $item->Zip);
          $insert_quiz_results->bindParam(':MemberNumber', $item->MemberNumber);
          $insert_quiz_results->bindParam(':ReferringCreditUnionId', $credit_union_id);
          $insert_quiz_results->bindParam(':Score', $item->Score);
          $insert_quiz_results->bindParam(':CreatedTimeStamp', $item->CreatedTimeStamp);

          $insert_quiz_results->execute();
          $last_insert_id = $insert_quiz_results->fetch(PDO::FETCH_ASSOC);

          // insert row id back to wp_quiz_results on success, flag it
          if ( !empty( $last_insert_id['UserExamID'] ) ) {
            $wpdb->update(
              'wp_quiz_results',
              array(
                'mssql_id' => $last_insert_id['UserExamID'],
                'transferred' => 1
              ),
              array( 'ID' => $item->ID ),
              array(
                '%d',
                '%d'
              ),
              array( '%d' )
            );
            // write to log what happened
            error_log('successfully transferred quiz results with ID ' . $item->ID . ' from MYSQL wp_quiz_results table to MSSQL [InBalance].[dbo].[UserExam]/[PersonalFinance].[dbo].[UserExam] with ID ' . $last_insert_id['UserExamID']);
          } else {
            error_log('transfer of quiz results with ID ' . $item->ID . ' from MYSQL wp_quiz_results table to MSSQL [InBalance].[dbo].[UserExam]/[PersonalFinance].[dbo].[UserExam] was not successful! '. join(' / ',$insert_quiz_results->errorInfo()));
//error_log('transfer of quiz results with ID ' . $item->ID . ' from MYSQL wp_quiz_results table to MSSQL [InBalance].[dbo].[UserExam]/[PersonalFinance].[dbo].[UserExam] was not successful!');
          }
        }
      }
    } catch (PDOException $e) {
      error_log('PDOException: '.$e->getMessage());
      die();
    }
  }
}

// function which creates (registeres) a new user (in wp_users and whitelabel_users table) on request and logins him/her on WP
function register_and_login_custom_user( $new_user_data ) {
  global $wpdb;

  $white_label_website_id = !empty( $new_user_data->white_label_website_id ) ? $new_user_data->white_label_website_id : 0;

  $user_email = '';
  $real_email = false;

  if ( !empty( $new_user_data->email ) ) {
    $user_email = $new_user_data->email;
    $real_email = true;
  } else {
    $user_email = strtolower($new_user_data->firstname . $new_user_data->lastname) . '@notrealemail.com';
  }

  $ID = email_exists($user_email);
  if ($ID == false) { // user with this email does not exist
    $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
    $sanitized_user_login = strtolower(sanitize_user($new_user_data->firstname . $new_user_data->lastname));

    $default_user_name = $sanitized_user_login;
    $i = 1;

    while (username_exists($sanitized_user_login)) {
      $sanitized_user_login = $default_user_name . $i;
      $i++;
    }

    error_log( 'CREATING NEW WP USER:' . $sanitized_user_login . '(' . $user_email . ')' );
    $ID = wp_create_user($sanitized_user_login, $random_password, $user_email);
    if (!is_wp_error($ID)) {
      if ( $real_email ) {
        wp_new_user_notification($ID);
      }

      $user_info = get_userdata($ID);
      wp_update_user(array(
        'ID' => $ID,
        'display_name' => $new_user_data->firstname . ' ' . $new_user_data->lastname,
        'first_name' => $new_user_data->firstname,
        'last_name' => $new_user_data->lastname
      ));

      update_user_meta($ID, 'wlw_id', $white_label_website_id);

      // insert user into relating whitelabel_users table (if does not exist yet)
      $whitelabel_user_exists = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM whitelabel_users WHERE email=%s", $user_email ) );
      if ( $whitelabel_user_exists == '0' ) { // there is no user yet in the whitelabel users table, create one
        $new_wlw_user = $wpdb->insert(
          'whitelabel_users',
          array(
            'firstname'              => $new_user_data->firstname,
            'lastname'               => $new_user_data->lastname,
            'white_label_website_id' => $white_label_website_id,
            'email'                  => $user_email,
            'status'                 => 'active',
            'password'               => $random_password,
            'avatar'                 => 'none',
            'confirmed'              => 1,
            'created_at'             => date('Y-m-d H:i:s'),
            'updated_at'             => date('Y-m-d H:i:s'),
            'streetAddress'          => ( !empty( $new_user_data->streetAddress) ? $new_user_data->streetAddress : ''),
            'city'                   => ( !empty( $new_user_data->city) ? $new_user_data->city : ''),
            'state'                  => ( !empty( $new_user_data->state) ? $new_user_data->state : ''),
            'zip'                    => ( !empty( $new_user_data->zip) ? $new_user_data->zip : ''),
            'memberNumber'           => ( !empty( $new_user_data->memberNumber ) ? $new_user_data->memberNumber : null),
            'is_employee'            => 0,
            'is_super_admin'         => 0
          ),
          array(
            '%s',
            '%s',
            '%d',
            '%s',
            '%s',
            '%s',
            '%s',
            '%d',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%d',
            '%d'
          )
        );
        error_log( 'CREATING NEW WLW USER:' . $wpdb->insert_id );
        // var_dump('CREATING NEW WLW USER:' . $wpdb->insert_id);
      }
    } else { // something went wrong!!!
      // var_dump($ID->get_error_message());
      error_log( 'ERROR CREATING NEW USER:' . $ID->get_error_message() );
    }
  } else { // user with this email already exists
    error_log( 'USER WITH THIS EMAIL ALREADY EXISTS (' . $user_email . ')' );
    // var_dump('USER WITH THIS EMAIL ALREADY EXISTS (' . $user_email . ')');
  }
  $user = get_user_by('id',$ID);
  clean_user_cache($ID);
  wp_clear_auth_cookie();
  wp_set_current_user( $ID );
  wp_set_auth_cookie( $ID , true, false);
  update_user_caches($user);

}
