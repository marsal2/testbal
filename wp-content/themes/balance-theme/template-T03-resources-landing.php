<?php
/*
Template Name: T03: Resources Landing Page
Modules: {"m34[0]":{"name":"M34: Section"}}
*/

?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'resources-landing';
get_custom_data();

get_header();

if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][0] ) ) {
  echo render_m34_section_title_copy( $data['m34_module'][0], true );
}

echo render_m14_15_resources_search();

get_footer();
?>
