<?php

/**
 * Custom login errors
 */
function login_errors_validation( $login_errors, $username, $password ) {
	require_once __DIR__ . '/recaptcha/autoload.php';
	$siteKey = '6LdqjBkTAAAAALZ7NB29sJUPmgOFzWqoxzY9hUl_';
	$secret = '6LdqjBkTAAAAAHi_-ec2PiVJrJC_gqC8rNOMGcoz';
	$recaptcha = new \ReCaptcha\ReCaptcha( $secret );
	$resp = $recaptcha->verify( $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'] );
	if ( !$resp->isSuccess() ) {
		return new WP_Error( 'registration-error', __( 'Invalid request (Captcha not valid).', 'woocommerce' ) );
	}
	return $login_errors;
}
add_filter( 'woocommerce_process_login_errors', 'login_errors_validation', 10, 3 );

/**
 * Custom registration errors
 */
function registration_errors_validation( $reg_errors, $sanitized_user_login, $user_email ) {
	global $woocommerce;
	extract( $_POST );

	if ( strcmp( $password, $password2 ) !== 0 ) {
		return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
	}

	if ( strlen( $password ) < 8 ) {
		return new WP_Error( 'registration-error', __( 'Your password needs to be at least 8 characters long.', 'woocommerce' ) );
	}

	$contains = preg_match( '/^.*(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/', $password );
	if ( !$contains ) {
		return new WP_Error( 'registration-error', __( 'Your password must contain at least one upper and one lower case character and number.', 'woocommerce' ) );
	}

	require_once __DIR__ . '/recaptcha/autoload.php';
	$siteKey = '6LdqjBkTAAAAALZ7NB29sJUPmgOFzWqoxzY9hUl_';
	$secret = '6LdqjBkTAAAAAHi_-ec2PiVJrJC_gqC8rNOMGcoz';
	$recaptcha = new \ReCaptcha\ReCaptcha( $secret );
	$resp = $recaptcha->verify( $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'] );
	if ( !$resp->isSuccess() ) {
		return new WP_Error( 'registration-error', __( 'Invalid request (Captcha not valid).', 'woocommerce' ) );
	}
	return $reg_errors;
}
add_filter( 'woocommerce_registration_errors', 'registration_errors_validation', 10, 3 );

function validate_reset_pasword( $errors, $user ) {

	if ( !empty( $_POST['password_1'] ) && !empty( $_POST['password_2'] ) ) {
		if ( strlen( $_POST['password_1'] ) < 8 ) {
			wc_add_notice( __( 'Your password needs to be at least 8 characters long.', 'woocommerce' ), 'error' );
		}
		$contains = preg_match( '/^.*(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/', $_POST['password_1'] );
		if ( !$contains ) {
			wc_add_notice( __( 'Your password must contain at least one upper and one lower case character and number.', 'woocommerce' ), 'error' );
		}
	}
	return $errors;
}
add_action( 'woocommerce_save_account_details_errors', 'validate_reset_pasword', 10, 3 );
add_action( 'validate_password_reset', 'validate_reset_pasword', 10, 3 );

function do_not_send_password_change_email( $send_email, $user, $userdata ) {
	$send_email = false;
	return $send_email;
}
add_filter( 'send_password_change_email', 'do_not_send_password_change_email', 10, 3 );

/**
 * internet_allow_email_login filter to the authenticate filter hook, to fetch a username based on entered email
 *
 * @param obj     $user
 * @param string  $username [description]
 * @param string  $password [description]
 * @return boolean
 */
function internet_allow_email_login( $user, $username, $password ) {
	if ( is_email( $username ) ) {
		$user = get_user_by_email( $username );
		if ( $user ) $username = $user->user_login;
	}
	return wp_authenticate_username_password( null, $username, $password );
}
add_filter( 'authenticate', 'internet_allow_email_login', 20, 3 );


function save_data_custom( $customer_id, $new_customer_data, $password_generated ) {
	wp_update_user(
		array(
			'ID' => $customer_id,
			'first_name' => $_POST['billing_first_name'],
			'last_name' => $_POST['billing_last_name'],
		)
	);
}

add_action( 'woocommerce_created_customer', 'save_data_custom', 10, 3 );

function custom_lostpassword_url( $default_url = '' ) {
	$wc_password_reset_url = wc_get_page_permalink( 'myaccount' );

	if ( false !== $wc_password_reset_url ) {
		return $wc_password_reset_url . '?action=lost-password';
	} else {
		return $default_url;
	}
}
add_filter( 'lostpassword_url',  'custom_lostpassword_url', 10, 1 );

function save_billing_address_redirect( $user_id, $load_address ) {
	wp_safe_redirect( wc_get_page_permalink( 'myaccount' ) . '?action=view-billing-address' );
	exit();
}
add_action( 'woocommerce_customer_save_address', 'save_billing_address_redirect', 10, 1 );

class woocommerce_confirmation_email {

	public $my_account_id;
	private $user_id;
	private $email_id;
	private $plugin_slug;

	public function __construct() {
		@session_start();
		$this->my_account = get_option( 'woocommerce_myaccount_page_id' );

		add_action( "woocommerce_login_redirect", array( $this, "authorized_is_valid_user" ), 10, 2 );
		add_action( 'wp_ajax_verify_code', array( $this, 'verify_code' ) );
		add_action( 'wp_ajax_nopriv_verify_code', array( $this, 'verify_code' ) );
		add_action( 'wp_ajax_resend_verify_code', array( $this, 'resend_verify_code' ) );
		add_action( 'wp_ajax_nopriv_resend_verify_code', array( $this, 'resend_verify_code' ) );
		add_filter( 'manage_users_columns', array( $this, 'update_user_table' ), 10, 1 );
		add_filter( 'manage_users_custom_column', array( $this, 'new_modify_user_table_row' ), 10, 3 );
		add_filter( "woocommerce_registration_auth_new_customer", array( $this, "new_user_registration" ), 10, 2 );
		add_action( "admin_head", array( $this, "manual_verify_user" ) );
		add_action( "wp", array( $this, "authenticate_user_by_email" ) );
	}

	public function new_user_registration( $customer, $user_id ) {
		$current_user = get_user_by( 'id', $user_id );
		$_SESSION["verify_email"] = array( "verify_email_user" => $user_id );
		wp_logout();
		wp_redirect( get_the_permalink( $this->my_account ) . '?action=login&status=please-verify' );
		exit();
		return $customer;
	}

	function authenticate_user_by_email() {
		if ( isset( $_GET["account-verification-code"] ) ) {
			$user_meta = explode( "@", base64_decode( $_GET["account-verification-code"] ) );
			$verificated = get_user_meta( (int) $user_meta[1], "wcemailverified", TRUE );
			if ( get_user_meta( (int) $user_meta[1], "wcemailverifiedcode", TRUE ) == $user_meta[0] && $verificated != "true" ) {
				update_user_meta( (int) $user_meta[1], "wcemailverified", "true" );
				unset( $_SESSION["verify_email"] );
				$_SESSION["Show_login_message"] = array( "verify_email_user" => $user_meta[1] );
				// TO DO: send email with account information!
				wp_redirect( get_the_permalink( $this->my_account ) . '?action=verify-account&status=success' );
			} else {
				wc_add_notice( __( 'Invalid verification code.', 'woocommerce' ), "error" );
				wp_redirect( get_the_permalink( $this->my_account ) . '?action=login&status=invalid-verification-code' );
			}
		}
		if ( isset( $_GET["wc_confirmation_resend"] ) ) {
			$user_id = base64_decode( $_GET["wc_confirmation_resend"] );
			$this->new_user_registeration( 1, $user_id );
		}
	}

	public function verify_code() {
		if ( isset( $_POST["action"] ) ) {
			extract( $_POST );
			$validation = array();
			if ( get_user_meta( (int) $user_id, "wcemailverifiedcode", TRUE ) == $_POST["verifycode"] ) {
				$validation["valid"] = 1;
				$validation["reload"] = 1;
				update_user_meta( (int) $user_id, "wcemailverified", "true" );
				if ( apply_filters( 'woocommerce_registration_auth_new_customer', true, $user_id ) ) {
					wc_set_customer_auth_cookie( $user_id );
				}
				setcookie( "emailverification_failed", $user_ID, time() - 3600 );
			} else {
				$validation["valid"] = 0;
				$validation["reload"] = 0;
			}
			echo json_encode( $validation );
		}
		exit();
	}

	public function resend_verify_code() {
		extract( $_POST );
		$error["user_id"] = $user_id;
		$this->user_id = $user_id;
		$user = get_user_by( "id", $user_id );
		$scret_code = md5( $user->user_email . time() );
		update_user_meta( (int) $user_id, "wcemailverifiedcode", $scret_code );
		$this->codeMailSender( $user->user_email );
		echo json_encode( array( "resend" => 1 ) );
		exit();
	}

	public function authorized_is_valid_user( $redirect, $user ) {
		$user_ID = $user->ID;
		$status = get_user_meta( (int) $user_ID, "wcemailverified", true );
		if ( !is_super_admin() ) {
			if ( $status != "true" ) {
				$myaccount_id = get_option( 'woocommerce_myaccount_page_id' );
				$location = get_the_permalink( $myaccount_id ) . '?action=login&status=please-verify';
				wc_add_notice( __( 'Your email address is not verified please confirm your email address', 'woocommerce' ), "notice" );
				$_SESSION["verify_email"] = array( "verify_email_user" => $user_ID );
				wp_logout();
				wp_redirect( $location );
			} else {
				unset( $_SESSION["verify_email"] );
				//  setcookie("emailverification_failed", $user_ID, time() - 3600);
				return $redirect;
			}
		} else {
			return $redirect;
		}
	}

	public function update_user_table( $column ) {
		$column['wc_verified'] = 'Verified user';
		$column['wc_manual_verified'] = 'Manual Verify';
		return $column;
	}

	public function new_modify_user_table_row( $val, $column_name, $user_id ) {
		$user_role = get_userdata( $user_id );
		if ( $column_name == "wc_verified" ) {

			if ( $user_role->roles[0] != "administrator" ) {
				if ( get_user_meta( $user_id, "wcemailverified", true ) == "true" ) {
					return "<span class='dashicons dashicons-yes'></span>";
				} else {
					return "<span class='dashicons dashicons-no-alt'></span>";
				}
			} else {
				return "Admin";
			}
		}

		if ( $column_name == "wc_manual_verified" ) {
			if ( $user_role->roles[0] != "administrator" ) {
				if ( get_user_meta( $user_id, "wcemailverified", true ) != "true" ) {
					//$_GET["wc_confirm"]="true";
					$text = "Verify Account";
					return "<a href=" . add_query_arg( array( "user_id" => $user_id, "wp_nonce" => wp_create_nonce( "wc_email" ), "wc_confirm" => "true" ), get_admin_url() . "users.php" ) . ">" . apply_filters( "wc_email_confirmation_manual_verify", $text ) . "</a>";
				}
			}
		}
	}

	public  function manual_verify_user() {
		global $pagenow;
		if ( $pagenow == 'users.php' && isset( $_GET["user_id"] ) && wp_verify_nonce( $_GET["wp_nonce"], "wc_email" ) ) {
			update_user_meta( $_GET["user_id"], "wcemailverified", "true" );
		}
	}

}

function add_notice_when_woocommerc_not_installed() {
?>
    <div class="error">
        <p><?php _e( 'Woocommerce email Confirmation: Error Please Install Woocommerce First', 'woocommerce   ' ); ?></p>
    </div>
    <?php
}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	new woocommerce_confirmation_email();
} else {
	add_action( 'admin_notices', 'add_notice_when_woocommerc_not_installed' );
}

add_filter( 'woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98 );
function wcs_woo_remove_reviews_tab( $tabs ) {
	unset( $tabs['reviews'] );
	return $tabs;
}

function view_order_url( $view_order_url, $order ) {
	$myaccount_id = get_option( 'woocommerce_myaccount_page_id' );
	$location = get_the_permalink( $myaccount_id ) . '?action=view-my-orders&order_id=' . $order->id;
	return $location;
}
add_filter( 'woocommerce_get_view_order_url', 'view_order_url', 10, 2 );

/**
 * Optimize WooCommerce Scripts
 * Remove WooCommerce Generator tag, styles, and scripts from non WooCommerce pages.
 */
add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );

function child_manage_woocommerce_styles() {
	//remove generator meta tag
	remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );

	//first check that woo exists to prevent fatal errors
	if ( function_exists( 'is_woocommerce' ) ) {
		//dequeue scripts and styles
		if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
			wp_dequeue_style( 'woocommerce_frontend_styles' );
			wp_dequeue_style( 'woocommerce_fancybox_styles' );
			wp_dequeue_style( 'woocommerce_chosen_styles' );
			wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
			wp_dequeue_style( 'woocommerce-general' );
			wp_dequeue_style( 'woocommerce-layout' );
			wp_dequeue_style( 'woocommerce-smallscreen' );
			wp_dequeue_script( 'wc_price_slider' );
			wp_dequeue_script( 'wc-single-product' );
			wp_dequeue_script( 'wc-add-to-cart' );
			wp_dequeue_script( 'wc-cart-fragments' );
			wp_dequeue_script( 'wc-checkout' );
			wp_dequeue_script( 'wc-add-to-cart-variation' );
			wp_dequeue_script( 'wc-single-product' );
			wp_dequeue_script( 'wc-cart' );
			wp_dequeue_script( 'wc-chosen' );
			wp_dequeue_script( 'woocommerce' );
			wp_dequeue_script( 'prettyPhoto' );
			wp_dequeue_script( 'prettyPhoto-init' );
			wp_dequeue_script( 'jquery-blockui' );
			wp_dequeue_script( 'jquery-placeholder' );
			wp_dequeue_script( 'fancybox' );
			wp_dequeue_script( 'jqueryui' );
		}
	}

}

function hide_product_tabs( $tabs ) {
	$tabs = array();
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'hide_product_tabs', 10, 2);

function custom_checkout_button( $button_html ) {
	$button_html =  '<input type="submit" class="btn btn-warning" name="woocommerce_checkout_place_order" id="place_order" value="' . __( 'Place Order', 'woocommerce' ) . '" data-value="' . __( 'Place Order', 'woocommerce' ) . '" />';
	return $button_html;
}

add_filter( 'woocommerce_checkout_fields', 'populate_checkout_fields_with_user_data' );
function populate_checkout_fields_with_user_data( $fields ) {
	$fields['billing']['billing_first_name']['default'] = wp_get_current_user()->user_firstname;
	$fields['billing']['billing_last_name']['default'] = wp_get_current_user()->user_lastname;
	return $fields;
}

add_filter( 'woocommerce_order_button_html', 'custom_checkout_button', 10, 3 );
add_filter( 'woocommerce_pay_order_button_html', 'custom_checkout_button', 10, 3 );

add_filter( 'woocommerce_product_add_to_cart_text', 'woo_custom_cart_button_text' );
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );
function woo_custom_cart_button_text() {
  return __( 'Add to Cart', 'woocommerce' );
}

add_filter( 'wc_add_to_cart_message', 'custom_add_to_cart_message', 10, 2 );
function custom_add_to_cart_message($message, $product_id) {
	$titles = array();
	if ( is_array( $product_id ) ) {
		foreach ( $product_id as $id ) {
			$titles[] = get_the_title( $id );
		}
	} else {
		$titles[] = get_the_title( $product_id );
	}

	$titles     = array_filter( $titles );
	return sprintf( _n( '%s has been added to your cart.', '%s have been added to your cart.', sizeof( $titles ), 'woocommerce' ), wc_format_list_of_items( $titles ) );
}
