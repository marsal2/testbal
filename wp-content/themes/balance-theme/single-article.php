<?php
/*
Modules: {"m1[0]":{"name":"M1: Hero"},"m34[0]":{"name":"M34: Multi page article intro"},"multipage[0]":{"name":"Multipage article"}}
*/


?>
<?php
global $additional_body_class, $post, $data, $article_page, $total_article_pages;
Balance_Resources_Public::increment_resource_view_count( $post->ID, $article_page, 0 );
$additional_body_class = 'single-article';
get_custom_data();

if(!empty($data['multipage_module_module'][0]['pages'])){
  get_header();

  $total_article_pages = count($data['multipage_module_module'][0]['pages']);
  if($total_article_pages == 1) {
    get_template_part('partials/multipage-article-hero');
    get_template_part('partials/simple-article');
    echo render_m23a_may_also_like_resources();
  }else{
    if ( !empty( $data['multipage_module_module'][0]['last_page_quiz'] ) ){
      $total_article_pages += 1;
    }

    $article_page = intval(get_query_var( 'paged', 1 ) );

    if( $article_page < 1 ) {
      $article_page = 1;
    }

    if ( $article_page > $total_article_pages ) {
      require('404.php');
      exit();
    }
    if ( $article_page == 1 ) {
    	get_template_part('partials/multipage-article-hero');
    }
    get_template_part('partials/multipage-article-chapters');
    get_template_part('partials/multipage-article-content');
    get_template_part('partials/multipage-article-pager');

  }
  get_footer();
}else{
  require('404.php');
}
