<?php

function render_m56_m17_three_four_messages( $module_data, $partners_website = false ) {
	$output = '';

	$column_class = $module_data['radiochoice'] == 'three' ? "col-sm-6 col-md-4" : "col-sm-6 col-md-3";

	if ( !empty( $module_data['title'] ) || ( !empty( $module_data['messages'][0]['uploadimage']['image']['filename'] ) ) ) {

		$output .= '<section aria-label="'.(!empty($module_data['title']) ? $module_data['title'] : "" ).'" class="inner-block same-height-holder" '. ( !empty( $module_data['bgimage']['image']['fullpath'] ) ? "style='background-image:url(".$module_data['bgimage']['image']['fullpath'].");'" : '' ) .'>';
		$output .=     '<div class="container">';
		$output .=         '<div class="row">';
		$output .=             !empty( $module_data['title'] ) ? '<h1 class="text-center text-info">'.$module_data['title'].'</h1>' : '';
		$output .=             '<div class="col-holder">';
		foreach ( $module_data['messages'] as $message ) {
			if ( !empty( $message['title'] ) || !empty( $message['description'] ) && !empty( $message['uploadimage']['image']['fullpath'] ) ) {
				$output .= '<div class="'. $column_class .'">';
				$output .=   '<article class="finance-wrap '. ( ( $module_data['radiochoice'] != 'three' && !$partners_website ) ? 'background-white' : '' ) .'" aria-label="module for article '.str_replace(array("'",'"'),'', $message['title']).'">';
				$output .=     '<span class="icon">';
				$output .=       !empty( $message['uploadimage']['image']['fullpath'] ) ? '<img alt=" " src="'. $message['uploadimage']['image']['fullpath'] .'">' : '';
				$output .=     '</span>';
				if ( !empty( $message['title'] ) || !empty( $message['description'] ) ) {
					$output .=     '<div class="text-holder">';
					$output .=       !empty( $message['title'] ) ? '<h2>'.$message['title'].'</h2>' : '';
					$output .=       !empty( $message['description'] ) ? '<p class="height-finance-wrap">' . $message['description'] . '</p>' : '';
					$output .=       sprintf( build_link_aria_label( $message['link'], '', !empty( $message['title'] ) ? $message['title'] : '', true ), "text-read-more text-warning", "READ MORE" );
					$output .=     '</div>';
				}
				$output .=   '</article>';
				$output .= '</div>';
			}
		}
		$output .=             '</div>';
		$output .=         '</div>';
		if ( !empty( $module_data['buttontext'] && is_linkable( $module_data['linkfield'] ) ) ) {
			$output .=  '<div class="row">';
			$output .=    '<div class="btn-holder">';
			$output .=      sprintf( build_link( $message['link'], '' ), "btn btn-warning", $module_data['buttontext'] );;
			$output .=    '</div>';
			$output .=  '</div>';
		}
		$output .=     '</div>';
		$output .= '</section>';

	}

	return stripslashes( $output );

}
