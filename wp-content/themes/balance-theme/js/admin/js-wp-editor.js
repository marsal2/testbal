;
(function($, window) {
	$.fn.wp_editor = function(options) {

		if (!jQuery(this).is('textarea'))
			console.warn('Element must be a textarea');

		if (typeof tinyMCEPreInit == 'undefined' || typeof QTags == 'undefined' || typeof ap_vars == 'undefined')
			console.warn('js_wp_editor( $settings ); must be loaded');

		if (!jQuery(this).is('textarea') || typeof tinyMCEPreInit == 'undefined' || typeof QTags == 'undefined' || typeof ap_vars == 'undefined' || jQuery(this).hasClass('not-wp-editor'))
			return this;

		var default_options = {
				'mode': 'tmce',
				'mceInit': {
					"theme": "modern",
					"skin": "lightgray",
					"language": "en",
					"formats": {
						"alignleft": [{
							"selector": "p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li",
							"styles": {
								"textAlign": "left"
							},
							"deep": false,
							"remove": "none"
						}, {
							"selector": "img,table,dl.wp-caption",
							"classes": ["alignleft"],
							"deep": false,
							"remove": "none"
						}],
						"aligncenter": [{
							"selector": "p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li",
							"styles": {
								"textAlign": "center"
							},
							"deep": false,
							"remove": "none"
						}, {
							"selector": "img,table,dl.wp-caption",
							"classes": ["aligncenter"],
							"deep": false,
							"remove": "none"
						}],
						"alignright": [{
							"selector": "p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li",
							"styles": {
								"textAlign": "right"
							},
							"deep": false,
							"remove": "none"
						}, {
							"selector": "img,table,dl.wp-caption",
							"classes": ["alignright"],
							"deep": false,
							"remove": "none"
						}],
						"strikethrough": {
							"inline": "del",
							"deep": true,
							"split": true
						}
					},
					"relative_urls": false,
					"remove_script_host": false,
					"convert_urls": false,
					"browser_spellcheck": true,
					"fix_list_elements": true,
					"entities": "38,amp,60,lt,62,gt",
					"entity_encoding": "raw",
					"keep_styles": false,
					"paste_webkit_styles": "font-weight font-style color",
					"preview_styles": "font-family font-size font-weight font-style text-decoration text-transform",
					"wpeditimage_disable_captions": false,
					"wpeditimage_html5_captions": false,
					"plugins": "wordpress,wpeditimage,wpgallery,wplink,wpdialogs,wpview,image",
					"content_css": ap_vars.includes_url + "css/dashicons.css?ver=3.9," + ap_vars.includes_url + "js/mediaelement/mediaelementplayer.min.css?ver=3.9," + ap_vars.includes_url + "js/mediaelement/wp-mediaelement.css?ver=3.9," + ap_vars.includes_url + "js/tinymce/skins/wordpress/wp-content.css?ver=3.9," + ap_vars.stylesheet_directory_uri + '/css/admin/custom-editor-styles.css',
					"selector": "#apid",
					"resize": "vertical",
					"menubar": false,
					"wpautop": true,
					"indent": false,
					"toolbar1": "formatselect bold italic underline bullist numlist blockquote undo redo removeformat subscript superscript link unlink custombutton",
					"tabfocus_elements": ":prev,:next",
					"body_class": "apid",
					setup: function(editor) {
						editor.addButton('custombutton', {
							text: 'Button',
							classes: 'ico i-custom-button',
							icon: false,
							onclick: function(event) {
								popupButton(editor);
							}
						});
					},
				}
			},
			id_regexp = new RegExp('apid', 'g');

		if (tinyMCEPreInit.mceInit['apid']) {
			default_options.mceInit['apid'] = tinyMCEPreInit.mceInit['apid'];
		}

		if (options && (options.mode != default_options.mode)) {
			default_options.mode = options.mode;
		}


		var options = $.extend(true, default_options, options);

		// return this.each(function() {
		if (!jQuery(this).is('textarea')) {
			console.warn('Element must be a textarea');
		} else {
			var current_id = jQuery(this).attr('id');

			jQuery(this).attr('id', current_id);
			jQuery(this).attr('style', ''); //remove cloned styles

			$.each(options.mceInit, function(key, value) {
				if ($.type(value) == 'string') {
					options.mceInit[key] = value.replace(id_regexp, current_id);
				}
			});
			options.mode = options.mode == 'tmce' ? 'tmce' : 'html';

			delete tinyMCEPreInit.mceInit[current_id];
			tinyMCEPreInit.mceInit[current_id] = options.mceInit;

			var self = this;
			if (jQuery(this).closest('.wp-editor-wrap').length) {
				var parent_el = jQuery(this).closest('.wp-editor-wrap').parent();
				jQuery(this).closest('.wp-editor-wrap').before(jQuery(this).clone());
				jQuery(this).closest('.wp-editor-wrap').remove();
				self = parent_el.find('textarea[id="' + current_id + '"]');
			}

			var wrap = jQuery('<div id="wp-' + current_id + '-wrap" class="wp-core-ui wp-editor-wrap ' + options.mode + '-active" />'),
				editor_tools = jQuery('<div id="wp-' + current_id + '-editor-tools" class="wp-editor-tools hide-if-no-js" />'),
				editor_toolbar = jQuery('<div id="qt_wp' + current_id + 'editor_toolbar" class="quicktags-toolbar" />'),
				editor_tabs = jQuery('<div class="wp-editor-tabs" />'),
				switch_editor_html = jQuery('<a id="' + current_id + '-html" class="wp-switch-editor switch-html" data-wp-editor-id="' + current_id + '">Text</a>'),
				switch_editor_tmce = jQuery('<a id="' + current_id + '-tmce" class="wp-switch-editor switch-tmce" data-wp-editor-id="' + current_id + '">Visual</a>'),
				media_buttons = jQuery('<div id="wp-' + current_id + '-media-buttons" class="wp-media-buttons" />'),
				insert_media_button = jQuery('<a href="#" id="insert-media-button" class="button insert-media add_media" data-editor="' + current_id + '" title="Add Media"><span class="wp-media-buttons-icon"></span> Add Media</a>'),
				editor_container = jQuery('<div id="wp-' + current_id + '-editor-container" class="wp-editor-container" />'),
				content_css = /*Object.prototype.hasOwnProperty.call(tinyMCEPreInit.mceInit[current_id], 'content_css') ? tinyMCEPreInit.mceInit[current_id]['content_css'].split(',') :*/ false;
			if (options.media_buttons) {
				insert_media_button.appendTo(media_buttons);
				media_buttons.appendTo(editor_tools);
			}
			if (options.quicktags) {
				switch_editor_html.appendTo(editor_tabs);
				switch_editor_tmce.appendTo(editor_tabs);
			}

			editor_tabs.appendTo(editor_tools);

			editor_tools.appendTo(wrap);
			editor_container.appendTo(wrap);

			editor_container.append(jQuery(self).clone().addClass('wp-editor-area').show());

			jQuery(self).before(wrap);
			jQuery(self).remove();


			new QTags(current_id);
			QTags._buttonsInit();
			switchEditors.go(current_id, options.mode);

			jQuery(wrap).on('click', '.insert-media', function(event) {
				var elem = jQuery(event.currentTarget),
					editor = elem.data('editor'),
					options = {
						frame: 'post',
						state: 'insert',
						title: wp.media.view.l10n.addMedia,
						multiple: true
					};

				event.preventDefault();

				elem.blur();

				if (elem.hasClass('gallery')) {
					options.state = 'gallery';
					options.title = wp.media.view.l10n.createGalleryTitle;
				}

				wp.media.editor.open(editor, options);
			});

		}
		//});
	}

	/*Popup for adding a button*/
	function popupButton(editor) {
		jQuery('#wp-link').addClass('custom-button-dialog');
		jQuery('#wp-link-backdrop').addClass('custom-button-dialog');
		editor.execCommand('WP_Link');
		jQuery('.custom-button-dialog #link-modal-title').html('Insert/Edit Button');
		jQuery('.custom-button-dialog #wp-link-submit').val('Add Button');
	}

})(jQuery, window)
