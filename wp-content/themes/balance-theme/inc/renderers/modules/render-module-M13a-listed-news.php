<?php

function render_m13_news()
{
  $output = '';

  global $post;

  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

  $newsperpage = 100;

  $news_args = array(
    'numberposts' => $newsperpage,
    'post_type' => 'news',
    'post_status' => 'publish',
    'paged' => $paged,
  );


  $news = get_posts( $news_args );

  $totalnews = count( get_posts( array( 'numberposts' => -1,'post_type' => 'news', 'post_status' => 'publish' ) ) );

  $pagescount = (int)($totalnews / $newsperpage) + 1;

  if( count($news) > 0 ){

    $output .=  '<section aria-label="news section" class="list-block background-white">';
    $output .=    '<div class="container">';
    $output .=      '<div class="row">';
    $output .=        '<ul class="col-xs-12">';

    foreach ($news as $single_news) {

      $title = $single_news->post_title;
      $link = get_relative_permalink($single_news->ID);

      if( !empty($title) ){

        $output .= '<li><span class="text">'. $title .'</span><a aria-label="READ MORE from '. $single_news->ID .'" href="'. $link .'" class="text-read-more text-warning">READ MORE</a></li>';

      }

    }

    $output .=        '</ul>';
    $output .=      '</div>';
    $output .=    '</div>';
    $output .=  '</section>';

    /*Pagination*/

    /*Get next and previous*/
    $previous = $paged <= 1 ? -1 : $paged - 1;
    $next = $paged > $pagescount - 1 ? -1 : $paged + 1;

    $output .=        '<nav aria-label="paging holder m13a" class="paging-holder">';
    $output .=          '<div class="container">';
    $output .=            '<div class="row">';
    $output .=              '<ul class="pagination">';

    //previous button
    $output .=                '<li><a href="'.esc_url( get_relative_permalink( $post->ID ) . "page/" .$previous ).'" aria-label="Previous" '. ( $previous == -1 ? 'style="pointer-events: none;"' : '' ) .'><span class="btn-prev"></span><span class="hidden-xs">Prev</span></a></li>';

    if( $pagescount >= 10 ){ //if more than 10 pages

      //get 3 before current and 3 after current page
      $small_limit = $paged > 4 ? $paged - 2 : 1;
      $big_limit = $paged < $pagescount - 3 ? $paged + 3 : $pagescount + 1;

      if( $paged > 4 ){ //if not first 3 pages
        $output .=                '<li><a href="'. esc_url( get_relative_permalink( $post->ID ) ) .'">1</a></li>';
        $output .=                '<li><a href="#" style="pointer-events: none;"></a></li>'; // empty number after the first page pagination
      }

      for ( $i = $small_limit; $i < $big_limit; $i++) { 
        
        $output .=                '<li'. ( $paged == $i ? ' class="active" ' : '' ) .'><a href="'.esc_url( get_relative_permalink( $post->ID ) . "page/" . $i ).'">'. $i .'</a></li>';
      
      }
      //if not last 3 pages
      if( $paged < $pagescount - 3 ){
        $output .=                '<li><a href="#" style="pointer-events: none;"></a></li>'; //empty number before last page pagination
        $output .=                '<li><a href="'.esc_url( get_relative_permalink( $post->ID ) . "page/" . $pagescount ).'">'. $pagescount .'</a></li>';
      }

    } else { //normal pagination

      for ($i=1; $i < $pagescount + 1; $i++) { 
        $output .=                '<li'. ( $paged == $i ? ' class="active" ' : '' ) .'><a href="'.esc_url( get_relative_permalink( $post->ID ) . "page/" . $i ).'">'. $i .'</a></li>';
      }

    }

    //next button
    $output .=                 '<li><a href="'.esc_url( get_relative_permalink( $post->ID ) . "page/" .$next ).'" aria-label="Next" '. ( $next == -1 ? 'style="pointer-events: none;"' : '' ) .'><span class="hidden-xs">Next</span><span class="btn-next"></span></a></li>';

    $output .=               '</ul>';
    $output .=             '<p>of '. $pagescount .' pages</p>';
    $output .=           '</div>';
    $output .=         '</div>';
    $output .=       '</nav>';

  }

  return stripslashes($output);

}
