<?php
/*
 *  WP Edit module: M52
 *  Description: Module with login.  M52 - Hero w/Login
 */

function m52_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array( 'media_buttons' => false, 'quicktags' => true ) ) {
	global $data;

	if ( empty( $data['m52_module'][ $key ] ) ) {
		$data = init_array_on_var_and_array_key($data, 'm52_module');
		$data['m52_module'][ $key ] = array(
			'title' => '',
			'copy' => '',
			'image' => array(
				'attachment_id' => '',
				'filename' => '',
				'fullpath' => '',
			),
		);
	}

	if ( empty( $data['m52_module'][ $key ]['login'] ) ) {
		$data['m52_module'][ $key ]['login'] = '';
	}

	$showlogin = array(
		'loginshow' => __( 'Show', 'balance' ),
	);

	$output = '';
	$output .= '<a name="m52-module-wrapper-'. $key .'"></a>';
	$output .= '<div class="module-wrapper m52-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
	$output .= '  <div class="postbox postbox-custom m52-module-list-wrapper-'. $key .'">';
	$output .= '    <h3>'. $module_title . ( intval( $key ) > 0 ? ' #'.( intval( $key )+1 ) : '' ) .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
	$output .= '    <div class="inside hidden">';

	$output .= '      <p>';
	$output .= '        <label><b>'. __( 'Show login', 'balance' ) .':</b></label><br>';
	$output .=          checkboxlist_field( $data['m52_module'][ $key ]['login'], 'm52_module['.$key.'][login]', $showlogin );
	$output .= '      </p>';

	$output .= '        <p>';
	$output .= '           <label><b>'. __( 'Background image', 'balance' ) .':</b></label><br/>';
	$output .=             upload_field( $data['m52_module'][ $key ]['image'], 'm52_module['.$key.'][image]' );
	$output .= '        </p>';

	$output .= '        <p>';
	$output .= '          <label>'. __( 'Title', 'balance' ) .':</label>';
	$output .=              text_field( $data['m52_module'][ $key ]['title'], 'm52_module['.$key.'][title]' );
	$output .= '        </p>';

	$output .= '            <p>';
	$output .= '              <label><b>'. __( 'Copy', 'balance' ) .':</b></label><br/><br>';
	$output .=                textarea_field( $data['m52_module'][ $key ]['copy'], 'm52_module['.$key.'][copy]', true, 0, '', '', $custom_settings );
	$output .= '            </p>';

	$output .= '    </div>';
	$output .= '  </div>';
	$output .= '</div>';
	return $output;

}

?>
