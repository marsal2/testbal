<?php
/*
Template Name: T12: Company history
Modules: {"m1[0]":{"name":"M1: Hero"},"m34[0]":{"name":"M34: Section"}}
*/

?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'company-history';
get_custom_data();

get_header();

/* M01/M57 renderer */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0], 'medium' );
}

/* SECTION - TITLE, COPY */
if (!empty($data['m34_module']) && !empty($data['m34_module'][0])) {
    echo render_m34_section_title_copy($data['m34_module'][0]);
}

get_footer();
?>
