# WebhookAuthSetting

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_name** | **string** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 128 AN&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;The HTTP Basic authentication user name. Applicable only if auth_type is BASIC. | [optional] 
**user_password** | **string** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 128 AN&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;The HTTP Basic authentication password. Applicable only if auth_type is BASIC. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


