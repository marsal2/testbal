<?php
/*
Template Name: T14: Donors
Modules: {"m1[0]":{"name":"M1: Hero"},"m56_m17[0]":{"name":"M56: Impressive Stats"},"m19[0]":{"name":"M19: Vertical logos"},"m20[0]":{"name":"M20: Client Stories"},"m56_m17[1]":{"name":"M56: Why Partner or Donate"}}
*/

?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'donors';
get_custom_data();

get_header();

/* M01/M57 renderer */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0] );
}

/* FOUR/THREE MESSAGES */
if (!empty($data['m56_module']) && !empty($data['m56_module'][0])) {
    echo render_m56_m17_three_four_messages($data['m56_module'][0]);
}

/* VERTICAL LOGOS */
if (!empty($data['m19_module']) && !empty($data['m19_module'][0])) {
    echo render_m19_vertical_logos($data['m19_module'][0]);
}

/* M20 renderer */
if ( !empty( $data['m20_module'] ) && !empty( $data['m20_module'][0] ) ) {
	echo render_m20_split_carousel( $data['m20_module'][0] );
}

/* FOUR/THREE MESSAGES */
if (!empty($data['m56_module']) && !empty($data['m56_module'][1])) {
    echo render_m56_m17_three_four_messages($data['m56_module'][1]);
}

get_footer();
?>
