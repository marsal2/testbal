<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_Access_Groups
 * @subpackage Balance_Access_Groups/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Balance_Access_Groups
 * @subpackage Balance_Access_Groups/admin
 * @author     ROIDNA <devs@roidna.com>
 */


class Balance_Access_Groups_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param string  $plugin_name The name of this plugin.
	 * @param string  $version     The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Balance_Access_Groups_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Balance_Access_Groups_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/balance-access-groups-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( "jquery.tokeninput.css", plugin_dir_url( __FILE__ ) . 'css/token-input.custom.css' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		add_action( "admin_footer", array( $this, "access_groups_ajax_api_javascript" ) );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/balance-access-groups-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( "jquery.tokeninput", plugin_dir_url( __FILE__ ) . 'js/jquery.tokeninput.custom.js', array( 'jquery' ) );
	}

	public function access_groups_ajax_api() {
		global $wpdb; // this is how you get access to the database


		if ( isset( $_GET['method'] ) ) {
			$method = sanitize_text_field( $_GET['method'] );
			$query = sanitize_text_field( $_GET['data'] );


			switch ( $method ) {
			case "content_type":
				$data = Balance_Access_Groups_DB::getContentTypes( $query );
				break;
			case "resource":
				$data = Balance_Access_Groups_DB::getResources( $query );
				break;
			case "tag":
				$data = Balance_Access_Groups_DB::getTags( $query );
				break;
			}

			echo json_encode( $data );
		}


		wp_die(); // this is required to terminate immediately and return a proper response

	}


	//Renders the JS for jQuery Token Input

	public function access_groups_ajax_api_javascript() {
		$screen = get_current_screen();

		$arrSupportedScreens = array( "add", "edit" );

		// Helper function Convert PHP object to JSON
		function h_phpToJSON( $arr, $obj_name ) {
			$str = "";
			if ( !empty( $arr ) ) {
				$obj = array();
				$obj["prePopulate"] = $arr;

				$str = $obj_name.' = $.extend({}, '.$obj_name.', '.json_encode( $obj ).');'."\n";
			}
			return $str;
		}

		//Helper: Converting assoc array into an array of key/val objects
		function h_tokenizeArr( $ids, $names ) {
			$retArr = array();
			for ( $i=0; $i<count( $names ); $i++ ) {
				$id = $ids[$i];
				$name = $names[$i];

				//Since title is not reqd, use ID as an identifier
				if ( empty( $name ) ) {
					$name = "(No Title) ID: ".$id;
				}
				$retArr[] = array( "id" => $id,
					"name" => $name
				);
			}
			return $retArr;
		}

		//Ensure it is displayed only on Edit and Add screens
		if ( $screen->id === "access_group" && ( ($screen->base==="post" && $screen->post_type=="access_group") || ( $screen->action == "add" ) ) ) {
			//If edit screen, prefill content types
			if ( $screen->base==="post" && $screen->action != "add" ) {
				$postID = $_REQUEST["post"];
				$row = Balance_Access_Groups_DB::get( null, $postID );

				//Row not found, mismatch between tables?
				if ( !$row ) {
					return;
				}

				$content_types = array();
				$term_in = array();
				$term_not_in = array();
				$post_in = array();
				$post_not_in = array();

				//Add label to existing data
				if ( !empty( $row["post_type"] ) ) {
					$content_type_keys = explode( ",", $row["post_type"] );
					$content_type_values = explode( ",", Balance_Access_Groups_DB::h_content_types( $content_type_keys ) );

					$content_types = h_tokenizeArr( $content_type_keys, $content_type_values );

					if ( !empty( $content_type_keys ) && !empty( $content_type_values ) ) {
						$content_types = h_tokenizeArr ( $content_type_keys, $content_type_values );
					}

				}

				if ( !empty( $row["term__in"] ) ) {
					$term_in_keys = explode( ",", $row["term__in"] );
					$term_in_values =  explode( ",", Balance_Access_Groups_DB::h_tags( $term_in_keys ) );
					$term_in = h_tokenizeArr( $term_in_keys, $term_in_values );

					if ( !empty( $term_in_keys ) && !empty( $term_in_values ) ) {
						$term_in = h_tokenizeArr( $term_in_keys, $term_in_values );
					}

				}

				if ( !empty( $row["term__not_in"] ) ) {
					$term_not_in_keys = explode( ",", $row["term__not_in"] );
					$term_not_in_values = explode( ",", Balance_Access_Groups_DB::h_tags( $term_not_in_keys ) );
					$term_not_in = array_combine( $term_not_in_keys, $term_not_in_values );

					if ( !empty( $term_not_in_keys ) && !empty( $term_not_in_values ) ) {
						$term_not_in = h_tokenizeArr( $term_not_in_keys, $term_not_in_values );
					}

				}

				if ( !empty( $row["post__in"] ) ) {
					$post_in_keys = explode( ",", $row["post__in"] );
					$post_in_values = explode( ",", Balance_Access_Groups_DB::h_resources( $post_in_keys ) );

					$post_in = h_tokenizeArr( $post_in_keys, $post_in_values );

					if ( !empty( $post_in_keys ) && !empty( $post_in_values ) ) {
						$post_in = h_tokenizeArr( $post_in_keys, $post_in_values );
					}

				}

				if ( !empty( $row["post__not_in"] ) ) {
					$post_not_in_keys = explode( ",", $row["post__not_in"] );
					$post_not_in_values = explode( ",", Balance_Access_Groups_DB::h_resources( $post_not_in_keys ) );

					if ( !empty( $post_not_in_keys ) && !empty( $post_not_in_values ) ) {
						$post_not_in = h_tokenizeArr( $post_not_in_keys, $post_not_in_values );
					}
				}
			}

?>
			<script type="text/javascript" >
			jQuery(document).ready(function($) {

				var tokenFormatter = function(item) { return '<li class="button-secondary"><div>'+item.name+(item.count ? ' ('+item.count+')' : '')+(item.excerpt ? ' <p>'+item.excerpt+'</p>' : '')+'</div></li>' };
				var resultsFormatter = function(item) { return  '<li class="button-secondary"><div>'+item.name+(item.count ? ' ('+item.count+')' : '')+(item.excerpt ? ' <p>'+item.excerpt+'</p>' : '')+'</div></li>'};

				var config = {
					"method": "GET",
					"queryParam": "data",
					preventDuplicates: true,
					resultsFormatter: resultsFormatter,
              		      tokenFormatter: tokenFormatter,
                                animateDropdown: false,
                                hintText: "",
                                noResultsText: "No results",
                                searchingText: ""
				};


				var config_content_type = config,
					config_tag_include = config,
					config_tag_exclude = config,
					config_resource_include = config,
					config_resource_exclude = config;

				<?php


			//If there are existing values, override config with prepopulate values

			if ( isset( $content_types ) ) {
				echo h_phpToJSON( $content_types, "config_content_type" );
			}

			if ( isset( $term_in ) ) {
				echo h_phpToJSON( $term_in, "config_tag_include" );
			}

			if ( isset( $term_not_in ) ) {
				echo h_phpToJSON( $term_not_in, "config_tag_exclude" );
			}

			if ( isset( $post_in ) ) {
				echo h_phpToJSON( $post_in, "config_resource_include" );
			}

			if ( isset( $post_not_in ) ) {
				echo h_phpToJSON( $post_not_in, "config_resource_exclude" );
			}
?>


				$("[name=content_type]").tokenInput(ajaxurl+"?method=content_type&action=access_groups&", config_content_type);
				$("[name=tag_include]").tokenInput(ajaxurl+"?method=tag&action=access_groups&", config_tag_include);
				$("[name=tag_exclude]").tokenInput(ajaxurl+"?method=tag&action=access_groups&", config_tag_exclude);

				$("[name=resource_include]").tokenInput(ajaxurl+"?method=resource&action=access_groups&", config_resource_include);
				$("[name=resource_exclude]").tokenInput(ajaxurl+"?method=resource&action=access_groups&", config_resource_exclude);


			});
			</script>
		<?php
		}
	}


	public function access_groups_menu() {

	    $page_title = 'Resources Groups';
	    $menu_title = 'Resources Groups';
	    $capability = 'activate_plugins';
	    $menu_slug  = 'access_groups_list';
	    $function   = array( $this, 'access_groups_admin_page' );// Callback function which displays the page content.
	    $icon_url   = 'dashicons-groups';
	    $position   = 22;

	    add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position);

		$access_groups_submenu_pages = array();
		$access_groups_submenu_pages[] = array(
			'parent_slug'   => 'access_groups_list',
			'page_title'    => 'Resources Groups',
			'menu_title'    => 'Resources Groups',
			'menu_slug'    => 'access_groups_list',
			'capability'    => 'activate_plugins', //Ensure only admins see this
			'function'      => array( $this, 'access_groups_admin_page' )
		);

		$access_groups_submenu_pages[] = array(
		      'parent_slug'   => 'access_groups_list',
		      'page_title'    => 'Add New',
		      'menu_title'    => 'Add New',
		      'menu_slug'    => 'post-new.php?post_type=access_group',
		      'capability'    => 'activate_plugins', //Ensure only admins see this
		      'function'      => ""
		    );

		foreach ( $access_groups_submenu_pages as $submenu ) {
			add_submenu_page(
				$submenu['parent_slug'],
				$submenu['page_title'],
				$submenu['menu_title'],
				$submenu['capability'],
				$submenu['menu_slug'],
				$submenu['function']
			);
		}
		// remove_submenu_page('edit.php?post_type=access_group','access_groups_list');
		add_filter( 'parent_file', array( $this, 'access_groups_set_current_menu' ) );
	}

	public function access_groups_admin_page() {
		include plugin_dir_path( __FILE__ ) . 'partials/balance-access-groups-admin-display.php';
	}

	/**
	 * Sets the curent menu to the overview one
	 *
	 * @since   1.0.0
	 * @access  public
	 */
	function access_groups_set_current_menu( $parent_file ) {
		global $current_screen, $submenu_file, $pagenow;
		if ( $current_screen->id == 'toplevel_page_access_groups_list' ) {
			$parent_file = 'access_groups_list';
			$submenu_file = 'access_groups_list';
		} else if ( $current_screen->id == 'access_group' && $current_screen->base == 'post' && $pagenow == 'post-new.php') {
			$parent_file = 'access_groups_list';
			$submenu_file = 'post-new.php?post_type=access_group';
		} else if ( $current_screen->id == 'access_group' && $current_screen->base == 'post' && $pagenow == 'post.php') {
			$parent_file = 'access_groups_list';
			$submenu_file = 'access_groups_list';
		}
		return $parent_file;

	}

	public function register_custom_post_type() {

		$args = array(
			"labels" => array(
				"name" => __( "Resources Groups" ),
				"singular_name" => __( "Resources Group" ),
				"add_new" => __( "Add New" ),
				"add_new_item" => __( "Add New" ),
				"edit_item" => __( "Edit Resources Group" ),
				"new_item" => __( "Add New Resources Group" ),
				"view_item" => __( "View Resources Group" ),
				"search_items" => __( "Search Resources Group" ),
				"not_found" => __( "No Resources Groups found" )
			),
			"show_ui" => true,
			"show_in_nav" => false,
			"exclude_from_search" => true,
			"show_in_menu" => false,
			"public" => false,
			"register_meta_box_cb" => array( $this, 'add_access_group_metabox' ),
			"rewrite" => array( "slug" => "access_group" ),
			"supports" => array(
				"title"
			),
			'menu_icon' => 'dashicons-groups'
		);

		register_post_type( "access_group", $args );
	}

	/*
	 * Metabox callbacks
	 */
	public function add_access_group_metabox() {
		Balance_Access_Groups_DB::getTags();

		add_meta_box( 'access_group_content_type', 'Filter by including specific Resource Type', array( $this, 'add_access_group_metabox_content_type' ), 'access_group', 'normal', 'default' );
		add_meta_box( 'access_group_tags', 'Filter by including or excluding specific Tags', array( $this, 'add_access_group_metabox_tags' ), 'access_group', 'normal', 'default' );
		add_meta_box( 'access_group_resources', 'Filter by including or excluding specific Resources', array( $this, 'add_access_group_metabox_resources' ), 'access_group', 'normal', 'default' );
	}

	public function add_access_group_metabox_content_type() {
		wp_nonce_field( 'add_access_group_metabox_save_cb', 'add_access_group_nonce' );
		include plugin_dir_path( __FILE__ ) . 'partials/balance-access-groups-admin-add-metabox-content-types.php';
	}

	public function add_access_group_metabox_tags() {
		include plugin_dir_path( __FILE__ ) . 'partials/balance-access-groups-admin-add-metabox-tags.php';
	}

	public function add_access_group_metabox_resources() {
		include plugin_dir_path( __FILE__ ) . 'partials/balance-access-groups-admin-add-metabox-resources.php';
	}

	/*
	 * Post action callbacks
	 */

	//No need to check for post type here, specified in hook
	public function access_group_trash_cb( $post_id,  $post=null, $update=false ) {
		Balance_Access_Groups_DB::updateStatus( $post_id, "trash" );
	}

	public function access_group_restore_cb( $post_id,  $post=null, $update=false ) {
		if ( get_post_type( $post_id ) === "access_group" ) {
			Balance_Access_Groups_DB::updateStatus( $post_id, "publish" );
		}
	}

	public function access_group_delete_cb( $post_id,  $post=null, $update=false ) {
		if ( get_post_type( $post_id ) === "access_group" ) {
			Balance_Access_Groups_DB::delete( $post_id );
		}
	}

	/**
	 * Called on save_post to insert or update the data in the access_groups table
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param int     $post_id The post ID
	 * @param object  $object  The post object
	 * @return  void
	 */
	public function access_group_metabox_save_cb( $post_id, $post, $update = false ) {
		global $access_groups_cpts;
		global $wpdb;

		if ( $post && ( $post->post_type == 'access_group' ) ) {
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
				return $post_id;
			}

			// Ensure admin
			if ( !is_admin() ) {
				return $post_id;
			}

			// Only save to access_groups table when post is published
			if ( $post->post_status !== 'publish' ) {
				return $post_id;
			}

			$prefix = $wpdb->prefix;
			$access_groups_table_name = $prefix . 'access_groups';
			$tagInclude = array();
			$tagExclude = array();
			$resourceInclude = array();
			$resourceExclude = array();
			$contentType = array();

			// Necessary check for restore action.
			$exists = Balance_Access_Groups_DB::get( null, $post_id ) !== FALSE;
			$update = $update || $exists;

			//Clean up user input
			if ( isset( $_REQUEST["tag_include"] ) ) {
				$tagInclude = sanitize_text_field( $_REQUEST["tag_include"] );
			}

			if ( isset( $_REQUEST["tag_exclude"] ) ) {
				$tagExclude = sanitize_text_field( $_REQUEST["tag_exclude"] );
			}

			if ( isset( $_REQUEST["resource_include"] ) ) {
				$resourceInclude = sanitize_text_field( $_REQUEST["resource_include"] );
			}

			if ( isset( $_REQUEST["resource_exclude"] ) ) {
				$resourceExclude = sanitize_text_field( $_REQUEST["resource_exclude"] );
			}

			if ( isset( $_REQUEST["content_type"] ) ) {
				$contentType = sanitize_text_field( $_REQUEST["content_type"] );
			}

			if ( !$update ) {
				Balance_Access_Groups_DB::insert( $post->post_title, $post_id, $contentType, $tagInclude, $tagExclude, $resourceInclude, $resourceExclude );
			}
			else {
				Balance_Access_Groups_DB::update( $post->post_title, $post_id, $contentType, $tagInclude, $tagExclude, $resourceInclude, $resourceExclude );
			}

		}

	}

	//Disable WP auto-save by removing JS
	public function access_group_disable_autosave() {
		$screen = get_current_screen();
		if ( $screen->id === "access_group" && ( $screen->parent_base==="edit" || ( $screen->action == "add" ) ) ) {
			wp_dequeue_script( 'autosave' );
		}
	}


	//Override view post
	public function access_groups_post_updated_messages( $messages ) {
		$post = get_post();
		$post_type = get_post_type( $post );
		$post_type_object = get_post_type_object( $post_type );
		$messages['access_group'] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => __( 'Resources Group published.' ) ,
			2 => __( 'Custom field updated.' ) ,
			3 => __( 'Custom field deleted.' ) ,
			4 => __( 'Resources Group updated.' ) ,
			/* translators: %s: date and time of the revision */
			5 => isset( $_GET['revision'] ) ? sprintf( __( 'Resources Group restored to revision from %s' ) , wp_post_revision_title( (int)$_GET['revision'], false ) ) : false,
			6 => __( 'Resources Group published.' ) ,
			7 => __( 'Resources Group saved.' ) ,
			8 => __( 'Resources Group submitted.' ) ,
			9 => sprintf( __( 'Resources Group scheduled for: <strong>%1$s</strong>.' ) ,

				// translators: Publish box date format, see http://php.net/date

				date_i18n( __( 'M j, Y @ G:i' ) , strtotime( $post->post_date ) ) ) ,
			10 => __( 'Resources Group draft updated.' )
		);

		return $messages;
	}

	//DB installer
	public function access_groups_install() {
		Balance_Access_Groups_DB::install();
	}

	/**
	 * Called on delete_post to cleanup the data in the access_groups table
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param int     $post_id The post ID
	 * @return  void
	 */
	public function add_access_group_delete( $post_id ) {
		Balance_Access_Groups_DB::delete( $post_id );
	}

	/**
	 * Disable revisions for access_groups post types
	 *
	 * @since   1.0.0
	 * @access  public
	 * @uses  post_type_supports(), get_post(), get_post_type()
	 */
	public function access_groups_post_type_pre_update( $post_id ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;

		if ( in_array( get_post_type( $post_id ), "access_groups" ) ) { // if the type is one of the access_groups
			remove_action( 'pre_post_update', 'wp_save_post_revision', 10 );
			if ( !$post = get_post( $post_id, ARRAY_A ) )
				return;
			if ( !post_type_supports( $post['post_type'], 'revisions' ) )
				return;
			$return = _wp_put_post_revision( $post );
		}
	}



}
