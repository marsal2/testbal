<?php

function render_m68_chapter_associated_resources( $title, $resources = array(), $quiz_page = false) {
  if( !empty($resources) ){
    $list = '';
    foreach( $resources as $resource ) {
      $list.='
        <li>
          <a href="'.$resource['link'].'">
            <span class="icon-'.$resource['icon_type'].'"></span>'.$resource['link_text'].'
          </a>
        </li>';
    }
    if( $quiz_page ){
      $output = '<li>
          <a href="'.$resource['link'].'">
            <span class="icon-'.$resource['icon_type'].'"></span>'.__('Take the Quiz').'
          </a>
        </li>';
    }
    $output = '
    <div class="container">
      <div class="row">
        <div class="col-sm-4 col-sm-offset-8 aside-container">
          <aside id="sidebar">
            <div class="inner-aside hidden-xs">
              <div class="widget-wrap">
                <div class="widget">
                  '. ( !empty($title) ? '<h1>'.stripslashes($title).'</h1>' : '' ) .'
                  <ul class="side-list">
                    '.$list.'
                  </ul>
                </div>
              </div>
            </div>
          </aside>
        </div>
      </div>
    </div>';
    return $output;
  }
}