<?php
/**
 * CheckoutCustomerTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  qpPlatform
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Qualpay Platform API
 *
 * This document describes the Qualpay Platform API.
 *
 * OpenAPI spec version: 1.1.9
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.2
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace qpPlatform;

/**
 * CheckoutCustomerTest Class Doc Comment
 *
 * @category    Class
 * @description CheckoutCustomer
 * @package     qpPlatform
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CheckoutCustomerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "CheckoutCustomer"
     */
    public function testCheckoutCustomer()
    {
    }

    /**
     * Test attribute "customer_first_name"
     */
    public function testPropertyCustomerFirstName()
    {
    }

    /**
     * Test attribute "customer_last_name"
     */
    public function testPropertyCustomerLastName()
    {
    }

    /**
     * Test attribute "customer_email"
     */
    public function testPropertyCustomerEmail()
    {
    }

    /**
     * Test attribute "billing_addr1"
     */
    public function testPropertyBillingAddr1()
    {
    }

    /**
     * Test attribute "billing_city"
     */
    public function testPropertyBillingCity()
    {
    }

    /**
     * Test attribute "billing_state"
     */
    public function testPropertyBillingState()
    {
    }

    /**
     * Test attribute "billing_zip"
     */
    public function testPropertyBillingZip()
    {
    }
}
