<?php
/*
 *  WP Edit sample module: Sample module: "Sample module"
 *  Description:
 */


/**
 * Replace "sample" with module name eg. "cm1"
 */

function sample_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

  if ( empty($data['sample'][ $key ]) ) {
    $data = init_array_on_var_and_array_key($data, 'sample');
    $data['sample'][ $key ] = array(
        'multiautocompletefield' => '',
        'multiautocompletefieldattachments' => '',
        'textfield' => '',
        'datefield' => '',
        'repeatitems' => array(
          0 => array(
            'image' => array(
              'attachment_id' => '',
              'filename' => '',
              'fullpath' => ''
            ),
            'textareafield' => '',
            'uploadfield' => '',
            'linkfield' => array(
              'url_type' => '1',
              'url' => '',
              'url_page_id' => '',
              'url_onclick_js' => ''
            ),
          ),
        ),
        'page' => array(
          'id' => '',
          'title' => '',
          'permalink' => '',
        ),
        'radiochoice' => '',
	      'file' => '',
    );
  }

  if( empty($data['sample'][ $key ]['checkboxplain']) ){
    $data['sample'][ $key ]['checkboxplain'] = '';
  }

  if( empty($data['sample'][ $key ]['checkboxhtml']) ){
    $data['sample'][ $key ]['checkboxhtml'] = '';
  }

  if( empty($data['sample'][ $key ]['geolocatorfield']) ){
    $data['sample'][ $key ]['geolocatorfield'] = '';
  }

  if( empty($data['sample'][ $key ]['post_types_checkbox']) ){
    $data['sample'][ $key ]['post_types_checkbox'] = '';
  }

  if( empty($data['sample'][ $key ]['post_types_radio']) ){
    $data['sample'][ $key ]['post_types_radio'] = '';
  }

  if( empty($data['sample'][ $key ]['taxonomy_checkbox']) ){
    $data['sample'][ $key ]['taxonomy_checkbox'] = '';
  }

  if( empty($data['sample'][ $key ]['taxonomy_radio']) ){
    $data['sample'][ $key ]['taxonomy_radio'] = '';
  }

  /**
    * Array of allowed files for dropdown field
    * @see https://codex.wordpress.org/Function_Reference/get_allowed_mime_types to get types
    */
  $array_of_allowed_files = array(
    'application/pdf',
    'text/plain',
    'text/csv',
  );

  //options for radiobutton
  $radiobuttons_choices = array(
    'one' => __( 'Choice one', 'balance' ),
    'two'  => __( 'Choice two', 'balance' ),
  );

  //options for plain checkboxes
  $checkboxplain_choices = array(
    'one' => __( 'Choice one', 'balance' ),
    'two'  => __( 'Choice two', 'balance' ),
  );

  //options for html checkboxes
  $checkboxhtml_choices = array(
    array('one', '<span>Choice one</span>'),
    array('two', '<span>Choice two</span>'),
  );

  //args for autocomplete fields that define what to show in autocomplete - pages
  $autocomplete_args_pages = array(
    'post_type' => 'page',
    'posts_per_page' => -1,
    'post_status' => 'publish'
  );

  //args for autocomplete fields that define what to show in autocomplete - files
  //for all allowed attachemnt types: https://codex.wordpress.org/Function_Reference/get_allowed_mime_types
  $autocomplete_args_attachments = array(
    'post_type' => 'attachment',
    'post_mime_type' => array(
      'application/pdf',
      'text/csv',
      'text/plain',
    ),
    'posts_per_page' => -1
  );

  $output = '';
  $output .= '<a name="sample-form-wrapper-'. $key .'"></a>';
  $output .= '  <div class="module-wrapper sample-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '    <div class="postbox postbox-custom sample-wrapper-'. $key .'">';
  $output .= '      <h3>'. $module_title .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '      <div class="inside hidden">';

  //Autocomplete Field for pages
  $output .= '        <p>';
  $output .= '          <label><b>'. __( 'Multiple options autocomplete field for Pages', 'balance' ) .':</b></label>';
  $output .=              multi_autocomplete_field( $data['sample'][ $key ]['multiautocompletefield'], 'sample['.$key.'][multiautocompletefield]', $autocomplete_args_pages, 2);
  $output .= '        </p>';

  //Autocomplete Field for attachments
  $output .= '        <p>';
  $output .= '          <label><b>'. __( 'Multiple options autocomplete field for Attachments', 'balance' ) .':</b></label>';
  $output .=              multi_autocomplete_field( $data['sample'][ $key ]['multiautocompletefieldattachments'], 'sample['.$key.'][multiautocompletefieldattachments]', $autocomplete_args_attachments);
  $output .= '        </p>';

  //Text field
  $output .= '        <p>';
  $output .= '          <label><b>'. __( 'Text field', 'balance' ) .':</b></label>';
  $output .=              text_field( $data['sample'][ $key ]['textfield'], 'sample['.$key.'][textfield]');
  $output .= '        </p>';

  $output .= '        <label><b>'. __('Repeating item', 'balance') .':</b></label><br/>';
  $output .= '        <label><b><small>'. __('Change order by dragging items up & down.', 'balance' ) .'</small></b></label>';
  $output .= '        <br/>';

  //Repeated fields
  $output .= '        <div class="items-list">';
  foreach ( $data['sample'][ $key ]['repeatitems'] as $item_key => $item ) {

    if ( empty( $item['linkfield'] ) ) {
      $item['linkfield'] = array(
          'url_type' => '1',
          'url' => '',
          'url_page_id' => '',
          'url_onclick_js' => ''
      );
    }

    if ( empty( $item['linkfield']['url_onclick_js'] ) ) {
      $item['linkfield']['url_onclick_js'] = '';
    }

     if ( empty( $item['autocompletefield'] ) ) {
      $item['autocompletefield'] = '';
    }
    //Single item in repeater
    $output .= '          <div class="item">';
    $output .= '            <h4>';
    $output .=                __( 'COLUMN', 'balance' );
    $output .= '              #<span class="counter">'. ( $item_key + 1 ) .'</span>';
    $output .= '              <a class="remove-item description fright">'. __( 'Remove Item', 'balance' ) .'</a>&nbsp;';
    $output .= '              <a class="collapse-item description fright" data-toggle-title="'. __( 'Expand Item', 'balance' ) .'">'. __( 'Collapse Item', 'balance' ) .'</a>';
    $output .= '            </h4>';
    //Upload field
    $output .= '            <p>';
    $output .= '              <label><b>'. __( 'Upload field', 'balance' ) .':</b></label><br/>';
    $output .=                 upload_field( $item['uploadfield'], 'sample['.$key.'][repeatitems]['.$item_key.'][uploadfield]' );
    $output .= '            </p>';
    $output .= '            <br/>';
    //Textarea field
    $output .= '            <p>';
    $output .= '              <label><b>'. __( 'Textarea Field', 'balance' ) .':</b></label><br/>';
    $output .=                textarea_field( $item['textareafield'], 'sample['.$key.'][repeatitems]['.$item_key.'][textareafield]', true, 0, '', '', array('media_buttons' => true, 'quicktags' => true ) );
    $output .= '            </p>';

    //Link to page or custom URL
    $output .= '      <p>';
    $output .= '        <label><b>'. __( 'URL link/redirect to page or custom url', 'balance' ) .':</b></label>';
    $output .=          links_to_field( $item['linkfield'], 'sample['.$key.'][repeatitems]['.$item_key.'][linkfield]');
    $output .= '      </p>';

      //Autocomplete Field for attachments
      $output .= '        <p>';
      $output .= '          <label><b>'. __( 'Multiple options autocomplete field inside repeater', 'balance' ) .':</b></label>';
      $output .=              multi_autocomplete_field( $item['autocompletefield'], 'sample['.$key.'][repeatitems]['.$item_key.'][autocompletefield]');
      $output .= '        </p>';

    $output .= '          </div>';
  }
  $output .= '          <input type="button" class="button add-new-item" value="'. __( 'Add New', 'balance' ) .'" />';
  $output .= '      </div>';

  $output .= '      <br/><br/>';

  //Link to page or custom URL
  // $output .= '      <p>';
  // $output .= '        <label><b>'. __( 'URL link/redirect to page or custom url', 'balance' ) .':</b></label>';
  // $output .=          links_to_field( $data['sample'][ $key ]['linkfield'], 'sample['.$key.'][linkfield]');
  // $output .= '      </p>';

  //Dropdown of all allowed files
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Dropdown of attach files of chosen type', 'balance' ) .':</b></label><br>';
  if( count( get_attachments_array( $array_of_allowed_files ) ) > 0 ){
    $output .=          dropdown_field( $data['sample'][ $key ]['file'], 'sample['.$key.'][file]', 'File', get_attachments_array( $array_of_allowed_files ) );
  } else {
    $output .= '<span>There is no appropriate attachments.</span>';
  }
  $output .= '      </p>';

  //Dropdown of all pages
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Dropdown of pages', 'balance' ) .':</b></label><br>';
  $output .=          dropdown_field( $data['sample'][ $key ]['page']['id'], 'sample['.$key.'][page][id]', 'Page', get_array_of_pages( array( '-1' ), false ) );
  $output .= '      </p>';

  //Datepicker text input
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Datepicker field', 'balance' ) .':</b></label><br>';
  $output .=          datepicker_field( $data['sample'][ $key ]['datefield'], 'sample['.$key.'][datefield]');
  $output .= '      </p>';

  //Radiobuttons
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Radio buttons', 'balance' ) .':</b></label><br>';
  $output .=          radiobuttonlist_field( $data['sample'][ $key ]['radiochoice'], 'sample['.$key.'][radiochoice]', $radiobuttons_choices, 'two', true);
  $output .= '      </p>';

  //Checkbox plain text
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Checkbox plain text', 'balance' ) .':</b></label><br>';
  $output .=          checkboxlist_field( $data['sample'][ $key ]['checkboxplain'], 'sample['.$key.'][checkboxplain]', $checkboxplain_choices);
  $output .= '      </p>';

  //Checkbox html - $options_array has index [0] for value and [1] for text
  /**
   * @todo create sample with code snippets selector as in Demandbase
   */
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Checkbox html', 'balance' ) .':</b></label><br>';
  $output .=          checkboxlist_arrayed_field( $data['sample'][ $key ]['checkboxhtml'], 'sample['.$key.'][checkboxhtml]', $checkboxhtml_choices);
  $output .= '      </p>';

  //Post type selection with checkbox
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Post type selection', 'balance' ) .':</b></label><br>';
  $output .=          post_types_field( $data['sample'][ $key ]['post_types_checkbox'], 'sample['.$key.'][post_types_checkbox]', null, null, false, array( 'np-redirect' ) );
  $output .= '      </p>';

  //Post type selection with radio buttons
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Post type selection radio', 'balance' ) .':</b></label><br>';
  $output .=          post_types_field( $data['sample'][ $key ]['post_types_radio'], 'sample['.$key.'][post_types_radio]', null, null , false, array( 'np-redirect' ), 'radiolist' );
  $output .= '      </p>';

  //List of taxonomies in checkboxes
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'List of taxonomies checkbox', 'balance' ) .':</b></label><br>';
  $output .=          taxonomies_terms_field( $data['sample'][ $key ]['taxonomy_checkbox'], 'sample['.$key.'][taxonomy_checkbox]', array( 'category' ));
  $output .= '      </p>';

  //List of taxonomies in radiobuttons
  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'List of taxonomies radio', 'balance' ) .':</b></label><br>';
  $output .=          taxonomies_terms_field( $data['sample'][ $key ]['taxonomy_radio'], 'sample['.$key.'][taxonomy_radio]', array( 'category' ), null, false, 'radiolist');
  $output .= '      </p>';

  $output .= '   </div>';
  $output .= '  </div>';
  $output .= '</div>';

  return $output;
}


//Boilerplate for creating edit modules

// function m17_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
//   global $data;

//   if( !empty( $data['m17_module'] ) ){
//     $data['sample'][ $key ] = array(
//       'title' => '',
//     );
//   }

//   $output = '';
//   $output .= '<a name="m17-module-wrapper-'. $key .'"></a>';
//   $output .= '<div class="module-wrapper m17-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
//   $output .= '  <div class="postbox postbox-custom m17-module-list-wrapper-'. $key .'">';
//   $output .= '    <h3>'. $title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
//   $output .= '    <div class="inside hidden">';
//   $output .= '    </div>';
//   $output .= '  </div>';
//   $output .= '</div>';
//   return $output;

// }

?>
